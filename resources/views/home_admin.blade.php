@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Home
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Dashboard <small>Admin </small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
