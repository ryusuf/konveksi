<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
         
        <title>SAN'S TAILOR</title>
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/select2.css') }}">

        <link href="{{ asset('css/green.css') }}" rel="stylesheet">
         
        <!--[if lt IE 9]>
        <script src="../asset/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->
         
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![end

        <!-- Custom Theme Style -->
        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/pnotify.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pnotify.buttons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pnotify.nonblock.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('css/select2.css') }}">

        <link rel="stylesheet" href="{{asset('css/pikaday.css') }}" />
        
        <link rel="stylesheet" href="{{asset('css/custom-style.css') }}" />

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

        <style type="text/css">
            .markerDiv {
                cursor: pointer;
                padding: 5px;
            }
        </style>

        @yield('styles')
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                 
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="index.html" class="site_title"><span>SAN'S TAILOR</span></a>
                        </div>

                        <div class="clearfix"></div>
                 
                        <div class="profile"><!--img_2 -->
                            <div class="profile_pic">
                                <img src="{{ asset('images/img.jpg') }}" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Halo,</span>
                                <h2>{{ Auth::user()->name }}</h2>
                            </div>
                        </div>
                 
                        <br>

                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                 
                            <div class="menu_section">
                                <h3>{{ Auth::user()->role_id }}</h3>
                                <ul class="nav side-menu">
                                    @if(Auth::user()->role_id == 'admin' || Auth::user()->role_id == 'superadmin')
                                        <li class="{{ Request::is('home') ? 'active' : '' }}">
                                            <a href="{{ url('home') }}"><i class="fa fa-home"></i> Home </a>
                                        </li>
                                        <li class="{{ Request::is('karyawan/*') ? 'active' : '' }}">
                                            <a><i class="fa fa-user"></i> Karyawan <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="{{ Request::is('karyawan/*') ? 'display:block;' : '' }}">
                                                <li class="{{ Request::is('karyawan') ? 'active' : '' }}">
                                                    <a href="{{ url('karyawan') }}">Daftar Karyawan</a>
                                                </li>
                                                <li class="{{ Request::is('karyawan/kehadiran') ? 'active' : '' }}">
                                                    <a target="_blank" href="http://localhost/konveksi/public/karyawan/kehadiran">Kehadiran Karyawan</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="{{ Request::is('konsumen/*') ? 'active' : '' }}">
                                            <a href="{{ url('konsumen') }}"><i class="fa fa-users"></i> Konsumen</a>
                                        </li>
                                        <li class="{{ Request::is('bahan/*') ? 'active' : '' }}">
                                            <a href="{{ url('bahan/') }}"><i class="fa fa-cubes"></i> Bahan</a>
                                        </li>
                                        <li class="{{ Request::is('jasa/*') ? 'active' : '' }}">
                                            <a href="{{ url('jasa/') }}"><i class="fa fa-tags"></i> Jasa</a>
                                        </li>
                                        <li class="{{ Request::is('tambahan/*') ? 'active' : '' }}">
                                            <a href="{{ url('tambahan/') }}"><i class="fa fa-plus"></i> Master Tambahan</a>
                                        </li>
                                        <li class="{{ Request::is('order/*') ? 'active' : '' }}">
                                            <a><i class="fa fa-shopping-cart"></i> Order <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="{{ Request::is('order/*') ? 'display:block;' : '' }}">
                                                <li class="{{ Request::is('order') ? 'active' : '' }}">
                                                    <a href="{{ url('order') }}">Harian</a>
                                                </li>
                                                <li class="{{ Request::is('order/custom/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/custom') }}">PDU + JAS</a>
                                                </li>
                                                <li class="{{ Request::is('order/permak/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/permak') }}">Permak</a>
                                                </li>
                                                <li class="{{ Request::is('order/borongan/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/borongan') }}">Borongan</a>
                                                </li>
                                                <li class="{{ Request::is('order/sespim/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/sespim') }}">Sespim</a>
                                                </li>
                                                @foreach(\App\Model\DynamicOrder::get() as $key => $value)
                                                    <li class="{{ Request::is('new-order/index/'.str_slug($value->nama_order, '_').'/*') ? 'active' : '' }}">
                                                        <a href="{{ url('new-order/index/'.str_slug($value->nama_order, '_').'') }}">{{ $value->nama_order }}</a>
                                                    </li>
                                                @endforeach
                                                <li class="{{ Request::is('order/pending/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/pending') }}">Pending</a>
                                                </li>
                                                <li class="{{ Request::is('order/batal/*') ? 'active' : '' }}">
                                                    <a href="{{ url('order/batal') }}">Batal</a>
                                                </li>
                                            </ul>
                                        </li>
                                        {{-- <li class="{{ Request::is('order/*') ? 'active' : '' }}">
                                            <a href="{{ url('order/') }}"><i class="fa fa-shopping-cart"></i> Order </a>
                                        </li> --}}
                                        <li class="{{ Request::is('produksi/manual/*') ? 'active' : '' }}">
                                            <a><i class="fa fa-truck"></i> Mulai Produksi <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="{{ Request::is('produksi/manual/*') ? 'display:block;' : '' }}">
                                                <li class="{{ Request::is('produksi/manual/cutting') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/manual/cutting') }}">Proses Cutting</a>
                                                </li>
                                                <li class="{{ Request::is('produksi/manual/jahit') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/manual/jahit') }}">Proses Jahit</a>
                                                </li>
                                                <li class="{{ Request::is('produksi/manual/finishing') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/manual/finishing') }}">Proses Finishing</a>
                                                </li>
                                                <li class="{{ Request::is('produksi/manual/ekspedisi') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/manual/ekspedisi') }}">Proses Ekspedisi</a>
                                                </li>
                                            </ul>
                                        </li>
                                        {{-- <li class="{{ Request::is('produksi/manual') ? 'active' : '' }}">
                                            <a href="{{ url('produksi/manual') }}"><i class="fa fa-truck"></i> Mulai Produksi </a>
                                        </li> --}}
                                        <li class="{{ Request::is('produksi/*') && !Request::is('produksi/manual/*') ? 'active' : '' }}">
                                            <a><i class="fa fa-gavel"></i> Selesai Produksi <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="{{ Request::is('produksi/*') && !Request::is('produksi/manual/*') ? 'display:block;' : '' }}">
                                                <li class="{{ Request::is('produksi/cutting/selesai') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/cutting/selesai') }}">Proses Cutting</a>
                                                </li>
                                                <li class="{{ Request::is('produksi/jahit/selesai') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/jahit/selesai') }}">Proses Jahit</a>
                                                </li>
                                                {{-- <li class="{{ Request::is('produksi/packing/selesai') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/packing/selesai') }}">Proses Finishing</a>
                                                </li> --}}
                                                <li class="{{ Request::is('produksi/ekspedisi/selesai') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/ekspedisi/selesai') }}">Proses Ekspedisi</a>
                                                </li>
                                                <li class="{{ Request::is('produksi/finishing/selesai') ? 'active' : '' }}">
                                                    <a href="{{ url('produksi/finishing/selesai') }}">Proses Finishing</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="{{ Request::is('produksi/batal/*') ? 'active' : '' }}">
                                            <a href="{{ url('produksi/batal') }}"><i class="fa fa-times"></i> Batalkan Produksi </a>
                                        </li>
                                        <li class="{{ Request::is('laporan/*') ? 'active' : '' }}">
                                            <a><i class="fa fa-file-excel-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu" style="{{ Request::is('laporan/*') ? 'display:block;' : '' }}">
                                                <li class="{{ Request::is('laporan/list/harian') ? 'active' : '' }}">
                                                    <a href="{{ url('laporan/list/harian') }}">Progress</a>
                                                </li>
                                                <li class="{{ Request::is('laporan/gaji') ? 'active' : '' }}">
                                                    <a href="{{ url('laporan/gaji') }}" target="_blank">Gaji</a>
                                                </li>
                                                <li class="{{ Request::is('laporan/absen') ? 'active' : '' }}">
                                                    <a href="{{ url('laporan/absen') }}">Absen</a>
                                                </li>
                                                <li class="{{ Request::is('laporan/bonus') ? 'active' : '' }}">
                                                    <a href="{{ url('laporan/bonus') }}">Bonus Akhir Tahun</a>
                                                </li>
                                            </ul>
                                        </li>
                                        @if(Auth::user()->role_id == 'superadmin' && Auth::user()->username == 'edi')
                                            <li class="{{ Request::is('config') ? 'active' : '' }}">
                                                <a href="{{ url('config') }}"><i class="fa fa-cog"></i> Kelola Nomor Order </a>
                                            </li>
                                        @endif
                                        <li class="{{ Request::is('new-order/tambah-kode/*') ? 'active' : '' }}">
                                            <a href="{{ url('new-order/tambah-kode') }}"><i class="fa fa-cog"></i> Tambah Kode Order </a>
                                        </li>
                                        @if(Auth::user()->role_id == 'superadmin')
                                            <li class="{{ Request::is('report/*') ? 'active' : '' }}">
                                                <a href="{{ url('report') }}"><i class="fa fa-file-excel-o"></i> Report Excel </a>
                                            </li>
                                        @endif
                                    @elseif(Auth::user()->role_id == 'karyawan')
                                        <li class="{{ Request::is('home') ? 'active' : '' }}">
                                            <a href="{{ url('home') }}"><i class="fa fa-list"></i> List Hasil Produksi </a>
                                        </li>
                                        @if(Auth::user()->karyawan->bagian == 'finishing')
                                            <li class="{{ Request::is('produksi/jahit/selesai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/jahit/selesai') }}"><i class="fa fa-gavel"></i> Terima Jahit</a>
                                            </li>
                                            <li class="{{ Request::is('produksi/mulai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/mulai') }}"><i class="fa fa-truck"></i> Mulai QC </a>
                                            </li>
                                            <li class="{{ Request::is('laporan/list/harian') ? 'active' : '' }}">
                                                <a href="{{ url('laporan/list/harian') }}"><i class="fa fa-file-excel-o"></i>Progress</a>
                                            </li>
                                        @elseif(Auth::user()->karyawan->bagian != 'ekspedisi')
                                            <li class="{{ Request::is('produksi/mulai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/mulai') }}"><i class="fa fa-truck"></i> Mulai Produksi </a>
                                            </li>
                                        @endif
                                        @if(Auth::user()->karyawan->bagian == 'jahit')
                                            <li class="{{ Request::is('produksi/cutting/selesai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/cutting/selesai') }}"><i class="fa fa-gavel"></i> Selesai Cutting</a>
                                            </li>
                                        @elseif(Auth::user()->karyawan->bagian == 'ekspedisi')
                                            <li class="{{ Request::is('produksi/mulai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/mulai') }}"><i class="fa fa-truck"></i> Mulai Produksi </a>
                                            </li>
                                            <li class="{{ Request::is('produksi/ekspedisi/selesai') ? 'active' : '' }}">
                                                <a href="{{ url('produksi/ekspedisi/selesai') }}"><i class="fa fa-gavel"></i> Selesai Ekspedisi</a>
                                            </li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        </div>
                 
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout">
                                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="{{ asset('images/img.jpg') }}" alt="">{{ Auth::user()->name }}
                                        <span class=" fa fa-angle-down"></span>
                                    </a>

                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="javascript:;"> Profile</a></li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="badge bg-red pull-right">50%</span>
                                                <span>Settings</span>
                                            </a>
                                        </li>
                                        <li><a href="javascript:;">Help</a></li>
                                        <li>
                                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out pull-right"></i> Log Out
                                            </a>

                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green">6</span>
                                    </a>

                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                        <li>
                                            <a>
                                                <span class="image"><img src="{{ asset('images/img.jpg') }}" alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image"><img src="{{ asset('images/img.jpg') }}" alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image"><img src="{{ asset('images/img.jpg') }}" alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image"><img src="{{ asset('images/img.jpg') }}" alt="Profile Image" /></span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="text-center">
                                                <a>
                                                    <strong>See All Alerts</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="right_col" role="main">
                    <input type="hidden" value="{{ url('/') }}" id="base_url">
                    @yield('content')

                    @include('modal.loading')
                </div>

                <footer>
                    <div class="pull-right">
                        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <!-- FastClick -->
        <script src="{{ asset('js/fastclick.js') }}"></script>
        <!-- NProgress -->
        <script src="{{ asset('js/nprogress.js') }}"></script>
        
        <!-- Custom Theme Scripts -->
        <script src="{{ asset('js/custom.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

        <script src="{{ asset('js/pnotify.js') }}"></script>
        <script src="{{ asset('js/pnotify.buttons.js') }}"></script>
        <script src="{{ asset('js/pnotify.nonblock.js') }}"></script>

        <script src="{{ asset('js/select2.full.min.js') }}"></script>
        <script src="{{ asset('js/icheck.min.js') }}"></script>

        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/pikaday.js') }}"></script>
        <script src="{{ asset('js/pikaday.jquery.js') }}"></script>
        <script src="{{ asset('js/jquery.datetimepicker.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.inputs').first().focus()

                
                    $("form").submit(function() {
                        // submit more than once return false
                        $(this).submit(function() {
                            console.log('asdsad')
                            return false;
                        });
                        // submit once return true
                        return true;
                    });
            });

            var base_url = "{{ url('/') }}";

            @if (Session::has('message'))
                notify("{{ session('message_title','Info') }}","{{ session('message') }}","{{ session('message_type','ok') }}");
            @endif

            var prevent;
            function notifyConfirm(e) {
                if (!confirm("Apakah anda yakin?")) {
                    e.preventDefault();
                    prevent = true;
                    return false;
                }
                return true;
            }

            function notify(message,description,alert) {
                if (alert == "ok") {
                    new PNotify({
                        title: message,
                        text: description,
                        type: 'success',
                        icon: 'fa fa-check',
                        history: false,
                        styling: 'bootstrap3',
                    });
                } else {
                    new PNotify({
                        title: message,
                        text: description,
                        type: alert,
                        icon: 'fa fa-envelope-o',
                        history: false,
                        styling: 'bootstrap3',
                    });
                }
            }

            $(".inputs").on('keypress', function(e) {
                if(this.type != 'submit' && e.keyCode == 13) {
                    e.preventDefault();
                }else{
                    return true;
                }
            });

            $(".inputs").on('keyup', function(e) {
                if(this.value.length == this.maxLength) {
                    var idx = $('.inputs').index(this);
                    $('.inputs').eq(idx + 1).focus();
                }
                if(e.which == 13) {
                    if(this.type != 'submit') {
                        e.preventDefault();

                        var idx = $('.inputs').index(this);
                        $('.inputs').eq(idx + 1).focus();
                    }

                }
                if( e.which === 38 && e.ctrlKey ){
                    e.preventDefault();

                    var idx = $('.inputs').index(this);
                    $('.inputs').eq(idx - 1).focus();
                }
            });

            $('.markerDiv').click(function () {
                if(!$(this).find('input:checkbox').prop('disabled')) {
                    if ($(this).find('input:checkbox').is(":checked")) {
                        $(this).find('input:checkbox').prop("checked", false);
                    }else {
                        $(this).find('input:checkbox').prop("checked", true);
                    }
                }
            });

             $('input[type=checkbox]').click(function (e) {
                 e.stopPropagation();
             });

             $('.date').pikaday({ firstDay: 1, format: 'YYYY-MM-DD', yearRange: [1940,{{date('Y')}}] });
             $('#datetime').datetimepicker()
        </script>

        @include('modal.script')

        @yield('scripts')
    </body>
</html>