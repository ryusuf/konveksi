@extends('layouts.app_login')

@section('content')
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <h1>Login Form</h1>
                    <div class="{{ $errors->has('username') ? ' has-error' : '' }}">
                        <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus placeholder="username / email">

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>
                                <a class="reset_pass" href="{{ route('password.request') }}">Lost your password?</a>
                            </label>
                        </div>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-default submit btn-block">Login</button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <div>
                            <h1>SAN'S TAILOR</h1>
                            <p>©{{ date('Y') }} All Rights Reserved.</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
@endsection
