@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Report
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Report Excel</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-wrapper form-horizontal form-label-left">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                Tanggal Mulai 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="tanggal_mulai" class="form-control col-md-7 col-xs-12" id="tanggal_mulai">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                Tanggal Selesai
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="tanggal_selesai" class="form-control col-md-7 col-xs-12" id="tanggal_selesai">
                            </div>
                        </div>

                        <div class="col-md-offset-3 col-md-6">
                            <button class="btn btn-primary pull-right" onclick="download()">
                                <i class="fa fa-file-text"></i>&nbsp; Download
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var picker = new Pikaday({
            firstDay: 1, 
            format: 'YYYY-MM-DD', 
            onSelect: function() {
                vueApp.params.tanggal_mulai = $('#tanggal_mulai').val();
                
                $('#tanggal_selesai').val('');

                this.getMoment().format('DD-MM-YYYY');
                picker2.setMinDate(this.getDate());
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_mulai')
        });

        var picker2 = new Pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
            onSelect: function() {
                this.getMoment().format('DD-MM-YYYY');
                vueApp.params.tanggal_selesai = $('#tanggal_selesai').val();
            },
            disableDayFn: function (date) {
                var enabled_dates = picker.getDate();
                
                if (moment(date).format("YYYY-MM-DD") < enabled_dates) {
                    return date;
                }
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_selesai')
        });

        function download() {
            var url,tanggal_selesai,tanggal_mulai;

            tanggal_selesai     = $('#tanggal_selesai').val();
            tanggal_mulai       = $('#tanggal_mulai').val();
            url                 = "{{ url('report/download') }}?tanggal_mulai="+tanggal_mulai+"&tanggal_selesai="+tanggal_selesai;

            window.open(url);
        }
    </script>
@endsection