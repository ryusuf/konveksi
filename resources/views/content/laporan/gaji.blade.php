@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Laporan Gaji
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Laporan Gaji</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('laporan/get-gaji') }}">

                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bagian <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="bagian" class="form-control col-md-7 col-xs-12" v-model="params.bagian">
                                            <option value=""></option>
                                            <option value="cutting">Cutting</option>
                                            <option value="jahit">Jahit</option>
                                            <option value="finishing">Finishing</option>
                                            <option value="ekspedisi">Ekspedisi</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                        </select2>
                                    </div>
                                </div>

                                <template v-if="params.bagian == 'finishing'">
                                    <div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tunjangan Finishing
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="tunjangan[]" type="checkbox" v-model="params.tunjangan" class="" value="finishing">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tunjangan Mesin Bordir
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="tunjangan[]" type="checkbox" v-model="params.tunjangan" class="" value="bordir">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tunjangan Wilcom
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="tunjangan[]" type="checkbox" v-model="params.tunjangan" class="" value="wilcom">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tunjangan Steam
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="tunjangan[]" type="checkbox" v-model="params.tunjangan" class="" value="steam">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tunjangan QC
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="tunjangan[]" type="checkbox" v-model="params.tunjangan" class="" value="qc">
                                            </div>
                                        </div>
                                    </div>
                                </template>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Mulai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="tanggal_mulai" class="form-control col-md-7 col-xs-12" id="tanggal_mulai" readonly="" v-model="params.tanggal_mulai">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Selesai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="tanggal_selesai" class="form-control col-md-7 col-xs-12" id="tanggal_selesai" readonly="" v-model="params.tanggal_selesai">
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Mulai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="control-label">{{ getFullDate($start) }} 03:01 PM</label>
                                        <input type="hidden" name="tanggal_mulai" class="form-control col-md-7 col-xs-12" id="tanggal_mulai" readonly="" value="{{ $start }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Selesai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="control-label">{{ getFullDate($end) }} 03:00 PM</label>
                                        <input type="hidden" name="tanggal_selesai" class="form-control col-md-7 col-xs-12" id="tanggal_selesai" readonly="" value="{{ $end }}">
                                    </div>
                                </div> --}}

                                <div class="col-md-offset-3 col-md-6">
                                    <a href="javascript:void(0);" onclick="showData()" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> Lihat</a>
                                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Gaji</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper" v-if="params.bagian != 'finishing'">
                            <ajax-table
                                :url="'{{url('laporan/get-data-gaji')}}'"
                                :oid="'data-gaji'"
                                :params="params"
                                :config="{
                                    autoload: false,
                                    show_all: true,
                                    has_number: false,
                                    has_entry_page: false,
                                    has_pagination: false,
                                    has_action: false,
                                    has_search_input: true,
                                    has_search_header: false,
                                    has_custom_body: true,
                                    custom_header: 'header-gaji',
                                    default_sort: 'id',
                                    custom_empty_page: false,
                                    search_placeholder: 'Cari',
                                    class: {
                                        table: ['table-bordered'],
                                        wrapper: ['table-responsive'],
                                    }
                                }"
                                :rowparams="{}"
                                :rowtemplate="'tr-data-gaji'"
                            >
                            </ajax-table>
                        </div>
                        <div class="table-wrapper" v-if="params.bagian == 'finishing'">
                            <ajax-table
                                :url="'{{url('laporan/get-data-gaji')}}'"
                                :oid="'data-gaji'"
                                :params="params"
                                :config="{
                                    autoload: false,
                                    show_all: true,
                                    has_number: false,
                                    has_entry_page: false,
                                    has_pagination: false,
                                    has_action: false,
                                    has_search_input: true,
                                    has_search_header: false,
                                    has_custom_body: true,
                                    custom_header: '',
                                    default_sort: 'id',
                                    custom_empty_page: false,
                                    search_placeholder: 'Cari',
                                    class: {
                                        table: ['table-bordered'],
                                        wrapper: ['table-responsive'],
                                    },
                                }"
                                :rowparams="{}"
                                :rowtemplate="'tr-data-gaji'"
                                :columns="{
                                    tanggal: 'Tanggal',
                                    jam_masuk: 'Jam Masuk',
                                    jam_pulang: 'Jam Pulang',
                                    gaji:'Gaji',
                                    lembur: 'Lembur',
                                    lembur2: 'Lembur 2',
                                    tunjangan: 'Tunjangan',
                                    total_tunjangan: 'Total Tunjangan',
                                    subtotal: 'Subtotal',
                                }"
                            >
                            </ajax-table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKaryawan(vueApp.params.bagian);

        vueApp.params.tanggal_mulai = '{{ $start }}';
        vueApp.params.tanggal_selesai = '{{ $end }}';

        vueApp.$watch('params.bagian', function() {
            vueApp.getKaryawan(vueApp.params.bagian);
            eventHub.$emit('get-data-produksi');
        });

        var picker = new Pikaday({
            firstDay: 1, 
            format: 'YYYY-MM-DD', 
            onSelect: function() {
                vueApp.params.tanggal_mulai = $('#tanggal_mulai').val();
                
                $('#tanggal_selesai').val('');

                this.getMoment().format('DD-MM-YYYY');
                picker2.setMinDate(this.getDate());
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_mulai')
        });

        var picker2 = new Pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
            onSelect: function() {
                this.getMoment().format('DD-MM-YYYY');
                vueApp.params.tanggal_selesai = $('#tanggal_selesai').val();
            },
            disableDayFn: function (date) {
                var enabled_dates = picker.getDate();
                
                if (moment(date).format("YYYY-MM-DD") < enabled_dates) {
                    return date;
                }
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_selesai')
        });

        function showData(evt) {
            window.eventHub.$emit('refresh-ajaxtable','data-gaji');
            return false;
        }
    </script>
@endsection