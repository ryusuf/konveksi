@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Progress
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Progress</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <div class="table-responsive">
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No Order</th>
                                            <th class="text-center">Jenis Jahitan</th>
                                            <th class="text-center">Pola</th>
                                            <th class="text-center">Jahit</th>
                                            <th class="text-center">QC</th>
                                            <th class="text-center">Ekspedisi</th>
                                            <th class="text-center">JNE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($models as $ord)
                                            <tr>
                                                <td colspan="7" class="bg-primary">Order {{ $ord->kode }}</td>
                                            </tr>
                                            @foreach($ord->produksi as $prod)
                                                <tr>
                                                    <td>{{ $prod->kode }}</td>
                                                    <td>{{ $prod->jenis_jahitan }}</td>
                                                    <td>
                                                        ( {{ $prod->cutting ? $prod->cutting->nama : '' }} )<br>
                                                        {{ getShortDateTime($prod->waktu_mulai_cutting) }} - {{ getShortDateTime($prod->waktu_selesai_cutting) }}
                                                    </td>
                                                    <td>
                                                        ( {{ $prod->jahit ? $prod->jahit->nama : '' }} )
                                                        {{ getShortDateTime($prod->waktu_mulai_jahit) }}<br>
                                                        <br>
                                                        ( {{ $prod->selesai_jahit ? $prod->selesai_jahit->nama : '' }} )
                                                        {{ getShortDateTime($prod->waktu_selesai_jahit) }}
                                                    </td>
                                                    <td>
                                                        ( {{ $prod->finishing ? $prod->finishing->nama : '' }} )<br>
                                                        {{ getShortDateTime($prod->waktu_selesai_finishing) }}
                                                    </td>
                                                    <td>
                                                        ( {{ $prod->ekspedisi ? $prod->ekspedisi->nama : '' }} )<br>
                                                        {{ $prod->ekspedisi ? getShortDateTime($prod->waktu_mulai_ekspedisi) : '' }} - {{ $prod->ekspedisi ? getShortDateTime($prod->waktu_selesai_ekspedisi) : '' }}
                                                    </td>
                                                    <td>
                                                        {{ $prod->id_karyawan_ekspedisi == 0 ? getShortDateTime($prod->waktu_mulai_ekspedisi) : '' }}<br>
                                                        {{ $prod->id_karyawan_ekspedisi == 0 ? $prod->no_resi : '' }}<br>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection