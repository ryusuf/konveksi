<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

        <style type="text/css">
            @media print {
                .table {
                    overflow: visible !important;
                }
            }
        </style>
    </head>
    <body style="background: none;">
        <div class="row">
            <div class="col-xs-12">
                <table class="table borderless">
                    <tr>
                        <td colspan="3">
                            <p style="font-weight:bold;font-size:16px;text-align:center;">
                                STRUK GAJI
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">Nama</td>
                        <td width="5%">:</td>
                        <td>{{ $karyawan->nama }}</td>
                    </tr>
                    <tr>
                        <td width="15%">Bagian</td>
                        <td width="5%">:</td>
                        <td>{{ $karyawan->bagian }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="clearfix"></div>
        <br>
        <br>

        <div class="row">
            <div class="col-xs-12">
                <table class="table table-responsive table-striped">
                    <tbody>
                        <tr>
                            <th>Tanggal</th>
                            <th>Jam Masuk</th>
                            <th>Jam Pulang</th>
                            <th>Gaji</th>
                            <th>Lembur</th>
                            <th>Lembur 2</th>
                            <th>Tunjangan</th>
                            <th>Total Tunjangan</th>
                            <th>Subtotal</th>
                        </tr>
                        @foreach($data as $key => $val)
                            <tr>
                                <td>{{ ($val['tanggal']) }}</td>
                                <td>{{ ($val['masuk']) }}</td>
                                <td>{{ ($val['pulang']) }}</td>
                                <td>{{ 'Rp. '. number_format($val['gaji'] + 0, 0, '.', ',') }}</td>
                                <td>{{ 'Rp. '. number_format($val['lembur'] + 0, 0, '.', ',') }}</td>
                                <td>{{ 'Rp. '. number_format($val['lembur2'] + 0, 0, '.', ',') }}</td>
                                <td>
                                    {{ collect($val['tunjangan'])->map(function($val){return $val['nama'];})->implode(', ') }}
                                </td>
                                <td>{{ 'Rp. '. number_format($val['total_tj'] + 0, 0, '.', ',') }}</td>
                                <td>{{ 'Rp. '. number_format($val['subtotal'] + 0, 0, '.', ',') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="8" style="text-align: right;"><label>Total</label></td>
                            <td>{{ 'Rp. '. number_format($gaji + 0, 0, '.', ',') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>        
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

        <script type="text/javascript">
            window.print()
        </script>
    </body>
</html>