<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

        <style type="text/css">
            @media print {
                .table {
                    overflow: visible !important;
                }
            }
        </style>
    </head>
    <body style="background: none;">
        <div class="row">
            <div class="col-xs-12">
                <table class="table borderless">
                    <tr>
                        <td colspan="3">
                            <p style="font-weight:bold;font-size:16px;text-align:center;">
                                STRUK GAJI
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">Nama</td>
                        <td width="5%">:</td>
                        <td>{{ $karyawan->nama }}</td>
                    </tr>
                    <tr>
                        <td width="15%">Bagian</td>
                        <td width="5%">:</td>
                        <td>{{ $karyawan->bagian }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="clearfix"></div>
        <br>
        <br>

        <div class="row">
            <div class="col-xs-12">
                <table class="table table-responsive table-striped">
                    <tbody>
                        <tr>
                            <th>No Order</th>
                            <th>Qty</th>
                            <th>Order</th>
                            <th>Tgl Selesai</th>
                            <th>Harga</th>
                            <th>Tambahan</th>
                            <th>Subtotal</th>
                        </tr>
                        @foreach($data as $key => $val)
                            <tr>
                                <td>{{ ($val['kode']) }}</td>
                                <td style="text-align: center;">{{ ($val['qty']) }}</td>
                                <td>{{ ($val['jenis_jahitan']) }}</td>
                                <td>{{ ($val['tanggal_selesai']) }}</td>
                                <td style="text-align: right;">{{ 'Rp. '. number_format($val['harga'] + 0, 0, '.', ',') }}</td>
                                <td style="text-align: right;">{{ 'Rp. '. number_format($val['tambahan'] + 0, 0, '.', ',') }}</td>
                                <td style="text-align: right;">{{ 'Rp. '. number_format($val['subtotal'] + 0, 0, '.', ',') }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><label>Total</label></td>
                            <td style="text-align: center;">{{ $qty }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;">{{ 'Rp. '. number_format($gaji + 0, 0, '.', ',') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>        
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

        <script type="text/javascript">
            window.print()
        </script>
    </body>
</html>