@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Absen
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Absen</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                    </select2>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Mulai <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="tanggal_mulai" class="form-control col-md-7 col-xs-12" id="tanggal_mulai" readonly="" v-model="params.tanggal_mulai">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Selesai <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="tanggal_selesai" class="form-control col-md-7 col-xs-12" id="tanggal_selesai" readonly="" v-model="params.tanggal_selesai">
                                </div>
                            </div>

                            <div class="col-md-offset-3 col-md-6">
                                <button onclick="submit()" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> Lihat</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Absen</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <ajax-table
                                :url="'{{url('laporan/get-absen')}}'"
                                :oid="'data-absen'"
                                :params="params"
                                :config="{
                                    autoload: true,
                                    show_all: true,
                                    has_number: false,
                                    has_entry_page: false,
                                    has_pagination: false,
                                    has_action: false,
                                    has_search_input: true,
                                    has_search_header: false,
                                    has_custom_body: true,
                                    custom_header: 'header-absen',
                                    default_sort: 'id',
                                    custom_empty_page: false,
                                    search_placeholder: 'Cari',
                                    class: {
                                        table: ['table-bordered'],
                                        wrapper: ['table-responsive'],
                                    }
                                }"
                                :rowparams="{}"
                                :rowtemplate="'tr-data-absen'"
                            >
                            </ajax-table>
                                {{-- <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No Order</th>
                                            <th class="text-center">Jenis Jahitan</th>
                                            <th class="text-center">Pola</th>
                                            <th class="text-center">Jahit</th>
                                            <th class="text-center">QC</th>
                                            <th class="text-center">Ekspedisi</th>
                                            <th class="text-center">JNE</th>
                                        </tr>
                                    </thead>
                                    
                                </table> --}}                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $('#document').ready(function() {
            vueApp.params.tanggal_mulai = '{{ $start }}';
            vueApp.params.tanggal_selesai = '{{ $end }}';
            vueApp.getKaryawan(vueApp.params.bagian);

            vueApp.$watch('params.bagian', function() {
                vueApp.getKaryawan(vueApp.params.bagian);
                eventHub.$emit('get-data-produksi');
            });

            window.eventHub.$emit('refresh-ajaxtable','data-absen');
        });

        var picker = new Pikaday({
            firstDay: 1, 
            format: 'YYYY-MM-DD', 
            onSelect: function() {
                vueApp.params.tanggal_mulai = $('#tanggal_mulai').val();

                this.getMoment().format('DD-MM-YYYY');
                // picker2.setMinDate(this.getDate());
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_mulai')
        });


        var picker2 = new Pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
            onSelect: function() {
                this.getMoment().format('DD-MM-YYYY');
                vueApp.params.tanggal_selesai = $('#tanggal_selesai').val();
            },
            disableDayFn: function (date) {
                var enabled_dates = picker.getDate();
                
                if (moment(date).format("YYYY-MM-DD") < enabled_dates) {
                    return date;
                }
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_selesai')
        });


        function submit() {
            window.eventHub.$emit('refresh-ajaxtable','data-absen');
        }

        setTimeout(function() {
            window.eventHub.$emit('refresh-ajaxtable','data-absen');
        }, 60000);

    </script>
@endsection