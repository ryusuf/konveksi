@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Bonus Akhir Tahun {{ date('Y') }}
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Bonus Akhir Tahun {{ date('Y') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bagian <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="bagian" class="form-control col-md-7 col-xs-12" v-model="params.bagian">
                                        <option value=""></option>
                                        <option value="cutting">Cutting</option>
                                        <option value="jahit">Jahit</option>
                                        <option value="finishing">Finishing</option>
                                        <option value="ekspedisi">Ekspedisi</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                    </select2>
                                </div>
                            </div>

                            <div class="col-md-offset-3 col-md-6">
                                <a href="javascript:void(0);" onclick="showData()" class="btn btn-primary pull-right"><i class="fa fa-eye"></i> Lihat</a>
                                <!-- <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Download</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Bonus {{ date('Y') }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" v-if="dataBonus.karyawan">
                        <div class="table-wrapper">
                            <div class="text-center" style="margin-bottom: 2rem;">
                                <h4>SAN'S TAILOR</h4>
                                <h4>BONUS TAHUNAN KARYAWAN</h4>
                                <h4>TAHUN {{ date('Y') }}</h4>
                            </div>
                            <div class="col-md-9 text-left">
                                <label>Kode : @{{ dataBonus.karyawan.id }}</label>
                                <br class="clearfix"/>
                                <label>Nama : @{{ dataBonus.karyawan.nama }}</label>
                            </div>
                            <div class="col-md-3 text-left">
                                <label>Masuk Tahun : @{{ dataBonus.karyawan.tahun_kerja }}</label>
                                <br class="clearfix"/>
                                <label>Lama Kerja : @{{ dataBonus.masa_kerja }} Tahun</label>
                            </div>
                            <div class="col-md-12" style="margin-top: 2rem;margin-bottom: 2rem;">
                                <label>Jumlah Hari Kerja : @{{ dataBonus.jumlah_hari_kerja }} Hari</label>
                                <br class="clearfix"/>
                                <label>Jumlah Kehadiran : @{{ dataBonus.jumlah_kehadiran }} Hari</label>
                                <br class="clearfix"/>
                                <label>Persentase Kehadiran : @{{ dataBonus.persentase_kehadiran }} %</label>
                                <br class="clearfix"/>
                                <label>Persentase Kedisiplinan : @{{ dataBonus.persentase_kedisiplinan }} %</label>
                            </div>
                            <div class="col-md-offset-9 col-md-3">
                                <label>Bonus Kehadiran : @{{ dataBonus.bonus_kehadiran}}</label>
                                <br class="clearfix"/>
                                <label>Bonus Kedisiplinan : @{{ dataBonus.bonus_kedisiplinan }}</label>
                                <br class="clearfix"/>
                                <label>Bonus Kebijakan : @{{ dataBonus.bonus_kebijakan }}</label>
                                <hr class="clearfix"/>
                                <label>Jumlah : @{{ dataBonus.total  }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKaryawan(vueApp.params.bagian);

        vueApp.$watch('params.bagian', function() {
            vueApp.getKaryawan(vueApp.params.bagian);
        });

        function showData(evt) {
            vueApp.getBonus();
            return false;
        }
    </script>
@endsection