@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Tambah Order Permak
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form <small>Tambah Order Permak</small></h2>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#konsumenModal">
                          <i class="fa fa-plus"></i> Konsumen
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#jasaModal">
                          <i class="fa fa-plus"></i> Jasa
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#bahanModal">
                          <i class="fa fa-plus"></i> Bahan
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#tambahanModal">
                          <i class="fa fa-plus"></i> Tambahan
                        </button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('new-order/store') }}">

                            {!! csrf_field() !!}

                            <input value="permak" type="hidden" name="prefix" />
                            
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>{{ $kode }}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Order <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="tanggal_order" class="form-control col-md-7 col-xs-12 date" required="required" readonly="" value="{{ getTanggalOrder() }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Konsumen <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="konsumen_id" class="form-control inputs col-md-7 col-xs-12" :options="option_konsumen" v-model="params.konsumen_id" :multiple="false" :focused="true">
                                    </select2>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <p class="fa fa-refresh fa-2x" style="cursor: pointer;" @click="getKonsumen()"></p>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Jahitan <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="jenis_jahitan" class="form-control inputs col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Qty <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="jumlah" class="form-control inputs col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Stel / Pcs <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="kuantitas" class="form-control inputs col-md-7 col-xs-12" required="required">
                                </div>
                            </div> --}}
                            {{-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Jahitan <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="col-md-3">
                                        <select name="qty" class="form-control">
                                            @foreach(range(1, 20) as $key => $val)
                                                <option value="{{ $val }}" @if($val == 1) selected @endif>{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="last-name" name="jenis_jahitan" class="form-control inputs col-md-7 col-xs-12" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tambahan</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="tambahan[]" class="form-control inputs" :options="option_tambahan" :multiple="true">
                                    </select2>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <p class="fa fa-refresh fa-2x" style="cursor: pointer;" @click="getTambahan()"></p>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Detail Jenis Jahitan <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input-dinamis nama="jenis_jahitan_detail"></input-dinamis>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Harga <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <currency-input kode="Rp." v-model="gaji" :name="'harga'"></currency-input>
                                    <input :value="gaji" type="hidden" name="harga" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="karyawan_id" class="form-control inputs col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                    </select2>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <p class="fa fa-refresh fa-2x" style="cursor: pointer;" @click="getKaryawan()"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Grup <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label><input type="radio" name="is_grup" value="1">Ya</label>
                                    <label><input type="radio" name="is_grup" value="0" checked="">Tidak</label>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success inputs">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('modal.konsumen')

        @include('modal.jasa')

        @include('modal.bahan')

        @include('modal.tambahan')
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKonsumen();
        vueApp.getTambahan();
        vueApp.getKaryawan('jahit');
    </script>
@endsection