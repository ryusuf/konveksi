@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Lihat Master Bahan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Bahan : {{ $model->nama_bahan }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode Barang</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->kode }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Bahan</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->nama_bahan }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Harga</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->harga }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
