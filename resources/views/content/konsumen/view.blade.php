@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Lihat Konsumen
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Konsumen : {{ $model->nama }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NRP</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->nrp }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->nama }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pangkat</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->pangkat }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Telepon</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->telp_1 . ($model->telp_2 ? '/'. $model->telp_2 : '') }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">TIK</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->tik }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fungsi</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->fungsi }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->alamat }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
