@extends('layouts.app')

@section('styles')
    @if(Session::has('download.in.the.next.request'))
        <meta http-equiv="refresh" content="5;url={{ Session::get('download.in.the.next.request') }}">
        @php
            Session::forget('download.in.the.next.request');
        @endphp
    @endif
@endsection

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Halaman Download
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel text-center">
                    <div class="x_title">
                        <h2>Download <small>Ekspedisi</small></h2>
                        <a href="{{ url('produksi/manual/ekspedisi') }}" class="btn btn-primary pull-right">Kembali</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="fa fa-spinner fa-spin" style="font-size: 50px;"></p>
                        <br>
                        <div>
                            File sedang di download
                            <br>
                            <br>
                            <a href="{{ Session::get('download.in.the.next.request') }}" class="btn btn-primary">klik disini jika terjadi kesalahan</a>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection