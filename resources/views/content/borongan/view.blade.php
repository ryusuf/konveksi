@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Lihat Order Borongan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order : </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @foreach($models as $model)
                            <div class="well">
                                <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Order</h4>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->kode }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Jahitan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->jenis_jahitan }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->jumlah }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kuantitas</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->kuantitas }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ getStatusProduksi($model->status_akhir) }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Diterima Oleh</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->penerima_ekspedisi }}
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>

                                <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konsumen</h4>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NRP</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->nrp : '' }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Konsumen</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->nama : '' }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pangkat</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->pangkat : '' }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Telp</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->telp_1 : '' }} {{ $model->konsumen ? ' / '.$model->konsumen->telp_2 : '' }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">TIK</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->tik : '' }}
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $model->konsumen ? $model->konsumen->alamat : '' }}
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>

                                <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Produksi</h4>
                                @foreach($model->produksi as $key => $prod)
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ $key+1 }}</label>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            {{ $prod->kode }}
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            {{ $prod->jenis_jahitan }}
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <label class="btn btn-success btn-xs">{{ getStatusProduksi($prod->status_akhir) }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tambahan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @if($prod->tambahan)
                                            @foreach($prod->tambahan as $tam)
                                                <label>{{ $tam->jenis_tambahan }}</label>
                                                <br>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 well">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Nama Proses</th>
                                                <th>Dimulai Oleh</th>
                                                <th>Waktu Mulai</th>
                                                <th>Selesai Oleh</th>
                                                <th>Waktu Selesai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Proses Cutting</td>
                                                <td>{{ $prod->cutting ? $prod->cutting->nama : '' }}</td>
                                                <td>{{ $prod->waktu_mulai_cutting ? getFullDateTime($prod->waktu_mulai_cutting) : '' }}</td>
                                                <td>{{ $prod->selesai_cutting ? $prod->selesai_cutting->nama : '' }}</td>
                                                <td>{{ $prod->waktu_selesai_cutting ? getFullDateTime($prod->waktu_selesai_cutting) : '' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Proses Jahit</td>
                                                <td>{{ $prod->jahit ? $prod->jahit->nama : '' }}</td>
                                                <td>{{ $prod->waktu_mulai_jahit ? getFullDateTime($prod->waktu_mulai_jahit) : '' }}</td>
                                                <td>{{ $prod->selesai_jahit ? $prod->selesai_jahit->nama : '' }}</td>
                                                <td>{{ $prod->waktu_selesai_jahit ? getFullDateTime($prod->waktu_selesai_jahit) : '' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Proses Finishing</td>
                                                <td>{{ $prod->finishing ? $prod->finishing->nama : '' }}</td>
                                                <td>{{ $prod->waktu_mulai_finishing ? getFullDateTime($prod->waktu_mulai_finishing) : '' }}</td>
                                                <td>{{ $prod->selesai_finishing ? $prod->selesai_finishing->nama : '' }}</td>
                                                <td>{{ $prod->waktu_selesai_finishing ? getFullDateTime($prod->waktu_selesai_finishing) : '' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Proses Ekspedisi</td>
                                                <td>{{ $prod->ekspedisi ? $prod->ekspedisi->nama : '' }}</td>
                                                <td>{{ $prod->waktu_mulai_ekspedisi ? getFullDateTime($prod->waktu_mulai_ekspedisi) : '' }}</td>
                                                <td>{{ $prod->selesai_ekspedisi ? $prod->selesai_ekspedisi->nama : '' }}</td>
                                                <td>{{ $prod->waktu_selesai_ekspedisi ? getFullDateTime($prod->waktu_selesai_ekspedisi) : '' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @endforeach

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
