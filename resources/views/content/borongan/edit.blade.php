@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Edit Order Borongan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form <small>Edit Order Borongan</small></h2>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#konsumenModal">
                          <i class="fa fa-plus"></i> Konsumen
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#jasaModal">
                          <i class="fa fa-plus"></i> Jasa
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#bahanModal">
                          <i class="fa fa-plus"></i> Bahan
                        </button>
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#tambahanModal">
                          <i class="fa fa-plus"></i> Tambahan
                        </button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('order/borongan/update') }}">

                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tanggal Order <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="tanggal_order" class="form-control col-md-7 col-xs-12 date" required="required" readonly="" value="{{ $model[0] ? $model[0]->created_at : '' }}">
                                </div>
                            </div>
                            
                            <borongan id="{{ $id }}" keterangan="{{ $model[0] ? $model[0]->keterangan_borongan : '' }}"></borongan>
                            
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success inputs">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('modal.konsumen')

        @include('modal.jasa')

        @include('modal.bahan')

        @include('modal.tambahan')
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        
    </script>
@endsection