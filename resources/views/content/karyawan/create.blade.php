@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Tambah Karyawan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form <small>Tambah Karyawan</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('karyawan/store') }}">

                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('nama') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No KTP <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="no_ktp" required="required" class="form-control col-md-7 col-xs-12 inputs" pattern=".{16,16}" maxlength="16" value="{{ old('no_ktp') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control inputs" name="alamat" rows="3" required="required">{{ old('alamat') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bagian <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="bagian" class="form-control col-md-7 col-xs-12 inputs" v-model="params.bagian_array" :multiple="false" :options="[{id:'cutting', text: 'Cutting'}, {id:'jahit', text: 'Jahit'}, {id:'finishing', text: 'Finishing'}, {id:'ekspedisi', text: 'Ekspedisi'}]">
                                    </select2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kasbon</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <currency-input kode="Rp." v-model="kasbon" :name="'kasbon'"></currency-input>
                                    <input :value="kasbon" type="hidden" name="kasbon" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Gaji</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <currency-input kode="Rp." v-model="gaji" :name="'gaji'"></currency-input>
                                    <input :value="gaji" type="hidden" name="gaji" />
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="username" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('username') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="last-name" name="email" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="last-name" name="password" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('password') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konfirmasi Password <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="last-name" name="konfirmasi_password" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('konfirmasi_password') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Kerja <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="tahun_kerja" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('konfirmasi_password') }}">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success inputs">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.gaji = {{ old('gaji') ? old('gaji') : 0 }};
        vueApp.kasbon = {{ old('kasbon') ? old('kasbon') : 0 }};
    </script>
@endsection