@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Edit Karyawan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form <small>Edit Karyawan</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('karyawan/update') }}">

                            {!! csrf_field() !!}

                            <input type="hidden" name="id" value="{{ $model->id }}">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ $model->nama }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No KTP <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="no_ktp" required="required" class="form-control col-md-7 col-xs-12 inputs" pattern=".{16,16}" maxlength="16" value="{{ $model->no_ktp }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control inputs" name="alamat" rows="3">{{ $model->alamat }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bagian <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="bagian" class="form-control col-md-7 col-xs-12 inputs" v-model="params.bagian_array" :multiple="false" :options="[{id:'cutting', text: 'Cutting'}, {id:'jahit', text: 'Jahit'}, {id:'finishing', text: 'Finishing'}, {id:'ekspedisi', text: 'Ekspedisi'}]">
                                    </select2>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kasbon <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <currency-input kode="Rp." v-model="kasbon" :name="'kasbon'"></currency-input>
                                    <input :value="kasbon" type="hidden" name="kasbon" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Gaji <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <currency-input kode="Rp." v-model="gaji" :name="'gaji'"></currency-input>
                                    <input :value="gaji" type="hidden" name="gaji" />
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="username" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ $model->user ? $model->user->username : '' }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="last-name" name="email" required="required" class="form-control col-md-7 col-xs-12 inputs" value="{{ $model->user ? $model->user->email : '' }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password (Isi jika ingin diganti)</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="last-name" name="password" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('password') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konfirmasi Password</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="password" id="last-name" name="konfirmasi_password" class="form-control col-md-7 col-xs-12 inputs" value="{{ old('konfirmasi_password') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Kerja</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="last-name" name="tahun_kerja" class="form-control col-md-7 col-xs-12 inputs" value="{{ $model->tahun_kerja }}">
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success inputs">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.gaji = {{ $model->gaji ? $model->gaji : 0 }};
        vueApp.kasbon = {{ $model->kasbon ? $model->kasbon : 0 }};

        vueApp.params.bagian_array.push('{{ $model->bagian }}');

    </script>
@endsection