@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Ambil Kehadiran Karyawan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form <small>Ambil Kehadiran Karyawan</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <a href="javascript:void(0);" id="download" class="btn btn-success">Ambil Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKaryawan('finishing');

        var picker = new Pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
            onSelect: function() {
                this.getMoment().format('DD-MM-YYYY');
            },
            yearRange: [1940,{{date('Y')}}],
            field: document.getElementById('tanggal')
        });

        function disableButton(that) {
            $(that).addClass('disabled');
        }

        $('#download').click(function() {
            var token = '{{ csrf_token() }}';

            vueApp.isLoading = true;

            $.ajax('{{ url('store/fp') }}', {
                type: 'POST',
                data: {_token: token},
                statusCode: {
                    500: function() {
                        vueApp.isLoading = false;
                        notify("Terjadi Kesalahan!", "Koneksi bermasalah", "error");
                    }
                },
                success: function(data) {
                    vueApp.isLoading = false;
                    notify("Info", data.message, data.message_type);
                }
            });
        });
    </script>
@endsection