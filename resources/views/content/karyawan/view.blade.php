@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Lihat Karyawan
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Karyawan : {{ $model->nama }}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->nama }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No KTP</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->no_ktp }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->alamat }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bagian</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->bagian }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kasbon</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->kasbon }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Gaji</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->gaji }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->user ? $model->user->username : '' }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->user ? $model->user->email : '' }}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tahun Kerja</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {{ $model->tahun_kerja }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
