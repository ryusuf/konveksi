@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Finishing
                </h3>
            </div>

            <div class="title_right">
                
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Proses Finishing<small>Selesai</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @foreach($order as $key => $ord)
                            <div class="well">
                                @foreach($ord->produksi as $d => $prod)
                                    @if($prod->status_akhir == 5)
                                        <form class="form-horizontal" method="post" action="{{ url('produksi/finishing/finish') }}">
                                            {!! csrf_field() !!}

                                            <input type="hidden" name="id_order" value="{{ $ord->id }}">

                                            <h4 class="panel-title">{{ $ord->kode }}</h4>
                                            <input type="hidden" name="produksi[{{ $d }}]" value="{{ $prod->id }}">

                                            <fieldset>
                                                <legend></legend>
                                                <label>NRP : {{ $prod->order->konsumen->nrp }}</label>
                                                <br>
                                                <label>Nama Konsumen : {{ $prod->order->konsumen->nama }}</label>
                                                <br>
                                                <label>Jenis Jahitan : {{ $prod->jenis_jahitan }}</label>
                                                <br>
                                                <label>Dikerjakan Oleh : {{ $prod->jahit ? $prod->jahit->nama : '' }}</label>
                                                <br>
                                                <label>Waktu Pengerjaan : {{ $prod->waktu_mulai_jahit ? getFullDateTime($prod->waktu_mulai_jahit) : '' }}</label>
                                                <br>
                                            </fieldset>
                                            
                                            @if($prod->jahit && $prod->waktu_mulai_jahit)
                                                <button type="submit" class="btn btn-success pull-right">Mulai</button>
                                            @else
                                                <button type="submit" class="btn btn-danger pull-right" disabled="">Mulai</button>
                                            @endif
                                        </form>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        
    </script>
@endsection