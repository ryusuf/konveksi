@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Cutting
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Proses Cutting<small>Mulai</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(count($order) > 0)

                            <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('produksi/cutting/store') }}">

                                {!! csrf_field() !!}

                                <input type="hidden" name="order_id" value="{{ $order->id }}">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Order</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $order->kode }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konsumen</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $order->konsumen ? $order->konsumen->nrp .' - ' : '' }} {{ $order->konsumen ? $order->konsumen->nama : '' }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Jenis Jahitan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $order->jenis_jahitan }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Order</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $order->jumlah }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kuantitas</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ $order->kuantitas }}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                                @foreach($produksi as $prod)
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bagian</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <ul class="to_do">
                                                <li>
                                                    <p>
                                                        &nbsp; {{ $prod->jenis_jahitan }}
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <pemakaian-bahan id="{{ $prod->id }}"></pemakaian-bahan>
                                    <div class="ln_solid"></div>
                                @endforeach
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                        </select2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Mulai</button>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Tidak ada order yang tersedia untuk saat ini.<br><br>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKaryawan();
    </script>
@endsection