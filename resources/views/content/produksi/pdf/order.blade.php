<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

    </head>
    <body style="background: none;">
        <div class="row">
            <div class="col-xs-12" style="margin-left:5%;margin-top: 2%;">
                <table class="col-xs-3">
                    <tr>
                        <td class="text-center" style="text-transform: uppercase;">
                            <p style="font-weight:bold;font-size:24px;text-align:left;">
                                <u>SAN,S TAILOR</u>
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                KOMPLEK GEMPOL ASRI GA1 NO 30
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                CIJERAH-BANDUNG
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                Telp. 022 6124864
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="clearfix"></div>
        <br>
        <br>

        <div class="row">
            <div class="col-xs-12" style="margin-left:4%;">
                <table class="col-xs-12">
                    <tr>
                        <td class="container-fluid" width="15%">
                            No Resi JNE
                        </td>
                        <td class="container-fluid" colspan="6">
                            {{ $order[0]->no_resi }}
                        </td>
                    </tr>

                    <tr>
                        <td class="container-fluid">Tanggal Kirim</td>
                        <td class="container-fluid" colspan="4">{{ getFullDate($order[0]->waktu_mulai_ekspedisi) }}</td>
                    </tr>

                    <tr>
                        <td class="container-fluid" width="15%">
                            <label>No Order</label>
                        </td>
                        <td class="container-fluid" colspan="6">
                            <label>{{ $order[0]->kode }}</label>
                        </td>
                    </tr>

                    @foreach($order as $key => $ord)
                        @if($key == 0)
                            <tr>
                                <td class="container-fluid">
                                    <label>{{ $ord->kode }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->jenis_jahitan }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->nama }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->pangkat }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->nrp }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->fungsi }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->telp_1 }} {{ $ord->konsumen->telp_2 ? '/ '.$ord->konsumen->telp_2 : '' }}</label>
                                </td>
                            </tr>

                            <tr>
                                <td class="container-fluid" colspan="5">
                                    <h5>Grup</h5>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td class="container-fluid">
                                    <label>{{ $ord->kode }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->jenis_jahitan }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->nama }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->pangkat }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->nrp }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->fungsi }}</label>
                                </td>
                                <td class="container-fluid">
                                    <label>{{ $ord->konsumen->telp_1 }} {{ $ord->konsumen->telp_2 ? '/ '.$ord->konsumen->telp_2 : '' }}</label>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>        
            <div class="pull-right text-right" style="margin-right: 10%;text-transform: uppercase;">
                <br>
                <br>
                <br>
                <br>
                <p style="font-size: 32px;font-weight: bold;">KEPADA : {{ $order[0]->konsumen->pangkat }} {{ $order[0]->konsumen->nama }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->tik }} - {{ $order[0]->konsumen->fungsi }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->alamat }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->telp_1 }} {{ $order[0]->konsumen->telp_2 ? '/ '.$order[0]->konsumen->telp_2 : '' }}</p>
                <br>
                <br>
            </div>

        </div>

        {{-- <div class="container body">
            <div class="main_container">

                <div id="app">
                    <div class="accordion">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <div class="col-xs-1">
                                        Kode
                                    </div>
                                    <div class="col-xs-2">
                                        Jenis Jahitan
                                    </div>
                                    <div class="col-xs-1">
                                        NRP
                                    </div>
                                    <div class="col-xs-2">
                                        Konsumen
                                    </div>
                                    <div class="col-xs-1">
                                        Pangkat
                                    </div>
                                    <div class="col-xs-2">
                                        Alamat
                                    </div>
                                    <div class="col-xs-1">
                                        No Resi
                                    </div>
                                    <div class="col-xs-1">
                                        Penerima
                                    </div>
                                    <div class="col-xs-1">
                                        
                                    </div>
                                </div>
                            </div>
                            <a class="panel-heading" href="javascript:void(0);"></a>
                        </div>
                    </div>
                    
                    @foreach($order as $key => $ord)
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-xs-1">
                                    {{ $ord->kode }}
                                </div>
                                <div class="col-xs-2">
                                    
                                </div>
                                <div class="col-xs-1">
                                    {{ $ord->konsumen->nrp }}
                                </div>
                                <div class="col-xs-2">
                                    {{ $ord->konsumen->nama }}
                                </div>
                                <div class="col-xs-1">
                                    {{ $ord->konsumen->pangkat }}
                                </div>
                                <div class="col-xs-2">
                                    {{ $ord->konsumen->alamat }}
                                </div>
                                <div class="col-xs-1">
                                    {{ $ord->no_resi }}
                                </div>
                                <div class="col-xs-1">
                                
                                </div>
                                <div class="col-xs-1">
                                    
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div> --}}

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>