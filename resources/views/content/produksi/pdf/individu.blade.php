<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

    </head>
    <body style="background: none;">
        <div class="row" style="width:100%;">
            <div class="col-xs-12" style="margin-left:10%;margin-top: 5%;">
                <table class="col-xs-3">
                    <tr>
                        <td class="text-center" style="text-transform: uppercase;">
                            <p style="font-weight:bold;font-size:24px;text-align:left;">
                                <u>SAN,S TAILOR</u>
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                KOMPLEK GEMPOL ASRI GA1 NO 30
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                CIJERAH-BANDUNG
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                Telp. 022 6124864
                            </p>

                            <p style="font-size:18px;text-align:left;line-height:15px;">
                                {{ getFullDate($order[0]->waktu_mulai_ekspedisi) }} 30 NOVEMBER 2017
                            </p>

                            <p style="font-size:18px;margin-bottom:20px;text-align:left;line-height:15px;">
                                NO ORDER : {{ $order[0]->kode }}
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="clearfix"></div>
        <br>
        <br>

        <div class="row">
            <div class="pull-right text-right" style="margin-right: 10%;text-transform: uppercase;">
                <br>
                <br>
                <br>
                <br>
                <p style="font-size: 32px;font-weight: bold;">KEPADA : {{ $order[0]->konsumen->pangkat }} {{ $order[0]->konsumen->nama }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->tik }} - {{ $order[0]->konsumen->fungsi }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->alamat }}</p>
                <p style="font-size: 32px;font-weight: bold;">{{ $order[0]->konsumen->telp_1 }} {{ $order[0]->konsumen->telp_2 ? '/ '.$order[0]->konsumen->telp_2 : '' }}</p>
                <br>
                <br>
            </div>

        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>