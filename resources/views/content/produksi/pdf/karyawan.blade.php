<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
         
        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

    </head>
    <body style="background: none;">
        <div class="col-xs-12">
            <table class="col-xs-12">
                <thead>
                    <tr>
                        <th>No Order</th>
                        <th>Jahitan</th>
                        <th>Nama</th>
                        <th>NRP</th>
                        <th>Tlp</th>
                        <th>Pangkat</th>
                        <th>TIK</th>
                        <th>Fungsi</th>
                        <th>Penerima</th>
                        <th style="width: 20%;">TTD</th>
                        <th style="width: 10%;">Tlp Penerima</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order as $ord)
                        <tr style="border-bottom: 1px solid #000;">
                            <td>{{ $ord->kode }}</td>
                            <td>{{ $ord->jahitan }}</td>
                            <td>{{ $ord->konsumen->nama }}</td>
                            <td>{{ $ord->konsumen->nrp }}</td>
                            <td>{{ $ord->konsumen->telp_1 }}</td>
                            <td>{{ $ord->konsumen->pangkat }}</td>
                            <td>{{ $ord->konsumen->tik }}</td>
                            <td>{{ $ord->konsumen->fungsi }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>