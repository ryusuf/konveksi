@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Mulai Produksi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Order</h2>
                        <div class="pull-right">
                            {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#searchModal"><span class="fa fa-search"></span> Pencarian</button> --}}
                            <input class="input-sm form-control" type="text" v-model="params.search" @keyup.enter="doSearchDebounce('mulai-qc')" placeholder="Pencarian">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($permak)
                            @foreach($permak as $idx => $perm)
                                <div class="row">
                                    @foreach($perm as $key => $per)
                                        <div class="well col-md-3" style="padding: 5px;cursor: pointer;" onclick="$(this).find('form').submit();">
                                            <form class="form-horizontal" method="post" action="{{ url('produksi/store') }}">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="id_order" value="{{ $per->id_order }}">
                                                
                                                <button type="submit" class="btn btn-success pull-right">Mulai</button>
                                                <h4 class="panel-title">{{ $per->kode }} {{ $per->tanggal_order }}</h4>

                                                <label>{{ $per->order->konsumen->nama }} ({{ $per->order->konsumen->nrp }}) {{ $per->order->konsumen->tik }} {{ $per->order->konsumen->fungsi }}</label>
                                                
                                                <label>{{ $per->jenis_jahitan }} {{ $per->bahan ? $per->bahan->nama_bahan : '' }} {{ $per->suffix_bahan }}</label>

                                                @if(Auth::user()->karyawan->bagian == 'finishing')
                                                    <label>{{ $per->selesai_jahit ? $per->selesai_jahit->nama : '' }}</label>
                                                @endif

                                                @if($per->tambahan)
                                                    <label>{{ $per->tambahan }}</label>
                                                @endif

                                                <input type="hidden" name="produksi[]" value="{{ $per->id }}"> 
                                            </form>
                                        </div>
                                    @endforeach
                            @endforeach
                        @endif

                        @foreach($produksi as $idx => $ord)
                            <div class="row">
                                @foreach($ord as $key => $prod)
                                    <div class="well col-md-3" style="padding: 5px;cursor: pointer;" onclick="$(this).find('form').submit();">
                                        <form class="form-horizontal" method="post" action="{{ url('produksi/store') }}">
                                            {!! csrf_field() !!}

                                            <input type="hidden" name="id_order" value="{{ $prod->id_order }}">

                                            @if($prod->disabled && auth()->user()->karyawan->bagian != 'finishing')
                                                <button type="submit" class="btn btn-danger pull-right" disabled="">Mulai</button>
                                                <h4 class="panel-title">{{ $prod->kode }} {{ $prod->tanggal_order }}</h4>

                                                <label>{{ $prod->order->konsumen->nama }} {{ $prod->order->konsumen->pangkat }} ({{ $prod->order->konsumen->nrp }}) {{ $prod->order->konsumen->pangkat }} {{ $prod->order->konsumen->tik }}</label>

                                                <label>{{ $prod->jenis_jahitan }} {{ $prod->bahan ? $prod->bahan->nama_bahan : '' }} {{ $prod->suffix_bahan }}</label>
                                                <br>

                                                @if($prod->tambahan)
                                                    <label>{{ $prod->tambahan }}</label>
                                                    <br>
                                                @else
                                                    <br>
                                                @endif

                                                @if(Auth::user()->karyawan->bagian == 'jahit')
                                                    <label>({{ $prod->cutting ? ' - '. $prod->cutting->nama : '' }})</label>
                                                @elseif(Auth::user()->karyawan->bagian == 'finishing')
                                                    <label>({{ $prod->selesai_jahit ? ' - '. $prod->selesai_jahit->nama : '' }})</label>
                                                @endif

                                                <input type="hidden" name="produksi[]" value="{{ $prod->id }}">
                                            @else
                                                <button type="submit" class="btn btn-success pull-right">Mulai</button>
                                                <h4 class="panel-title">{{ $prod->kode }} {{ $prod->tanggal_order }}</h4>

                                                @if($prod->order)
                                                    <label>{{ $prod->order->konsumen->nama }} {{ $prod->order->konsumen->pangkat }} ({{ $prod->order->konsumen->nrp }}) {{ $prod->order->konsumen->tik }} {{ $prod->order->konsumen->fungsi }} </label>
                                                @else
                                                    <label>Order tidak ditemukan</label>
                                                @endif

                                                <label>{{ $prod->jenis_jahitan }} {{ $prod->bahan ? $prod->bahan->nama_bahan : '' }} {{ $prod->suffix_bahan }}</label>

                                                @if($prod->tambahan)
                                                    <label>{{ $prod->tambahan }}</label>
                                                    <br>
                                                @else
                                                    <br><br>
                                                @endif

                                                @if(Auth::user()->karyawan->bagian == 'jahit')
                                                    <label>({{ $prod->cutting ? $prod->cutting->nama : '' }})</label>
                                                @elseif(Auth::user()->karyawan->bagian == 'finishing')
                                                    <label>({{ $prod->jahit ? $prod->jahit->nama : '' }})</label>
                                                    <br>
                                                    <label>{{ $prod->waktu_mulai_jahit ? getFullDateTime($prod->waktu_mulai_jahit) : '' }}</label>
                                                    <br>
                                                    <label>({{ $prod->selesai_jahit->nama }})</label>
                                                @endif

                                                <input type="hidden" name="produksi[]" value="{{ $prod->id }}">
                                            @endif

                                        </form>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach

                        {{-- <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                            @foreach($produksi as $key => $prod)
                                <form class="form-horizontal" method="post" action="{{ url('produksi/store') }}">
                                    {!! csrf_field() !!}

                                    <input type="hidden" name="id_order" value="{{ $prod->id_order }}">

                                    <div class="panel {{ $prod->disabled ? 'panel-danger' : '' }}">
                                        @if($prod->disabled)
                                            <a class="panel-heading" href="javascript:void(0);">
                                                <h4 class="panel-title">{{ $prod->order->kode }}{{ $prod->suffix }} - {{ $prod->jenis_jahitan }} ({{ $prod->order->jumlah }} {{ $prod->order->kuantitas }} {{ $prod->order->jenis_jahitan }})</h4>
                                            </a>
                                        @else
                                            <a class="panel-heading collapsed" role="tab" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo{{ $key }}" aria-expanded="false" aria-controls="collapseTwo">
                                                <h4 class="panel-title">{{ $prod->order->kode }}{{ $prod->suffix }} - {{ $prod->jenis_jahitan }} ({{ $prod->order->jumlah }} {{ $prod->order->kuantitas }} {{ $prod->order->jenis_jahitan }})</h4>
                                            </a>
                                        @endif

                                        <div id="collapseTwo{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                <ul class="to_do">
                                                    <li>
                                                        <p>
                                                            <input type="checkbox" name="produksi[]" class="flat" value="{{ $prod->id }}" {{ $prod->disabled ? 'disabled' : '' }}> {{ $prod->jenis_jahitan }}
                                                            @if(Auth::user()->karyawan->bagian == 'jahit')
                                                                {{ $prod->cutting ? ' - '. $prod->cutting->nama : '' }}
                                                            @elseif(Auth::user()->karyawan->bagian == 'finishing')
                                                                {{ $prod->selesai_jahit ? ' - '. $prod->selesai_jahit->nama : '' }}
                                                            @endif
                                                        </p>
                                                    </li>
                                                </ul>
                                                <button type="submit" class="btn btn-success pull-right">Mulai</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div> --}}
                        
                    </div>
                </div>
            </div>
        </div>

        @include('modal.search', ['url' => url('produksi/mulai')])
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.getKaryawan();
        vueApp.params.search = "{{ $search }}"
        eventHub.$on('get-mulai-qc', function(params) {
            window.location = '?search=' + params;
        })
    </script>
@endsection