@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Jahit
                </h3>
            </div>

            <div class="title_right">
                
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-2 pull-right">
                            <input class="input-sm form-control" type="text" v-model="params.search" @keyup.enter="doSearchDebounce('selesai-produksi')" placeholder="Pencarian">
                        </div>
                        <h2>Proses Jahit<small>Selesai</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(auth()->user()->role_id === 'admin' || auth()->user()->role_id === 'superadmin')
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Pengerjaan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="work_date" class="form-control col-md-4 col-xs-12 date" readonly="" @change="setWorkDate">
                                        <input type="time" name="work_time" class="form-control col-md-3 col-xs-12" v-model="params.work_time">
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="form-group">
                                <selesai-order bagian="jahit" url="produksi/jahit/finish"></selesai-order>
                            </div>
                        </div>

                        {{-- @foreach($order as $key => $ord)
                            <div class="row">
                                @foreach($ord->produksi as $d => $prod)
                                    @if($prod->status_akhir == 3)
                                        <div class="well col-md-3" style="padding: 5px;cursor: pointer;" onclick="$(this).find('form').submit();">
                                            <form class="form-horizontal" method="post" action="{{ url('produksi/jahit/finish') }}">
                                                {!! csrf_field() !!}

                                                <input type="hidden" name="id_order" value="{{ $ord->id }}">

                                                <div class="panel-title">
                                                    @if($prod->jahit && $prod->waktu_mulai_jahit)
                                                        <button type="submit" class="btn btn-sm btn-success pull-right">Selesai</button>
                                                    @else
                                                        <button type="submit" class="btn btn-sm btn-danger pull-right" disabled="">Selesai</button>
                                                    @endif

                                                    <h4>{{ $prod->kode }}</h4>
                                                </div>

                                                <input type="hidden" name="produksi[{{ $d }}]" value="{{ $prod->id }}">

                                                <fieldset>
                                                    <legend></legend>
                                                    <label>{{ $prod->order->konsumen->nama }} ({{ $prod->order->konsumen->nrp }}) {{ $prod->order->konsumen->tik }} {{ $prod->order->konsumen->fungsi }} </label>
                                                    <br>
                                                    <label>{{ $prod->jenis_jahitan }} {{ $prod->bahan['nama_bahan'] }} {{ $prod->suffix_bahan }}</label>
                                                    <br>
                                                    <label>{{ $prod->jahit ? $prod->jahit->nama : '' }} {{ $prod->waktu_mulai_jahit ? getFullDateTime($prod->waktu_mulai_jahit) : '' }}</label>
                                                </fieldset>
                                            </form>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach --}}

                        {{-- @foreach($produksi as $key => $prod)

                                    @if($prod->disabled)
                                        <h4 class="panel-title">{{ $prod->order->kode }}{{ $prod->suffix }}</h4>
                                        <br>
                                        <label>NRP : {{ $prod->order->konsumen->nrp }}</label>
                                        <br>
                                        <label>Nama Konsumen : {{ $prod->order->konsumen->nama }}</label>
                                        <br>
                                        <label>Jenis Jahitan : {{ $prod->jenis_jahitan }}</label>
                                        <br>

                                        @if(Auth::user()->karyawan->bagian == 'jahit')
                                            <label>Karyawan Cutting : {{ $prod->cutting ? ' - '. $prod->cutting->nama : '' }}</label>
                                        @elseif(Auth::user()->karyawan->bagian == 'finishing')
                                            <label>Karyawan Terima Jahit {{ $prod->selesai_jahit ? ' - '. $prod->selesai_jahit->nama : '' }}</label>
                                        @endif

                                        <input type="hidden" name="produksi[]" value="{{ $prod->id }}">

                                        <button type="submit" class="btn btn-danger pull-right" disabled="">Mulai</button>
                                    @else
                                        <h4 class="panel-title">{{ $prod->order->kode }}{{ $prod->suffix }}</h4>
                                        <br>
                                        <label>NRP : {{ $prod->order->konsumen->nrp }}</label>
                                        <br>
                                        <label>Nama Konsumen : {{ $prod->order->konsumen->nama }}</label>
                                        <br>
                                        <label>Jenis Jahitan : {{ $prod->jenis_jahitan }}</label>
                                        <br>

                                        @if(Auth::user()->karyawan->bagian == 'jahit')
                                            <label>Karyawan Cutting : {{ $prod->cutting ? ' - '. $prod->cutting->nama : '' }}</label>
                                        @elseif(Auth::user()->karyawan->bagian == 'finishing')
                                            <label>Karyawan Terima Jahit {{ $prod->selesai_jahit ? ' - '. $prod->selesai_jahit->nama : '' }}</label>
                                        @endif

                                        <input type="hidden" name="produksi[]" value="{{ $prod->id }}">

                                        <button type="submit" class="btn btn-success pull-right">Mulai</button>
                                    @endif

                                
                        @endforeach --}}

                        {{-- <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                            @foreach($order as $key => $ord)
                                <form class="form-horizontal" method="post" action="{{ url('produksi/jahit/finish') }}">
                                    {!! csrf_field() !!}
                                    
                                    <input type="hidden" name="id_order" value="{{ $ord->id }}">

                                    <div class="panel">
                                        <a class="panel-heading collapsed" role="tab" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo{{ $key }}" aria-expanded="false" aria-controls="collapseTwo">
                                            <h4 class="panel-title">{{ $ord->kode }} {{ $ord->jenis_jahitan }}</h4>
                                        </a>
                                        <div id="collapseTwo{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                <ul class="to_do">
                                                    @foreach($ord->produksi as $prod)
                                                        <li>
                                                            <p>
                                                                @if($prod->id_karyawan_jahit)
                                                                    <input type="checkbox" name="produksi[]" class="flat" value="{{ $prod->id }}" @if($prod->waktu_selesai_jahit) disabled="" @endif> {{ $prod->jenis_jahitan }} {{ $prod->jahit ? ' - '. $prod->jahit->nama : '' }}
                                                                @endif
                                                            </p>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                <button type="submit" class="btn btn-success pull-right">Selesai</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endforeach
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        
    </script>
@endsection