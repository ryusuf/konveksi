@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Mulai Produksi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="col-md-2 pull-right">
                            <input class="input-sm form-control" type="text" v-model="params.search" @keyup.enter="doSearchDebounce('produksi')" placeholder="Pencarian">
                        </div>
                        <h2>Daftar Order</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="demo-form2" class="form-horizontal form-label-left" method="post" action="{{ url('produksi/store/manual') }}">

                            {!! csrf_field() !!}

                            <input type="hidden" name="bagian" v-model="params.bagian">

                            {{-- <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bagian <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="bagian" class="form-control col-md-7 col-xs-12" v-model="params.bagian">
                                        <option value=""></option>
                                        <option value="cutting">Cutting</option>
                                        <option value="jahit">Jahit</option>
                                        <option value="finishing">Finishing</option>
                                        <option value="ekspedisi">Ekspedisi</option>
                                    </select>
                                </div>
                            </div> --}}

                            <div class="form-group">
                                <order bagian="{{ $bagian }}"></order>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                    </select2>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <p class="fa fa-refresh fa-2x" style="cursor: pointer;" @click="getKaryawan(params.bagian)"></p>
                                </div>
                            </div>
                            
                            @if(auth()->user()->role_id === 'admin' || auth()->user()->role_id === 'superadmin')
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Pengerjaan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="work_date" class="form-control col-md-4 col-xs-12 date" readonly="">
                                        <input type="time" name="work_time" class="form-control col-md-3 col-xs-12">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Mulai</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.params.bagian = '{{ $bagian }}';
        vueApp.getKaryawan(vueApp.params.bagian);

        vueApp.$watch('params.bagian', function() {
            vueApp.getKaryawan(vueApp.params.bagian);
            eventHub.$emit('get-data-produksi');
        });
    </script>
@endsection