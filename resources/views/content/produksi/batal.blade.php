@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Pembatalan Produksi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="pull-right">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#searchModal"><span class="fa fa-search"></span> Pencarian</button>
                        </div>
                        <h2>Produksi <small>Pembatalan</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="post" action="{{ url('produksi/batal') }}">
                            {!! csrf_field() !!}

                            <div class="accordion">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <div class="col-md-3">
                                                Kode Produksi
                                            </div>
                                            <div class="col-md-3">
                                                Konsumen
                                            </div>
                                            <div class="col-md-6">
                                                Jenis
                                            </div>
                                        </div>
                                    </div>
                                    <a class="panel-heading" href="javascript:void(0);"></a>
                                </div>
                            </div>

                            @if(count($order) == 0)
                                <div class="accordion">
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="col-md-12">
                                                    Silahkan cari proses produksi yang akan dibatalkan
                                                </div>
                                            </div>
                                        </div>
                                        <a class="panel-heading" href="javascript:void(0);"></a>
                                    </div>
                                </div>
                            @endif
                            
                            @foreach($order as $key => $ord)
                                <div class="accordion custom-accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="order[]" class="flat" value="{{ $ord->id }}"> {{ $ord->kode }}
                                                </div>
                                                <div class="col-md-3">
                                                    {{ $ord->konsumen->nama }} ({{ $ord->konsumen->nrp }}) {{ $ord->konsumen->pangkat }} {{ $ord->konsumen->tik }} {{ $ord->konsumen->fungsi }}
                                                </div>
                                                <div class="col-md-3">
                                                    ({{ $ord->qty }}) {{ $ord->jenis_jahitan }} {{ $ord->bahan ? $ord->bahan->nama_bahan : '' }} {{ $ord->suffix_bahan }}
                                                </div>
                                            </div>
                                        </div>
                                        <a class="panel-heading" href="javascript:void(0);"></a>
                                    </div>
                                </div>
                            @endforeach

                            <button type="submit" class="btn btn-success pull-right">Batalkan Produksi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('modal.search', ['url' => url('produksi/batal')])
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });
    </script>
@endsection