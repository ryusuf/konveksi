@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Ekspedisi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Proses Ekspedisi<small>Mulai</small></h2>
                        <a href="{{ url('produksi/ekspedisi/list') }}" class="btn btn-primary pull-right">Daftar Proses Ekspedisi</a>
                        <div class="col-md-2 pull-right">
                            <input class="input-sm form-control" type="text" v-model="params.search" @keyup.enter="doSearchDebounce('produksi')" placeholder="Pencarian">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="post" action="{{ url('produksi/ekspedisi/proses') }}" id="ekspedisi">
                            {!! csrf_field() !!}

                            <input type="hidden" name="proses" value="mulai">
                            
                            @foreach($order as $idx => $ord)
                                <div class="row">
                                    @foreach($ord as $key => $prod)
                                        <div class="well col-md-3 markerDiv" style="{{ $prod->disabled ? 'background-color:#f2dede!important;' : '' }}">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <input type="checkbox" name="produksi[]" value="{{ $prod->id }}" {{ $prod->disabled ? 'disabled' : '' }}>
                                                </div>
                                                <div class="col-md-11">
                                                    <h4 class="panel-title">{{ $prod->kode }} {{ $prod->tanggal_order }}</h4>
                                                    <div class="col-md-6">
                                                        <label>{{ $prod->konsumen->nama }} ({{ $prod->konsumen->nrp }}) {{ $prod->konsumen->tik }} {{ $prod->konsumen->fungsi }}</label>
                                                        <label>{{ $prod->konsumen->alamat }}</label>
                                                        <br>
                                                        <label>{{ $prod->konsumen->telp_1 }} {{ $prod->konsumen->telp_2 ? '/ '. $prod->konsumen->telp_2 : '' }}</label>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach

                            <div class="clearfix"></div>
                            <br>
                            <br>

                            <div class="form-group {{ Auth::user()->role_id == 'karyawan' ? 'hide' : '' }}">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Tipe <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="tipe" class="form-control col-md-7 col-xs-12" v-model="params.tipe_laporan">
                                        <option value="karyawan">Karyawan</option>
                                        <option value="jne">JNE</option>
                                    </select>
                                </div>
                            </div>

                            <template v-if="params.tipe_laporan == 'karyawan'">
                                <div class="form-group {{ Auth::user()->role_id == 'karyawan' ? 'hide' : '' }}">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Karyawan <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                        </select2>
                                    </div>
                                    <div class="col-md-1 col-sm-1">
                                        <p class="fa fa-refresh fa-2x" style="cursor: pointer;" @click="getKaryawan('ekspedisi')"></p>
                                    </div>
                                </div>
                            </template>

                            <template v-if="params.tipe_laporan == 'jne'">
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">No Resi <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="no_resi" class="form-control col-md-7 col-xs-12">
                                        <div class="col-xs-5">
                                            <label><input type="checkbox" name="group" value="group"> Group</label>
                                        </div>
                                    </div>
                                </div>
                            </template>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Alamat</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea name="alamat" class="form-control"></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success pull-right">Mulai</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });

        @if(Auth::user()->role_id == 'karyawan')
            vueApp.params.tipe_laporan = 'karyawan'
            vueApp.params.karyawan_id = '{{ Auth::user()->karyawan->id }}'
        @endif

        $('#ekspedisi').submit(function () {
            var c = confirm('Apakah barang yang akan dikirim sudah sesuai?');
            if(c) {
                location.reload();

                return true;
            }else{
                return false;
            }
        });
    </script>
@endsection