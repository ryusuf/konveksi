@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Ekspedisi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Proses Ekspedisi<small>Selesai</small></h2>
                        <a href="{{ url('produksi/ekspedisi/list') }}" class="btn btn-primary pull-right">Daftar Proses Ekspedisi</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                            
                        @foreach($order as $key => $ord)
                            <div class="row">
                                @foreach($ord->produksi as $d => $prod)
                                    @if($prod->status_akhir == 7)
                                        <div class="well col-md-3">
                                            <div class="row">
                                                <form class="form-horizontal" method="post" action="{{ url('produksi/ekspedisi/proses') }}">
                                                    {!! csrf_field() !!}

                                                    <input type="hidden" name="proses" value="selesai">

                                                    <input type="hidden" name="produksi[]" value="{{ $prod->id }}">
                                                    
                                                    <h4 class="panel-title">&nbsp; {{ $prod->kode }} {{ getFullDate($prod->created_at) }}</h4>
                                                    <div class="col-md-12">
                                                        <label>{{ $prod->order->konsumen->nama }} ({{ $prod->order->konsumen->nrp }}) {{ $prod->order->konsumen->tik }} {{ $prod->order->konsumen->fungsi }} </label>
                                                        <label>{{ $ord->konsumen->alamat }}</label>
                                                        <label>{{ $prod->konsumen->telp_1 }} {{ $prod->konsumen->telp_2 ? '/ '. $prod->konsumen->telp_2 : '' }}</label>
                                                        <label>{{ $prod->qty }} {{ $prod->jenis_jahitan }} {{ $prod->bahan['nama_bahan'] }} {{ $prod->suffix_bahan }}</label>
                                                        <input type="text" name="penerima" class="form-control col-md-7 col-xs-12 inputs" placeholder="Nama Penerima">
                                                        <input type="text" name="telp_penerima" class="form-control col-md-7 col-xs-12 inputs" placeholder="No Telp Penerima">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <br>
                                                    <button type="submit" class="btn btn-success pull-right inputs">Selesai</button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });
    </script>
@endsection