@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Proses Ekspedisi
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Proses Ekspedisi<small>List</small></h2>
                        <a href="{{ url('produksi/manual/ekspedisi') }}" class="btn btn-primary pull-right">Kembali</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="get" action="{{ url('produksi/cetak') }}">

                            @foreach($order as $key => $prod)
                                <div class="well col-md-3">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <input type="checkbox" name="id[]" value="{{ $prod->id }}">
                                        </div>
                                        <div class="col-md-11">
                                            <h4 class="panel-title">{{ $prod->kode }}</h4>
                                            <br>
                                            <div class="col-md-6">
                                                <label>{{ $prod->order->konsumen->nama }} ({{ $prod->order->konsumen->nrp }}) {{ $prod->order->konsumen->tik }} {{ $prod->order->konsumen->fungsi }} </label>
                                                <br>
                                                <label>{{ $prod->jenis_jahitan }} {{ $prod->bahan['nama_bahan'] }}</label>
                                                <br>
                                                <label>{{ $prod->konsumen->alamat }}</label>
                                                <br>
                                                <label>{{ $prod->konsumen->telp_1 }} {{ $prod->konsumen->telp_2 ? $prod->konsumen->telp_2 : '' }}</label>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <button type="submit" class="btn btn-success pull-right">Print</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });
    </script>
@endsection