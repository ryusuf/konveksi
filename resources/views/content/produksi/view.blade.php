@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Lihat Order
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order : {{ $model->jumlah }} {{ $model->kuantitas }} {{ $model->jenis_jahitan }}</h2>
                        <a href="{{ url('produksi/cetak') }}/{{ $model->id }}" class="btn btn-success pull-right"><i class="fa fa-print"></i> Print</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal form-label-left" method="post" action="{{ url('produksi/ekspedisi/proses') }}">

                            {!! csrf_field() !!}
                            <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Order</h4>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kode</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->kode }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Jahitan</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->jenis_jahitan }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->jumlah }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kuantitas</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->kuantitas }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Status</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ getStatusProduksi($model->status_akhir) }}
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ln_solid"></div>

                            <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konsumen</h4>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NRP</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->nrp : '' }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Konsumen</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->nama : '' }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Pangkat</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->pangkat : '' }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No Telp</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->telp_1 : '' }} {{ $model->konsumen ? ' / '.$model->konsumen->telp_2 : '' }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">TIK</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->tik : '' }}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Alamat</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{ $model->konsumen ? $model->konsumen->alamat : '' }}
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="ln_solid"></div>

                            <h4 class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Produksi</h4>
                            @foreach($produksi as $key => $prod)
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ $key+1 }}</label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        {{ $prod->kode }}
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        {{ $prod->jenis_jahitan }}
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <label class="btn btn-success btn-xs">{{ getStatusProduksi($prod->status_akhir) }}</label>
                                    </div>
                                </div>
                            @endforeach

                            <div class="clearfix"></div>
                            <div class="ln_solid"></div>

                            @if(Auth::user()->role_id == 'admin' && $model->status_akhir == 6 || Auth::user()->role_id == 'superadmin' && $model->status_akhir == 6)
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Karyawan</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select2 name="karyawan_id" class="form-control col-md-7 col-xs-12" :options="option_karyawan" v-model="params.karyawan_id" :multiple="false">
                                        </select2>
                                    </div>
                                </div>
                            @else
                                <input type="hidden" name="karyawan_id" value="{{ Auth::user()->karyawan_id }}">
                            @endif

                            @if($model->status_akhir == 7)
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Penerima</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="penerima" class="form-control col-md-7 col-xs-12" placeholder="Nama Penerima">
                                    </div>
                                </div>
                            @endif

                            <input type="hidden" name="id" value="{{ $model->id }}">

                            @if($model->status_akhir == 6)
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-sign-in"></i> Mulai</button>
                            @endif

                            @if($model->status_akhir == 7)
                                <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-sign-in"></i> Selesai</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        vueApp.params.bagian = 'ekspedisi';
        vueApp.getKaryawan(vueApp.params.bagian);
    </script>
@endsection