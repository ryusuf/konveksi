@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Daftar Order
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Order</h2>
                        <div class="pull-right">
                            <a href="{{ url('new-order/create/'.$slug) }}" class="btn btn-sm btn-primary inputs"><i class="fa fa-plus"></i> Tambah Order</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <ajax-table
                                :url="'{{url('new-order/get-data')}}?prefix={{$slug}}'"
                                :oid="'data-order'"
                                :params="params"
                                :config="{
                                    autoload: true,
                                    show_all: false,
                                    has_number: true,
                                    has_entry_page: false,
                                    has_pagination: true,
                                    has_action: true,
                                    has_search_input: true,
                                    has_search_header: false,
                                    custom_header: '',
                                    default_sort: 'kode',
                                    custom_empty_page: false,
                                    search_placeholder: 'Cari',
                                    class: {
                                        table: [],
                                        wrapper: ['table-responsive'],
                                    }
                                }"
                                :rowparams="{}"
                                :rowtemplate="'tr-data-order'"
                                :columns="{kode: 'Kode', nrp: 'NRP', nama:'Nama', harga:'Harga', status_akhir: 'Status'}"
                            >
                            </ajax-table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
