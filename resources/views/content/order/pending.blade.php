@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Pending Order
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order <small>Pending</small></h2>
                        {{-- <a href="{{ url('produksi/ekspedisi/list') }}" class="btn btn-primary pull-right">Daftar Proses Ekspedisi</a> --}}
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="post" action="{{ url('order/pending') }}">
                            {!! csrf_field() !!}

                            <div class="accordion">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <div class="col-md-2">
                                                Kode Order
                                            </div>
                                            <div class="col-md-2">
                                                Jenis Jahitan
                                            </div>
                                            <div class="col-md-2">
                                                Proses Produksi
                                            </div>
                                            <div class="col-md-2">
                                                Keterangan
                                            </div>
                                            <div class="col-md-2">
                                                Status
                                            </div>
                                        </div>
                                    </div>
                                    <a class="panel-heading" href="javascript:void(0);"></a>
                                </div>
                            </div>
                            
                            @foreach($order as $key => $ord)
                                <div class="accordion">
                                    <div class="panel {{ $ord->is_pending == 1 ? 'panel-danger' : '' }}">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="col-md-2">
                                                    <input type="checkbox" name="order[]" class="flat" value="{{ $ord->id }}" {{ $ord->disabled ? 'disabled' : '' }}> {{ $ord->kode }}
                                                </div>
                                                <div class="col-md-2">
                                                    {{ $ord->jenis_jahitan }}
                                                </div>
                                                <div class="col-md-2">
                                                    {{ $ord->proses_produksi }}
                                                </div>
                                                <div class="col-md-2">
                                                    {{ $ord->keterangan_pending }}
                                                </div>
                                                <div class="col-md-2">
                                                    {{ $ord->status_pending }}
                                                </div>
                                            </div>
                                        </div>
                                        <a class="panel-heading" href="javascript:void(0);"></a>
                                    </div>
                                </div>
                            @endforeach

                            <div class="clearfix"></div>
                            <br>
                            <br>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Keterangan Pending <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="keterangan_pending" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Status Pending <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="status_pending" class="form-control col-md-7 col-xs-12">
                                        <option value="">Pilih Status</option>
                                        <option value="0">Batal Pending</option>
                                        <option value="1">Pending</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success pull-right">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });
    </script>
@endsection