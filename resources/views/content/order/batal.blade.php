@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Pembatalan Order
                </h3>
            </div>

            <div class="title_right">
                <div class="form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="pull-right">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#searchModal"><span class="fa fa-search"></span> Pencarian</button>
                        </div>
                        <h2>Order <small>Pembatalan</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Maaf</strong> Terdapat kesalahan pada data masukan Anda.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="post" action="{{ url('order/batal') }}">
                            {!! csrf_field() !!}

                            <div class="accordion">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <div class="col-md-3">
                                                Kode Order
                                            </div>
                                            <div class="col-md-9">
                                                Konsumen
                                            </div>
                                        </div>
                                    </div>
                                    <a class="panel-heading" href="javascript:void(0);"></a>
                                </div>
                            </div>
                            
                            @foreach($order as $key => $ord)
                                <div class="accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="panel-title">
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="order[]" class="flat" value="{{ $ord->id }}"> {{ $ord->kode }}
                                                </div>
                                                <div class="col-md-9">
                                                    {{ $ord->konsumen->nama }} ({{ $ord->konsumen->nrp }}) {{ $ord->konsumen->pangkat }} {{ $ord->konsumen->tik }} {{ $ord->konsumen->fungsi }}
                                                </div>
                                            </div>
                                        </div>
                                        <a class="panel-heading" href="javascript:void(0);"></a>
                                    </div>
                                </div>
                            @endforeach

                            <button type="submit" class="btn btn-success pull-right">Batalkan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('modal.search', ['url' => url('order/batal')])
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        vueApp.$watch('params.tipe_laporan', function() {
            if(vueApp.params.tipe_laporan == 'karyawan') {
                vueApp.getKaryawan('ekspedisi');
            }
        });
    </script>
@endsection