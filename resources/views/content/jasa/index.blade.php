@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Daftar Master Jasa
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar Master Jasa</h2>
                        <div class="pull-right">
                            <a href="{{ url('jasa/create') }}" class="btn btn-sm btn-primary inputs"><i class="fa fa-plus"></i> Tambah Master Jasa</a>

                            @if (Session::has('link'))
                                <a href="{{ session('link','') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> Edit Terakhir</a>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <ajax-table
                                :url="'{{url('jasa/get-data')}}'"
                                :oid="'data-jasa'"
                                :params="params"
                                :config="{
                                    autoload: true,
                                    show_all: false,
                                    has_number: true,
                                    has_entry_page: false,
                                    has_pagination: true,
                                    has_action: true,
                                    has_search_input: true,
                                    has_search_header: false,
                                    custom_header: '',
                                    default_sort: 'jenis_jasa',
                                    custom_empty_page: false,
                                    search_placeholder: 'Cari',
                                    class: {
                                        table: [],
                                        wrapper: ['table-responsive'],
                                    }
                                }"
                                :rowparams="{}"
                                :rowtemplate="'tr-data-jasa'"
                                :columns="{kode: 'Kode', jenis_jasa: 'Jenis Jasa', biaya:'Biaya', jenis_jahitan: 'Jenis Jahitan'}"
                            >
                            </ajax-table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
