@extends('layouts.app')

@section('content')
    <div class="" id="app">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Daftar Hasil Produksi
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lihat <small>Hasil Produksi </small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-wrapper">
                            <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Mulai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="tanggal_mulai" class="form-control col-md-7 col-xs-12" id="tanggal_mulai" readonly="" value="{{ $start }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal Selesai <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="tanggal_selesai" class="form-control col-md-7 col-xs-12" id="tanggal_selesai" readonly="" value="{{ $end }}">
                                    </div>
                                </div>

                                <div class="col-md-offset-3 col-md-6">
                                    <button class="btn btn-primary pull-right" onclick="submit();">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Daftar <small>Hasil Produksi </small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <template v-if="daftar_produksi.data ? daftar_produksi.data.length > 0 : false">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Produksi</th>
                                        <th>Jenis Jahitan</th>
                                        <th>Waktu Pengerjaan</th>
                                        <th>Jumlah</th>
                                        <th>Biaya</th>
                                        <th>Tambahan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(prod, key) in daftar_produksi.data">
                                        <td>@{{ (key + 1) }}</td>
                                        <td>@{{ prod.kode_produksi }}</td>
                                        <td>@{{ prod.jenis_jahitan }}</td>
                                        <td>@{{ prod.waktu }}</td>
                                        <td>@{{ prod.jumlah }}</td>
                                        <td>@{{ prod.biaya }}</td>
                                        <td>@{{ prod.tambahan }}</td>
                                        <td>@{{ prod.total }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">Grand Total</td>
                                        <td>@{{ daftar_produksi.grand_total }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </template>

                    <template v-else>
                        <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                            <div class="panel">
                                <a class="panel-heading collapsed">
                                    <h4 class="panel-title">Data Kosong</h4>
                                </a>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        submit();

        var picker = new Pikaday({
            firstDay: 1, 
            format: 'YYYY-MM-DD', 
            onSelect: function() {
                $('#tanggal_selesai').val('');
            
                this.getMoment().format('DD-MM-YYYY');
                picker2.setMinDate(this.getDate());
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_mulai')
        });

        var picker2 = new Pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
            onSelect: function() {
                this.getMoment().format('DD-MM-YYYY');
            },
            disableDayFn: function (date) {
                var enabled_dates = picker.getDate();
                
                if (moment(date).format("YYYY-MM-DD") < enabled_dates) {
                    return date;
                }
            },
            yearRange: [1940,{{date('Y')-16}}],
            field: document.getElementById('tanggal_selesai')
        });

        function submit() {
            var tgl1 = $('#tanggal_mulai').val();
            var tgl2 = $('#tanggal_selesai').val();

            $.getJSON(vueApp.base_url + '/home/get-produksi', {tanggal_mulai: tgl1, tanggal_selesai: tgl2}, function(data){
                vueApp.daftar_produksi = data;
            });
        }
    </script>
@endsection