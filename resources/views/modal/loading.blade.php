<div class="modal fade" tabindex="-1" role="dialog" id="loadingModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Silahkan Tunggu</h4>
            </div>
            <div class="modal-body form-horizontal form-label-left">
                <center>
                    <p class="fa fa-spinner fa-spin fa-5x"></p>
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->