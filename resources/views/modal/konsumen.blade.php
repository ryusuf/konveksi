<div class="modal fade" tabindex="-1" role="dialog" id="konsumenModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Konsumen</h4>
            </div>
            <div class="modal-body form-horizontal form-label-left">
                <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NRP <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" id="first-name" name="nrp" required="required" class="form-control col-md-7 col-xs-12 inputs">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" id="first-name" name="nama" required="required" class="form-control col-md-7 col-xs-12 inputs">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pangkat <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" id="last-name" name="pangkat" required="required" class="form-control col-md-7 col-xs-12 inputs">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Telepon <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="col-md-4">
                                <input type="text" name="telp1_col1" class="form-control inputs" maxlength="4">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="telp1_col2" class="form-control inputs" maxlength="4">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="telp1_col3" class="form-control inputs" maxlength="5">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="col-md-4">
                                <input type="text" name="telp2_col1" class="form-control inputs" maxlength="4">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="telp2_col2" class="form-control inputs" maxlength="4">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="telp2_col3" class="form-control inputs" maxlength="5">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">TIK <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="tik" required="required" class="form-control col-md-7 col-xs-12 inputs">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fungsi <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="fungsi" required="required" class="form-control col-md-7 col-xs-12 inputs">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Alamat <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control inputs" name="alamat" rows="3" required="required"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary inputs" onkeyup="enterKonsumen(event)" onclick="saveKonsumen()">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->