
<script>

    function enterKonsumen(e) {
        if(e.which == 13) {
            saveKonsumen();
        }
    }

	function saveKonsumen() {
        var nrp = $('input[name=nrp]').val();
        var nama = $('input[name=nama]').val();
        var pangkat = $('input[name=pangkat]').val();
        var telp1_col1 = $('input[name=telp1_col1]').val();
        var telp1_col2 = $('input[name=telp1_col2]').val();
        var telp1_col3 = $('input[name=telp1_col3]').val();
        var telp2_col1 = $('input[name=telp2_col1]').val();
        var telp2_col2 = $('input[name=telp2_col2]').val();
        var telp2_col3 = $('input[name=telp2_col3]').val();
        var tik = $('input[name=tik]').val();
        var fungsi = $('input[name=fungsi]').val();
        var alamat = $('textarea[name=alamat]').val();

        var token = '{{ csrf_token() }}';

        $.post('{{ url('konsumen/store') }}', {nrp: nrp, nama: nama, pangkat: pangkat, telp1_col1: telp1_col1, telp1_col2: telp1_col2, telp1_col3: telp1_col3, telp2_col1: telp2_col1, telp2_col2: telp2_col2, telp2_col3: telp2_col3, tik: tik, fungsi: fungsi, alamat: alamat, _token: token}, function(data) {

            notify("Info", data.message, data.message_type);

            if(data.message_type == 'ok'){
                vueApp.getKonsumen();

                $('#konsumenModal').modal('hide');
            }
        });
    }

    $('#konsumenModal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea,select")
                .val('')
                .end()
            .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();

        vueApp.modalValue = 0;
    })

    function enterJasa(e) {
        if(e.which == 13) {
            saveJasa();
        }
    }

    function saveJasa() {
        var bagian = $('select[name=bagian]').val();
        var jenis_jahitan = $('input[name=jenis_jahitan]').val();
        var biaya = $('input[name=biaya]').val();
        var satuan = $('input[name=satuan]').val();

        var token = '{{ csrf_token() }}';

        $.post('{{ url('jasa/store') }}', {bagian: bagian, jenis_jahitan: jenis_jahitan, biaya: biaya, satuan: satuan, _token: token}, function(data) {

            notify("Info", data.message, data.message_type);

            if(data.message_type == 'ok'){
                vueApp.getJahitan();

                $('#jasaModal').modal('hide');
            }
        });
    }

    $('#jasaModal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea,select")
                .val('')
                .end()
            .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();

        vueApp.modalValue = 0;
    })

    function enterBahan(e) {
        if(e.which == 13) {
            saveBahan();
        }
    }

    function saveBahan() {
        var nama_bahan = $('input[name=nama_bahan]').val();
        var harga = $('input[name=harga]').val();

        var token = '{{ csrf_token() }}';

        $.post('{{ url('bahan/store') }}', {nama_bahan: nama_bahan, harga: harga, _token: token}, function(data) {

            notify("Info", data.message, data.message_type);

            if(data.message_type == 'ok'){
                window.eventHub.$emit('refresh-bahan');

                $('#bahanModal').modal('hide');
            }
        });
    }

    $('#bahanModal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea,select")
                .val('')
                .end()
            .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();

        vueApp.modalValue = 0;
    })

    function enterTambahan(e) {
        if(e.which == 13) {
            saveTambahan();
        }
    }

    function saveTambahan() {
        var bagian = $('select[name=bagian2]').val();
        var jenis_tambahan = $('input[name=jenis_tambahan]').val();
        var biaya = $('input[name=biaya]').val();

        var token = '{{ csrf_token() }}';

        $.post('{{ url('tambahan/store') }}', {bagian: bagian, jenis_tambahan: jenis_tambahan, biaya: biaya, _token: token}, function(data) {

            notify("Info", data.message, data.message_type);

            if(data.message_type == 'ok'){
                vueApp.getTambahan();

                $('#tambahanModal').modal('hide');
            }
        });
    }

    $('#tambahanModal').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input,textarea,select")
                .val('')
                .end()
            .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();

        vueApp.modalValue = 0;
    })

    function enterSearch(e) {
        if(e.which == 13) {
            saveSearch();
        }
    }

    function saveSearch() {
        var no_order = $('#no_order').val();
        var nama_konsumen = $('#nama_konsumen').val();
        var nrp = $('#nrp').val();

        $.get('{{ url('produksi/mulai') }}', {no_order: no_order, nama_konsumen: nama_konsumen, nrp: nrp}, function(data) {

        });
    }
</script>