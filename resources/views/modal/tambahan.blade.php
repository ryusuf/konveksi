<div class="modal fade" tabindex="-1" role="dialog" id="tambahanModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Tambahan</h4>
            </div>
            <div class="modal-body form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bagian <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select name="bagian2" class="form-control inputs col-md-7 col-xs-12">
                            <option value=""></option>
                            <option value="cutting">Cutting</option>
                            <option value="jahit">Jahit</option>
                            <option value="finishing">Finishing</option>
                            <option value="ekspedisi">Ekspedisi</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jenis Tambahan <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" id="last-name" name="jenis_tambahan" class="form-control inputs col-md-7 col-xs-12">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Harga</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <currency-input kode="Rp." v-model="modalValue" :name="'biaya'"></currency-input>
                        <input :value="modalValue" type="hidden" name="biaya" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary inputs" onkeyup="enterTambahan(event)" onclick="saveTambahan()">Simpan</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->