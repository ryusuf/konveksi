/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
require('./select2')

window._ = require('lodash')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('ajax-table', require('./components/AjaxTable.vue'))
Vue.component('sorter', require('./components/Sorter.vue'))
Vue.component('select2', require('./components/Select2.vue'))

Vue.component('input-dinamis', require('./components/InputDinamis.vue'))
Vue.component('pemakaian-bahan', require('./components/PemakaianBahan.vue'))

Vue.component('input-dinamis-custom', require('./components/InputDinamisCustom.vue'))
Vue.component('pemakaian-bahan-custom', require('./components/PemakaianBahanCustom.vue'))
Vue.component('currency-input', require('./components/InputCurrency.vue'))

Vue.component('order', require('./components/Order.vue'))
Vue.component('selesai-order', require('./components/SelesaiOrder.vue'))
Vue.component('borongan', require('./components/Borongan.vue'))

Vue.component('tr-data-karyawan', require('./components/table-row/DataKaryawan.vue'))
Vue.component('tr-data-konsumen', require('./components/table-row/DataKonsumen.vue'))
Vue.component('tr-data-bahan', require('./components/table-row/DataBahan.vue'))
Vue.component('tr-data-jasa', require('./components/table-row/DataJasa.vue'))
Vue.component('tr-data-order', require('./components/table-row/DataOrder.vue'))
Vue.component('tr-data-order-borongan', require('./components/table-row/DataOrderBorongan.vue'))
Vue.component('tr-data-tambahan', require('./components/table-row/DataTambahan.vue'))
Vue.component('tr-data-progress', require('./components/table-row/DataProgress.vue'))
Vue.component('tr-data-absen', require('./components/table-row/DataAbsen.vue'))
Vue.component('tr-data-gaji', require('./components/table-row/DataGaji.vue'))

Vue.component('header-progress', require('./components/table-header/HeaderProgress.vue'))
Vue.component('header-absen', require('./components/table-header/HeaderAbsen.vue'))
Vue.component('header-gaji', require('./components/table-header/HeaderGaji.vue'))

window.vueApp = new Vue({
	el: '#app',
	data: {
		params: {
			konsumen_id: '',
			karyawan_id: '',
			bahan_id: '',
			order_id: '',
			bagian: '',
			bagian_array: [],
			tipe_laporan: '',
			tambahan_id: [],
			tanggal_mulai: '',
			tanggal_selesai: '',
			tunjangan: [],
			search: '',
			work_date: '',
			work_time: '',
		},
		detail_order: {},
		option_konsumen: [],
		option_tambahan: [],
		option_jenis_jahitan: [],
		option_karyawan: [],
		option_bahan: [],
		option_order: [],
		satuan_bahan: '',
		base_url: $('#base_url').val(),
		csrf_token: $('meta[name="csrf-token"]').attr('content'),
		daftar_produksi: {},
		gaji: 0,
		modalValue: 0,
		kasbon: 0,
		isLoading: false,
		dataBonus: [],
	},
	methods: {
		refreshTable: function(oid) {
			window.eventHub.$emit('refresh-ajaxtable', oid)
		},
		getKonsumen: function() {
			var that = this
			this.isLoading = true
			$.getJSON(this.base_url + '/konsumen/get-konsumen', function(data) {
				that.option_konsumen = []
				$.each(data, function(key, val) {
					that.option_konsumen.push({ id: key, text: String(val) })
				})
				that.isLoading = false
			})
		},
		getJahitan: function() {
			var that = this
			this.isLoading = true
			$.getJSON(this.base_url + '/jasa/get-jahitan', function(data) {
				that.option_jenis_jahitan = []
				$.each(data, function(key, val) {
					that.option_jenis_jahitan.push({ id: key, text: String(val) })
				})
				that.isLoading = false
			})
		},
		getOrder: function() {
			var that = this
			$.getJSON(this.base_url + '/order/get-order', function(data) {
				$.each(data, function(key, val) {
					that.option_order.push({ id: key, text: String(val) })
				})
			})
		},
		getOrderDetail: function() {
			var that = this
			$.getJSON(
				this.base_url + '/order/get-order-detail',
				{ order_id: this.params.order_id },
				function(data) {
					that.detail_order = data
				},
			)
		},
		getKaryawan: function(data) {
			var that = this
			this.isLoading = true
			$.getJSON(this.base_url + '/karyawan/get-karyawan', { bagian: data }, function(data) {
				that.option_karyawan = []
				$.each(data, function(key, val) {
					that.option_karyawan.push({ id: key, text: String(val) })
				})
				that.isLoading = false
			})
		},
		getBahan: function() {
			var that = this
			this.isLoading = true
			$.getJSON(this.base_url + '/bahan/get-bahan', function(data) {
				that.option_bahan = []
				$.each(data, function(val, key) {
					that.option_bahan.push({ id: key, text: String(val) })
				})
				that.isLoading = false
			})
		},
		getSatuanBahan: function() {
			var that = this
			this.isLoading = true
			$.getJSON(
				this.base_url + '/bahan/get-satuan-bahan',
				{ bahan_id: this.params.bahan_id },
				function(data) {
					that.satuan_bahan = data.satuan
					that.isLoading = false
				},
			)
		},
		getTambahan: function() {
			var that = this
			this.isLoading = true
			$.getJSON(this.base_url + '/tambahan/get-tambahan', function(data) {
				that.option_tambahan = []
				$.each(data, function(key, val) {
					that.option_tambahan.push({ id: key, text: String(val) })
				})
				that.isLoading = false
			})
		},
		getBonus: function() {
			var that = this
			this.isLoading = true
			$.post(
				this.base_url + '/laporan/get-bonus',
				{
					karyawan_id: that.params.karyawan_id,
					bagian: that.params.bagian,
					_token: that.csrf_token,
				},
				function(data) {
					that.dataBonus = data
					that.isLoading = false
				},
			)
		},
		doSearchDebounce: _.debounce(function(data) {
			if (data == 'produksi') {
				eventHub.$emit('get-data-produksi', this.params.search)
			} else if (data == 'selesai-produksi') {
				eventHub.$emit('get-data-selesai-produksi', this.params.search)
			} else if (data == 'mulai-qc') {
				eventHub.$emit('get-mulai-qc', this.params.search)
			}
		}, 500),
		setWorkDate($event) {
			this.params.work_date = $($event.target).val()
		},
	},
	watch: {
		isLoading: function(value, oldVal) {
			if (value) {
				$('#loadingModal').modal('show')
			} else {
				$('#loadingModal').modal('hide')
			}
		},
	},
})
window.eventHub = new Vue()
