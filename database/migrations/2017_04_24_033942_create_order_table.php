<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('prefix', 1)->default('P');
            $table->increments('id', 5)->zero_fill();
            $table->integer('id_konsumen')->unsigned()->index();
            $table->string('jenis_jahitan');
            // $table->decimal('harga', 20, 2);
            $table->string('jumlah');
            $table->string('kuantitas');
            $table->integer('status_akhir')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['prefix', 'id']);
        });

        DB::statement('ALTER TABLE orders CHANGE id id INT(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
