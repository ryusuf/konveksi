<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJasaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_jasa', function (Blueprint $table) {
            $table->string('prefix', 1)->default('J');
            $table->increments('id', 5)->zero_fill();
            $table->string('jenis_jasa');
            $table->decimal('biaya', 20, 2)->nullalbe();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['prefix', 'id']);
        });

        DB::statement('ALTER TABLE m_jasa CHANGE id id INT(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_jasa');
    }
}
