<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProduksiAddFlagSkip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->integer('is_skip')->default(0);
        });

        Schema::table('produksi', function(Blueprint $table) {
            $table->integer('is_skip')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('is_skip');
        });

        Schema::table('produksi', function(Blueprint $table) {
            $table->dropColumn('is_skip');
        });
    }
}
