<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAbsenAddLembur2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('karyawan_absen', function(Blueprint $table) {
            $table->integer('jam_lembur')->default(0)->nullable();
            $table->integer('jam_lembur2')->default(0)->nullable();
            $table->integer('is_telat')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('karyawan_absen', function(Blueprint $table) {
            $table->dropColumn('jam_lembur');
            $table->dropColumn('jam_lembur2');
            $table->dropColumn('is_telat');
        });
    }
}
