<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterBahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_bahan', function (Blueprint $table) {
            $table->string('prefix', 1)->default('A');
            $table->increments('id', 5)->zero_fill();
            $table->string('nama_bahan');
            $table->string('satuan')->nullable();
            $table->decimal('harga', 20, 2);
            $table->string('jenis')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['prefix', 'id']);
        });

        DB::statement('ALTER TABLE m_bahan CHANGE id id INT(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_bahan');
    }
}
