<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUkuranBahanToJasaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_bahan', function ($table) {
            $table->dropColumn('satuan');
            $table->dropColumn('jenis');
            $table->dropColumn('jenis_model');
        });

        Schema::table('m_jasa', function ($table) {
            $table->string('satuan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
