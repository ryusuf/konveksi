<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan_absen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('karyawan_id')->unsigned()->index();
            $table->date('tanggal');
            $table->string('jam_kerja');
            $table->string('jam_lembur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('karyawan_absen');
    }
}
