<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterPrefix2Char extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE orders CHANGE prefix prefix varchar(5)');
        DB::statement('ALTER TABLE produksi CHANGE prefix prefix varchar(5)');

        Schema::table('orders', function ($table) {
            $table->integer('increment_borongan')->nullable()->default(null);
            $table->string('keterangan_borongan')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
