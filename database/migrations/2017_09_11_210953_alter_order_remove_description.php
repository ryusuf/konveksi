<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderRemoveDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($table) {
            $table->dropColumn('jenis_jahitan');
            $table->dropColumn('jumlah');
            $table->dropColumn('kuantitas');
            $table->dropColumn('id_tambahan');
        });

        Schema::table('produksi', function ($table) {
            $table->text('id_tambahan')->nullable()->default(null);
            $table->integer('karyawan_permak')->unsigned()->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
