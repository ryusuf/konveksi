<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterProduksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produksi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prefix', 1)->default('P');
            $table->integer('id_order')->length(5)->zero_fill()->index();
            $table->string('suffix', 1)->default('A');
            $table->string('jenis_jahitan')->nullable();
            $table->integer('id_konsumen')->unsigned()->index()->nullable();

            
            $table->integer('id_bahan')->unsigned()->index()->nullable();
            $table->string('pemakaian_bahan')->nullable();

            // $table->integer('id_bahan_cutting')->unsigned()->index()->nullable();
            // $table->string('pemakaian_bahan_cutting')->nullable();
            $table->dateTime('waktu_mulai_cutting')->nullable();
            // $table->integer('id_user_mulai_cutting')->unsigned()->index()->nullable();
            $table->dateTime('waktu_selesai_cutting')->nullable();
            $table->integer('id_user_selesai_cutting')->unsigned()->index()->nullable();
            $table->integer('id_karyawan_cutting')->unsigned()->index()->nullable();

            // $table->integer('id_bahan_jahit')->unsigned()->index()->nullable();
            // $table->string('pemakaian_bahan_jahit')->nullable();
            $table->dateTime('waktu_mulai_jahit')->nullable();
            // $table->integer('id_user_mulai_jahit')->unsigned()->index()->nullable();
            $table->dateTime('waktu_selesai_jahit')->nullable();
            $table->integer('id_user_selesai_jahit')->unsigned()->index()->nullable();
            $table->integer('id_karyawan_jahit')->unsigned()->index()->nullable();

            // $table->integer('id_bahan_packing')->unsigned()->index()->nullable();
            // $table->string('pemakaian_bahan_packing')->nullable();
            $table->dateTime('waktu_mulai_packing')->nullable();
            // $table->integer('id_user_mulai_packing')->unsigned()->index()->nullable();
            $table->dateTime('waktu_selesai_packing')->nullable();
            $table->integer('id_user_selesai_packing')->unsigned()->index()->nullable();
            $table->integer('id_karyawan_packing')->unsigned()->index()->nullable();

            // $table->integer('id_bahan_ekspedisi')->unsigned()->index()->nullable();
            // $table->string('pemakaian_bahan_ekspedisi')->nullable();
            $table->dateTime('waktu_mulai_ekspedisi')->nullable();
            // $table->integer('id_user_mulai_ekspedisi')->unsigned()->index()->nullable();
            $table->dateTime('waktu_selesai_ekspedisi')->nullable();
            $table->integer('id_user_selesai_ekspedisi')->unsigned()->index()->nullable();
            $table->integer('id_karyawan_ekspedisi')->unsigned()->index()->nullable();

            // $table->integer('id_bahan_finishing')->unsigned()->index()->nullable();
            // $table->string('pemakaian_bahan_finishing')->nullable();
            $table->dateTime('waktu_mulai_finishing')->nullable();
            // $table->integer('id_user_mulai_finishing')->unsigned()->index()->nullable();
            $table->dateTime('waktu_selesai_finishing')->nullable();
            $table->integer('id_user_selesai_finishing')->unsigned()->index()->nullable();
            $table->integer('id_karyawan_finishing')->unsigned()->index()->nullable();

            $table->integer('status_akhir')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['prefix', 'id_order', 'suffix']);
        });

        DB::statement('ALTER TABLE produksi CHANGE id_order id_order INT(5) UNSIGNED ZEROFILL NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produksi');
    }
}
