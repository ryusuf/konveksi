<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RefreshPresensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:presensi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh presensi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\RefreshPresensi());
    }
}
