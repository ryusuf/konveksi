<?php

namespace App\Http\Controllers;

use App\Model\DynamicOrder;
use App\Model\MReferensi;
use App\Model\Order;
use App\Model\Produksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class newOrderController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index($slug)
    {
    	return view('content.order.dynamic_index', compact('slug'));
    }

    public function create($slug)
    {
        $increment = MReferensi::where('flag', 'increment_'.$slug)->first();

        $prefix = MReferensi::where('flag', 'prefix_'.$slug)->first();
        $kode = $prefix->value.str_pad($increment->value, 5, '0', STR_PAD_LEFT);

    	return view('content.order.dynamic_create', compact('slug', 'kode'));
    }

    public function tambahKode(Request $request)
    {
    	return view('content.order.tambah_kode');
    }

    public function saveKode(Request $request)
    {
    	$model = new MReferensi;

		$error = $this->validate($request, [
    		'nama_order'	=>  'required',
    		'kode_order'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $exists = DynamicOrder::where('nama_Order', $request->nama_order)->count();
        
        if($exists == 0) {
        	$new = new DynamicOrder;
        	$new->nama_order = $request->nama_order;
        	$new->kode_Order = $request->kode_Order;
        	$new->prefix_Order = 'prefix_'.str_slug($request->nama_order, '_');
        	$new->save();

        	$prefix = new MReferensi;
        	$prefix->flag = 'prefix_'.str_slug($request->nama_order, '_');
        	$prefix->value = $request->kode_order;
        	$prefix->save();

        	$increment = new MReferensi;
        	$increment->flag = 'increment_'.str_slug($request->nama_order, '_');
        	$increment->value = 1;
        	$increment->save();
        }

        return redirect()->back()->with([
            'message' => trans('Kode order telah ditambahkan'),
        ]);
    }

    public function getData(Request $request)
    {
    	$prefix = $request->prefix;
    	$ref = MReferensi::where('flag', 'prefix_'.$prefix)->first();

    	$params = $request->get('params',false);
        $models = Order::with('konsumen')
            ->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))
            ->where('prefix', $ref->value)
            ->where('status_akhir', '!=', 8);

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'kode':
                        $models = $models->where(DB::raw('CONCAT(prefix,increment_id)'), $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,increment_id)'), 'like', '%'.$search.'%')
                    ->orWhereHas('konsumen', function($que) use($search) {
                        $que->where('nama','like',"%$search%");
                    });
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            $bg = 'btn-default';
            if($model->status_akhir == 1 || $model->status_akhir == 2) {
                $bg = 'btn-success';
            }elseif($model->status_akhir == 3 || $model->status_akhir == 4) {
                $bg = 'btn-info';
            }elseif($model->status_akhir == 5 || $model->status_akhir == 6) {
                $bg = 'btn-danger';
            }elseif($model->status_akhir == 7 || $model->status_akhir == 8) {
                $bg = 'btn-warning';
            }

            $model->bg_color = $bg;
        	$model->status_akhir = getStatusProduksi($model->status_akhir);
            $model->harga = "Rp ".number_format($model->harga, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function store(Request $request)
    {
    	$model = new Order;

		$error = $this->validate($request, [
    		'konsumen_id'	=>  'required',
    		// 'jenis_jahitan'	=>  'required',
    		// 'harga'	=>  'required',
    		// 'jumlah'	=>  'required',
    		// 'kuantitas'	=>  'required',
    		'jenis_jahitan_detail'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $increment = MReferensi::where('flag', 'increment_'.$request->prefix)->first();

        $prefix = MReferensi::where('flag', 'prefix_'.$request->prefix)->first();

        $model->prefix = $prefix->value;
        $model->increment_id = $increment->value;
        $model->id_konsumen = $request->get('konsumen_id', '');
        $model->harga = $request->get('harga', 0);
        $model->status_akhir = $request->prefix == 'permak' ? 2 : 0;
        $model->created_at = $request->tanggal_order;
        $model->is_skip = $request->is_skip == 1 ? 1 : 0;
        $model->is_grup = $request->is_grup;
        $model->save();

    	$abjad = 1;
        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
        	$produksi = new Produksi;
            $produksi->prefix = $prefix->value;
        	$produksi->id_order = $model->id;
        	$produksi->suffix = chr(64 + $abjad);
        	$produksi->id_konsumen = $model->id_konsumen;
        	$produksi->jenis_jahitan = $value;
        	$produksi->status_akhir = $request->prefix == 'permak' ? 2 : 0;;
            $produksi->id_bahan = $request->bahan_id[$idx];
            $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
            $produksi->suffix_bahan = $request->suffix_bahan[$idx];
            $produksi->qty = $request->qty[$idx];
            $produksi->increment_id = $increment->value;
            $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;

            if ($request->prefix == 'permak') {
            	$produksi->karyawan_permak = $request->karyawan_id;
            }

            if(!empty($request->tambahan)) {
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
            }

        	$produksi->save();

        	$abjad++;
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        $add = MReferensi::where('flag', 'increment_'.$request->prefix)->first();
        $add->value = $add->value+1;
        $add->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah ditambahkan'),
            'link' => url('order/edit').'/'.$model->id,
        ]);
    }

    public function update(Request $request)
    {
    	$model = Order::find($request->id);

    	$error = $this->validate($request, [
            'konsumen_id'   =>  'required',
            // 'jenis_jahitan' =>  'required',
            // 'harga'  =>  'required',
            // 'jumlah'    =>  'required',
            // 'kuantitas' =>  'required',
            'jenis_jahitan_detail'  =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->id_konsumen = $request->get('konsumen_id', '');
        $model->harga = $request->get('harga', 0);
        $model->created_at = $request->tanggal_order;
        $model->is_grup = $request->is_grup;
        $model->is_skip = $request->is_skip == 1 ? 1 : 0;
        $model->save();

    	$ord = Produksi::where('id_order', $request->id)->orderBy('id', 'DESC')->first();
        $abjad = ord(strtolower($ord->suffix)) - 96;

        $ex = Produksi::where('id_order', $request->id);
        $diff2 = array_diff($ex->pluck('id')->toArray(), $request->produksi_id);

        if($diff2) {
            $rem = Produksi::whereIn('id', $diff2)->delete();
        }

        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
            if($request->produksi_id[$idx]) {
                $produksi = Produksi::find($request->produksi_id[$idx]);
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;
                $produksi->increment_id = $model->increment_id;

                if ($request->prefix == 'permak') {
	            	$produksi->karyawan_permak = $request->karyawan_id;
	            }
                
                $produksi->save();
            }else{
                $produksi = new Produksi;
                $produksi->prefix = $model->prefix;
                $produksi->id_order = $model->id;
                $produksi->suffix = chr(64 + ($abjad+1));
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->status_akhir = $request->prefix == 'permak' ? 2 : 0;;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;
                $produksi->increment_id = $model->increment_id;

                if ($request->prefix == 'permak') {
	            	$produksi->karyawan_permak = $request->karyawan_id;
	            }
	            
                $produksi->save();

                $abjad++;
            }
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah diubah'),
            'link' => url('order/edit').'/'.$model->id,
        ]);
    }
}
