<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Jobs\RefreshPresensi;
use PDO;
use PDOException;
use App\Model\SatuanKerja;
use App\Model\UnitKerja;
use App\Model\Pegawai;
use App\Model\UserID;
use App\Model\Ijin;

class ApiController extends Controller
{
    public function syncMesin()
    {
        $host = config('database.connections.mysql.host');
        $dbname = config('database.connections.mysql.database');
        $user = config('database.connections.mysql.username');
        $pass = config('database.connections.mysql.password');

        try{
            $db = new PDO("mysql:dbname=$dbname;host=$host", $user, $pass);
        }catch(PDOException $e){
            die($e);
        }

        $result = file_get_contents('php://input');
        $response = json_decode( $result, true );

        //var_dump($response['param']['machineNum']);

        try{
            $app_api_key = 'DCl9cp4y0MkmUWvfnwk85t6HZOBfTs';
            $bar = '"MachineNumber"';
            $query = $db->prepare("Select * from machines where ".$bar."='".$response['param']['machineNum']."'");

            if($query->execute()){
                $result = $query->fetch(PDO::FETCH_OBJ);
                if (!$result) {
                    die();
                }
                $device = $result->MachineNumber;
                $api_key = $result->API_KEY;

                $time = $response['param']['time'];
                $key = $response['param']['key'];

                if ($time < time() - 6000){
                    die();
                }

                if (!$device){
                    die();
                }

                if (md5($app_api_key. $api_key . $time) != $key){
                    die();
                }

                //routine untuk update waktu terakhir yang sukses input data ke table machines
                if($response['success']=='true'){
                    try{
                        $foo = '"FirmwareVersion"';
                        $bar = '"MachineNumber"';
                        $rec = $db->prepare("UPDATE machines SET ".$foo." = NOW() WHERE ".$bar." = '".$response['param']['machineNum']."'");

                        if($rec->execute()){
                            var_dump("sukses");
                        }else{
                            var_dump("gagal");
                        }
                    }catch(PDOException $e){
                        echo $e->getMessage();
                    }
                }

                //routine untuk posting data ke table public.checkinout
                if($response["success"] == 'true'){
                    try{
                        $rec = $db->prepare("INSERT into public.checkinout(userid,checktime,sensorid) 
                            select :userid,:checktime,:sensorid where not exists (
                                select userid,checktime,sensorid from public.checkinout where userid=:userid and checktime=:checktime and sensorid=:sensorid)");

                        foreach($response["data"] as $k=>$v){
                            $rec->bindParam(":userid",$v["userid"]);
                            $rec->bindParam(":checktime",$v["checktime"]);
                            $rec->bindParam(":sensorid",$v["sensorid"]);
                            
                            $sql = "INSERT into public.checkinout(userid,checktime,sensorid) 
                            select ".$v["userid"].",'".$v["checktime"]."','".$v["sensorid"]."' where not exists (
                                select userid,checktime,sensorid from public.checkinout where userid=".$v["userid"]." and checktime='".$v["checktime"]."' and sensorid='".$v["sensorid"]."')";
                            
                            if($db->query($sql)){
                                var_dump("sukses");
                            }else{
                                var_dump("gagal");
                            }
                        }
                    }catch(PDOException $e){
                        die('tera');
                    }
                }
                //routine untuk update jumlah record yg di post ke table public.checkinout pada waktu import
                try{
                    $rec = $db->prepare('UPDATE machines SET "SecretCount" = :jumlah, "FirmwareVersion"= now() WHERE "MachineNumber" = :machineNumber');
                    $rec->bindParam(":jumlah",count($response["data"]));
                    $rec->bindParam(":machineNumber",$response["param"]["machineNum"]);
                    if($rec->execute()){
                        var_dump("sukses");
                    }else{
                        var_dump("gagal");
                    }
                    $this->dispatch(new RefreshPresensi());
                }catch(PDOException $e){
                    echo $e->getMessage();
                }
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    
    public function getTpp(Request $req)
    {
        $nip = $req->get('nip');
        $tahun = $req->get('tahun',date('Y'));
        $bulan = $req->get('bulan',date('m'));
        $start = date("Y-m-d",strtotime("$tahun-$bulan-01"));
        $end = date("Y-m-d",strtotime("$start +1 month"));
        
        return with(new PresensiController)->getPresensiDetail($nip,$start,$end);
    }

    public function getSatuanKerja(){
        $model = SatuanKerja::where('satuan_kerja_nama','not like', '%-%')->orderBy('satuan_kerja_nama')->lists('satuan_kerja_nama','satuan_kerja_id');

        echo $_GET['callback']. '(' . json_encode($model) . ');';
    }

    public function getSatuanOrganisasi(Request $req){
        $input_data = $req->except('_');
        $id = $input_data['satuan_kerja_id'];

        $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->where('unit_kerja_level', 1)->get();
  
        echo $_GET['callback']. '(' . json_encode($unit_kerja) . ');';
    }

    public function getUnitKerja(Request $req){
        $input_data = $req->except('_');
        $id = $input_data['satuan_kerja_id'];
        $parent = $input_data['unit_kerja_parent'];

        if($parent != null){
            $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->where('unit_kerja_parent', $parent)->get();
        }else{
            $unit_kerja = UnitKerja::where('satuan_kerja_id', $id)->get();
        }
         echo $_GET['callback']. '(' . json_encode($unit_kerja) . ');';
    }

    public function getDaftarHadir(Request $req, $tanggal_from, $tanggal_to){
        $pegawais = Pegawai::where('peg_status', true);
        $params = $req->get('params',false);
        
        if ($params) {
            foreach ($params as $key => $search) {
                if ($search == '' || $search == 0) continue;
                switch($key) {
                    case 'search':
                        $pegawais->where(function($q) use ($search) {
                            $q->where('peg_nip','like',"%$search%")
                              ->orWhere('peg_nama','like',"%$search%");
                        });
                        break;
                    case 'peg_nip':
                    case 'peg_nama':
                        $pegawais->where($key,'like',"%$search%");
                        break;
                    case 'satuan_organisasi_id':
                        $pegawais->whereIn('unit_kerja_id',getAllUnitKerjaChildren($search));
                        break;
                    case 'unit_kerja_id':
                        $pegawais->whereIn('unit_kerja_id',getAllUnitKerjaChildren($search));
                        break;
                    case 'tanggal_from':
                        break;
                    default:
                        $pegawais->where($key,$search);
                        break;
                }
            }
        }
        $page = $req->get('page',1);
        $perpage = $req->get('perpage',20);

        $count = $pegawais->count();
        $pegawais = $pegawais->skip(($page-1) * $perpage)->take($perpage)->orderBy('peg_nama')->get();
        
        $data = [];

        $hari_kerja = getHariKerja($tanggal_from, $tanggal_to);
        
        foreach ($pegawais as $i => $pegawai) {
            $presensi = $pegawai->getPresensi($tanggal_from, $tanggal_to, $hari_kerja);

            $data[] = [
                ($page-1) * $perpage + $i + 1, // NO
                $pegawai->peg_nama, //NAMA
                $presensi['ci_tepat_waktu'], // HADIR TEPAT WAKTU
                $presensi['ci_d'], // PAGI: DINAS
                $presensi['ci_s'], // PAGI: SAKIT
                $presensi['ci_i'], // PAGI: IZIN
                $presensi['ci_tk'], // PAGI: TANPA KETERANGAN
                $presensi['ci_total_tk'], // PAGI: TOTAL WAKTU TELAT
                $presensi['co_tepat_waktu'], // PULANG: PULANG DIATAS JAM KERJA
                $presensi['co_d'], // PULANG: DINAS
                $presensi['co_s'], // PULANG: SAKIT
                $presensi['co_i'], // PULANG: IZIN
                $presensi['co_tk'], // PULANG: TANPA KETERANGAN
                $presensi['co_total_tk'], // PULANG: TOTAL WAKTU PULANG CEPAT
                $presensi['hari_d'], // HARI: DINAS
                $presensi['hari_s'], // HARI: SAKIT
                $presensi['hari_i'], // HARI: IZIN
                $presensi['hari_c'], // HARI: CUTI
                $presensi['hari_tk'], // HARI: TANPA KETERANGAN
                $presensi['hari_total_tk'], // HARI: TOTAL WAKTU TANPA KETERANGAN
                $presensi['jam_masuk'], // JAM MASUK
                $presensi['jam_pulang'], // JAM PULANG
                $pegawai->peg_nip,
            ];
        }
        
        $result = [
            'data' => $data,
            'count' => $count,
            'hari_kerja' => count($hari_kerja),
        ];

        echo $_GET['callback']. '(' . json_encode($result) . ');';
        
    }

    public function getPresensiBulan(Request $req) {
        $nip = $req->get('nip');
        $tahun = $req->get('tahun',date('Y'));
        $bulan = $req->get('bulan',date('m'));
        $start = date("Y-m-d",strtotime("$tahun-$bulan-01"));
        $end = date("Y-m-d",strtotime("$start +1 month -1 day"));
        
        $result = with(new PresensiController)->getPresensiDetail($nip,$start,$end)->getData();
        if (isset($result->total)) {
            $tpp = floatval(str_replace('%', '', $result->tpp));
            $dec = floatval(str_replace('%', '', $result->total));
            return response()->json([
                'success' => true,
                'message' => 'Kehadiran calculated',
                'hari_total_tk' => intval($result->hari_total_tk),
                'persentase_kehadiran' => $dec,
                'persentase_ketidakhadiran' => 100-$dec,
                'tpp' => $tpp,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Pegawai not found'
            ]);
        }
    }

    public function getPresensiDetail(Request $req) {
        $nip = $req->get('nip');
        $start = date("Y-m-d",strtotime($req->get('start')));
        $end = date("Y-m-d",strtotime($req->get('end')));
        
        $result = with(new PresensiController)->getPresensiDetail($nip,$start,$end)->getData();
        if (isset($result->total)) {
            $dec = floatval(str_replace('%', '', $result->total));
            $data = [];
            foreach ($result->data as $row) {
                $jam_masuk = $row[2] ? date('H:i:s',$row[2]-7*3600) : '-';
                $jam_pulang = $row[8] ? date('H:i:s',$row[8]-7*3600) : '-';
                $data[] = [
                    'no' => $row[0],
                    'tanggal' => $row[1],
                    'jam_masuk' => $jam_masuk,
                    'ci_d' => $row[3],
                    'ci_s' => $row[4],
                    'ci_i' => $row[5],
                    'ci_tk' => $row[6],
                    'ci_waktu_keterlambatan' => $row[7] ? date('H:i:s',$row[7]-7*3600) : '-',
                    'jam_pulang' => $jam_pulang,
                    'co_d' => $row[9],
                    'co_s' => $row[10],
                    'co_i' => $row[11],
                    'co_tk' => $row[12],
                    'co_waktu_pulang_duluan' => $row[13] ? date('H:i:s',$row[13]-7*3600) : '-',
                    'hari_d' => $row[14],
                    'hari_s' => $row[15],
                    'hari_i' => $row[16],
                    'hari_c' => $row[17],
                    'hari_tk' => $row[18],
                    'ci_tel' => $row[22],
                    'co_tel' => $row[23],
                ];
            }
            return response()->json([
                'success' => true,
                'message' => 'Get detail success',
                'data' => $data,
                'persentase_kehadiran' => $dec,
                'persentase_ketidakhadiran' => 100-$dec,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Pegawai not found'
            ]);
        }
    }

    public function getDaftarAbsenApel(Request $req){
        $tanggal_from = $tanggal_to = $req->get('tanggal');
        $pegawais = Pegawai::where('peg_status', true)->where('satuan_kerja_id','<>',999999)->take(1000);
        /*
        $satkers = SatuanKerja::whereNotNull('kode_skpd')->pluck('satuan_kerja_id');
        $pegawais->whereIn('satuan_kerja_id',$satkers);
        */
        if ($req->has('satuan_kerja_id')) {
            $pegawais->where('satuan_kerja_id',$req->get('satuan_kerja_id'));
        }
        if ($req->has('nip_above')) {
            $pegawais->where('peg_nip','>',$req->get('nip_above'));
        }
        $satkers = SatuanKerja::whereNotNull('kode_skpd')->pluck('satuan_kerja_nama','satuan_kerja_id');
        
        $count = $pegawais->count();
        $pegawais = $pegawais->orderBy('peg_nip')->get();
        
        $data = [];
        $total = 0;

        $hari_kerja = getHariKerja($tanggal_from, $tanggal_to);
        $last_nip = '0';
        
        foreach ($pegawais as $i => $pegawai) {
            $presensi = $pegawai->getPresensi($tanggal_from, $tanggal_to, $hari_kerja);
            $userid = UserID::where('ssn',$pegawai->peg_nip)->orWhere('badgenumber',$pegawai->peg_nip)->first();

            if ($userid && ($presensi['ci_total_tk'] > 15*60 || $presensi['hari_tk'] || $presensi['ci_tk'])) {
                if ($presensi['ci_total_tk'] == 0) {
                    $presensi['hari_tk'] = true;
                }
                $data[] = [
                    $pegawai->peg_nip,
                    $pegawai->peg_nama,
                    isset($satkers[$pegawai->satuan_kerja_id]) ? $satkers[$pegawai->satuan_kerja_id] : '',
                    getTime($presensi['ci_total_tk']),
                    ($presensi['hari_tk'] ? 2 : 0) + ($presensi['ci_tk'] ? 1 : 0),
                ];
                $total++;
            }
            $last_nip = $pegawai->peg_nip;
        }
        
        $result = [
            'data' => $data,
            'total' => $total,
            'count' => $count,
            'hari_kerja' => count($hari_kerja),
            'last_nip' => $last_nip,
        ];

        return response()->json($result);
        
    }

    public function getDataCuti(Request $request)
    {
        $bulan = $request->bulan;
        $tahun = $request->tahun ? : date('Y');

        if(!$bulan){
            return response()->json([
                'success' => false,
                'message' => 'Bulan not found'
            ]);
        }

        $tanggal_from = date($tahun.'-'.$bulan.'-01');
        $tanggal_from = date('Y-m-d',strtotime($tanggal_from));
        $tanggal_to = date('Y-m-t',strtotime($tanggal_from));

        $harikerja = getHariKerja($tanggal_from,$tanggal_to);

        $cuti = Ijin::whereMonth('tgl','=',$bulan)->whereYear('tgl','=',$tahun)->where('ijin3','C')->where(function($q){
            $q->where('jeniscuti_id',1)->orWhere('jeniscuti_id',4);
        })->get();
        
        $data = [];

        foreach ($cuti as $c) {
            if(in_array($c->tgl, $harikerja)){
                $data[$c->nip][] = [$c->tgl,$c->jeniscuti_id];
            }
        }

        return response()->json($data);
    }
}
