<?php

namespace App\Http\Controllers;

use App\Model\Karyawan;
use App\Model\KaryawanAbsen;
use App\Model\MJasa;
use App\Model\Order;
use App\Model\Produksi;
use App\Model\Tambahan;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class ReportController extends Controller
{
    public $startHeader     = 1;
    public $startRow        = 2;

	public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        $today = Carbon::now();

        if($today->dayOfWeek === Carbon::SATURDAY) {
            $start = $today->toDateString();
        }else{
            $start = Carbon::parse('last saturday')->toDateString();
        }

        $end = Carbon::parse('this friday')->toDateString();
        

        return view('content.report.index', compact('start', 'end'));
    }

    public function download(Request $request)
    {
        $filename           = "Laporan ".date("Y-m-d H.i.s");
        $tanggal_mulai      = $request->tanggal_mulai;
        $tanggal_selesai    = $request->tanggal_selesai;  

        if (empty($tanggal_mulai) AND empty($tanggal_selesai)) {
            $tanggal_mulai  = date("Y-m-d");
        }

        $data   = array();
        $data[] = $this->getAbsen($tanggal_mulai,$tanggal_selesai);
        $data[] = $this->getEkspedisi($tanggal_mulai,$tanggal_selesai);
        $data[] = $this->getPaketJne($tanggal_mulai,$tanggal_selesai);
        $data[] = $this->getOrder($tanggal_mulai,$tanggal_selesai);
        $data[] = $this->getOrderDay($tanggal_mulai,$tanggal_selesai);
        $data[] = $this->getPemakaianBahan($tanggal_mulai,$tanggal_selesai);

        $this->makeExcels($filename,$data);
    }

    private function getOrderDay($tanggal_mulai,$tanggal_selesai)
    {
        
        $models     = array();

        $orders = Order::select('*', DB::raw('CONCAT(prefix,id) as kode'))
            ->with(['produksi' => function($q) {
                $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['bahan', 'cutting', 'jahit', 'selesai_jahit', 'finishing', 'selesai_finishing', 'ekspedisi'])->orderBy('suffix', 'asc');
            }, 'konsumen']);

        if (empty($tanggal_selesai)) {
            $orders = $orders->whereDate('orders.created_at','=',$tanggal_mulai);
        }
        else{
            $orders = $orders->whereBetween('orders.created_at',[$tanggal_mulai,$tanggal_selesai]);
        }

        $dataOrders = $orders->get();

        foreach ($dataOrders as $key => $data) {
            foreach ($data->produksi as $value) {
                $value->tanggal_order = getFullDate($value->created_at);
                
                $konsumen = $data->konsumen->nrp. " ". $data->konsumen->nama. " ". $data->konsumen->pangkat. " ". $data->konsumen->tik. " ". $data->konsumen->fungsi;
                $bahan = $value->bahan ? "( " . $value->bahan->nama_bahan . " )" : "";
                $jahitan = $value->qty. " ". $value->jenis_jahitan. "\n". $bahan. "\n" . $konsumen;
                $mulai_cutting = $value->cutting ? "( " . $value->cutting->nama . " )" : ""."\n".getShortDateTime($value->waktu_mulai_cutting);
                $mulai_jahit = $value->jahit ? "( " . $value->jahit->nama . " )" : ""."\n".$value->waktu_mulai_jahit;
                
                $dataEkspedisi = $value->id_karyawan_ekspedisi == 0 ? $value->waktu_mulai_ekspedisi : $value->ekspedisi !== null ? $value->waktu_mulai_ekspedisi : "";
                $nama_ekspedisi = $value->ekspedisi !== null ? "( " . $value->ekspedisi->nama . " )" : "";
                
                $mulai_ekspedisi = $value->id_karyawan_ekspedisi == 0 ? "( " . $value->no_resi . " )" : $nama_ekspedisi ."\n".$dataEkspedisi;
                $selesai_ekspedisi = $value->ekspedisi && $value->id_karyawan_ekspedisi != 0 ? $value->waktu_selesai_ekspedisi : "";

                $models[]   = array(
                    "No Order" => str_replace("Kode ", "", $data->kode). "\n". $value->tanggal_order,
                    "Jenis Jahitan" => $jahitan,
                    "Mulai Pola" => $mulai_cutting,
                    "Selesai Pola" => getShortDateTime($value->waktu_selesai_cutting),
                    "Mulai Jahit" => $mulai_jahit,
                    "Selesai Jahit"    => getShortDateTime($value->waktu_selesai_jahit),
                    "Selesai QC" => getShortDateTime($value->waktu_mulai_finishing),
                    "Mulai Ekspedisi" => $mulai_ekspedisi,
                    "Selesai Ekspedisi" => $selesai_ekspedisi,
                );
            }
        }

        $data   = array(
            'sheetname' =>  "Progress", 
            'header'    =>  ["No","No Order", "Jenis Jahitan","Mulai Pola","Selesai Pola","Mulai Jahit","Selesai Jahit","Selesai QC", "Mulai Ekspedisi", "Selesai Ekspedisi"],
            'data'      =>  $models ,
            'param'     =>  [] 

        );

        return $data;
    }

    private function getOrder($tanggal_mulai,$tanggal_selesai)
    {
        
        $models     = array();  
        
        $orders     = Order::with('konsumen')
                        ->with('produksi')
                        ->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))
                        ->where('is_batal',false)
                        ->where('status_akhir', '!=', 8);

        if (empty($tanggal_selesai)) {
            $orders = $orders->whereDate('orders.created_at','=',$tanggal_mulai);
        }
        else{
            $orders = $orders->whereBetween('orders.created_at',[$tanggal_mulai,$tanggal_selesai]);
        }

        $dataOrders = $orders->get();
        
        foreach ($dataOrders as $key => $value) {

            $order_id           = $value->kode; 
            $nama_konsumen      = $value->konsumen->nama;
            $pangkat_konsumen   = $value->konsumen->pangkat;
            $nrp_konsumen       = $value->konsumen->nrp;
            $telp1_konsumen     = $value->konsumen->telp_1;
            $telp2_konsumen     = $value->konsumen->telp_2;

            $produksi           = $value->produksi;
            $kerjaan            = array(); 
            $mulai_cutting      = "";
            $selesai_cutting    = "";
            $mulai_jahit        = "";
            $selesai_jahit      = "";
            // $mulai_finishing    = "";
            $selesai_finishing  = "";
            $mulai_packing      = "";
            $selesai_packing    = "";
            $mulai_ekspedisi    = "";
            $selesai_ekspedisi  = "";

            foreach ($produksi as $k => $v) {

               $kerjaan[]       = $v->jenis_jahitan;

               $mulai_cutting       = ($mulai_cutting == "") ? $v->waktu_mulai_cutting : $mulai_cutting;
               $selesai_cutting     = ($selesai_cutting == "") ? $v->waktu_selesai_cutting : $selesai_cutting;

               $mulai_jahit         = ($mulai_jahit == "") ? $v->waktu_mulai_jahit : $mulai_jahit;
               $selesai_jahit       = ($selesai_jahit == "") ? $v->waktu_selesai_jahit : $selesai_jahit;

               $mulai_packing       = ($mulai_packing == "") ? $v->waktu_mulai_packing : $mulai_packing;
               $selesai_packing     = ($selesai_packing == "") ? $v->waktu_selesai_packing : $selesai_packing;

               // $mulai_finishing     = ($mulai_finishing == "") ? $v->waktu_mulai_finishing : $mulai_finishing;
               $selesai_finishing   = ($selesai_finishing == "") ? $v->waktu_mulai_finishing : $selesai_finishing;

               $mulai_ekspedisi     = ($mulai_ekspedisi == "") ? $v->waktu_mulai_ekspedisi : $mulai_ekspedisi;
               $selesai_ekspedisi   = ($selesai_ekspedisi == "") ? $v->waktu_selesai_ekspedisi : $selesai_ekspedisi;
            }       

            $models[]   = array(
                "Nama"          => $nama_konsumen,
                "Pangkat"       => $pangkat_konsumen,
                "NRP"           => $nrp_konsumen,
                "Kode" => str_replace("Kode ", "", $order_id),
                "Tanggal"      => getShortDate($v->created_at),
                "HP"    => (empty($telp2_konsumen)) ? $telp1_konsumen : $telp1_konsumen." / ".$telp2_konsumen ,
                "Fungsi"       => $value->konsumen->fungsi,
                "HARGA"        => '',
                "TIK"          => $value->konsumen->tik,
                "ITEM"       => implode(",", $kerjaan),

                // "Mulai Cutting"         => ( empty($mulai_cutting) ) ? "-" : getFullDateTime($mulai_cutting),
                // "Selesai Cutting"       => ( empty($selesai_cutting) ) ? "-" : getFullDateTime($selesai_cutting),

                // "Mulai Jahit"           => ( empty($mulai_jahit) ) ? "-" : getFullDateTime($mulai_jahit),
                // "Selesai Jahit"         => ( empty($selesai_jahit) ) ? "-" : getFullDateTime($selesai_jahit),

                // // "Mulai Finishing"       => ( empty($mulai_finishing) ) ? "-" : getFullDateTime($mulai_finishing),
                // "Selesai Finishing"     => ( empty($selesai_finishing) ) ? "-" : getFullDateTime($selesai_finishing),

                // "Mulai Ekspedisi"       => ( empty($mulai_ekspedisi) ) ? "-" : getFullDateTime($mulai_ekspedisi),
                // "Selesai Ekspedisi"     => ( empty($selesai_ekspedisi) ) ? "-" : getFullDateTime($selesai_ekspedisi),

                // "Harga"           => '',

            ); 
        }

        $data   = array(
            'sheetname' =>  "Order", 
            'header'    =>  [
                                "No",
                                "Nama",
                                "Pangkat",
                                "NRP",
                                "Kode",
                                "Tanggal",
                                "HP",
                                "Fungsi",
                                "HARGA",
                                "TIK",
                                "ITEM",
                                // "Selesai Jahit",
                                // "Selesai Finishing",
                                // "Mulai Ekspedisi",
                                // "Selesai Ekspedisi",
                                // "Harga",
                            ], 
            'data'      =>  $models ,
            'param'     =>  [] 

        );

        // dd($data);

        return $data;
    }   

    private function getEkspedisi($tanggal_mulai,$tanggal_selesai)
    {
        $models     = array();

        $ekspedisi  = Order::with(['konsumen', 'produksi'])
            ->select('*',  DB::raw('CONCAT(prefix,increment_id) as kode'))
            ->whereHas('produksi', function ($prod) use($tanggal_selesai, $tanggal_mulai) {
                if (empty($tanggal_selesai)) {
                    $prod = $prod->whereDate('waktu_mulai_ekspedisi','=',$tanggal_mulai)
                        ->where('id_karyawan_ekspedisi', '!=', 0);
                }
                else{
                    $prod = $prod->whereBetween('waktu_mulai_ekspedisi',[$tanggal_mulai,$tanggal_selesai])
                        ->where('id_karyawan_ekspedisi', '!=', 0)
                        ->orWhereBetween('waktu_selesai_ekspedisi',[$tanggal_mulai,$tanggal_selesai])
                        ->where('id_karyawan_ekspedisi', '!=', 0);
                }
                return $prod;
            })
            ->where('status_akhir', 8);

        // $ekspedisi  = Produksi::with('konsumen')
        //     ->leftJoin('karyawan as kcm','kcm.id','=','produksi.id_karyawan_cutting')
        //     ->leftJoin('karyawan as kcs','kcs.id','=','produksi.id_user_selesai_cutting')
        //     ->leftJoin('karyawan as kjm','kjm.id','=','produksi.id_karyawan_jahit')
        //     ->leftJoin('karyawan as kjs','kjs.id','=','produksi.id_user_selesai_jahit')
        //     ->leftJoin('karyawan as kpm','kpm.id','=','produksi.id_karyawan_packing')
        //     ->leftJoin('karyawan as kps','kps.id','=','produksi.id_user_selesai_packing')
        //     ->leftJoin('karyawan as kem','kem.id','=','produksi.id_karyawan_ekspedisi')
        //     ->leftJoin('karyawan as kes','kes.id','=','produksi.id_user_selesai_ekspedisi')
        //     ->leftJoin('karyawan as kfm','kfm.id','=','produksi.id_karyawan_finishing')
        //     ->leftJoin('karyawan as kfs','kfs.id','=','produksi.id_user_selesai_finishing')
        //     ->select(
        //         'produksi.*', 
        //         DB::raw('CONCAT(prefix,increment_id,suffix) as kode'),
        //         'kcm.nama as karyawan_cutting',
        //         'kcs.nama as selesai_cutting',
        //         'kjm.nama as karyawan_jahit',
        //         'kjs.nama as selesai_jahit',
        //         'kpm.nama as karyawan_packing',
        //         'kps.nama as selesai_packing',
        //         'kem.nama as karyawan_packing',
        //         'kes.nama as selesai_packing',
        //         'kfm.nama as karyawan_finishing',
        //         'kfs.nama as selesai_finishing'
        //     )
        //     ->where('status_akhir', 7)
        //     ->orwhere('status_akhir', 8);

        $dataEks    = $ekspedisi->get();

        foreach ($dataEks as $key => $value) {
            // $order_id           = $value->kode; 
            // $nama_konsumen      = $value->konsumen->nama;
            // $pangkat_konsumen   = $value->konsumen->pangkat;
            // $nrp_konsumen       = $value->konsumen->nrp;
            // $telp1_konsumen     = $value->konsumen->telp_1;
            // $telp2_konsumen     = $value->konsumen->telp_2;
            // $tik_konsumen       = $value->konsumen->tik;
            // $fungsi_konsumen    = $value->konsumen->fungsi;
            // $kerjaan            = $value->jenis_jahitan;
            // $telp_penerima      = $value->telp_penerima;

            // // CUTING
            // $cutting_waktu_mulai        = $value->waktu_mulai_cutting;
            // $cutting_waktu_selesai      = $value->waktu_selesai_cutting;
            // $cutting_karyawan_mulai     = $value->karyawan_cutting;
            // $cutting_karyawan_selesai   = $value->selesai_cutting;

            // // JAHIT
            // $jahit_waktu_mulai          = $value->waktu_mulai_jahit;
            // $jahit_waktu_selesai        = $value->waktu_selesai_jahit;
            // $jahit_karyawan_mulai       = $value->karyawan_jahit;
            // $jahit_karyawan_selesai     = $value->selesai_jahit;

            // // PACKING
            // $packing_waktu_mulai        = $value->waktu_mulai_packing;
            // $packing_waktu_selesai      = $value->waktu_selesai_packing;
            // $packing_karyawan_mulai     = $value->karyawan_packing;
            // $packing_karyawan_selesai   = $value->selesai_packing;

            // // EKSPEDISI
            // $ekspedisi_waktu_mulai      = $value->waktu_mulai_ekspedisi;
            // $ekspedisi_waktu_selesai    = $value->waktu_selesai_ekspedisi;
            // $ekspedisi_karyawan_mulai   = $value->karyawan_ekspedisi;
            // $ekspedisi_karyawan_selesai = $value->selesai_ekspedisi;

            // // FINISHING
            // $finishing_waktu_mulai      = $value->waktu_mulai_finishing;
            // $finishing_waktu_selesai    = $value->waktu_selesai_finishing;
            // $finishing_karyawan_mulai   = $value->karyawan_finishing;
            // $finishing_karyawan_selesai = $value->selesai_finishing;
            $prodData = $value->produksi[0] ? $value->produksi[0] : [];

            $dataProduksi = $value->produksi->map(function($data){
                return $data->qty.' '.$data->jenis_jahitan;
            });

            $jahitan = $dataProduksi->implode(', ');

            $models[]   = array(
                "Kode" => $value->kode,
                "Barang" => $jahitan,
                "Nama" => $value->konsumen->nama,
                "NRP" => $value->konsumen->nrp,
                "Fungsi" => $value->konsumen->fungsi,
                "Lokasi" => $value->konsumen->alamat, 
                "Dikirim" => getFullDateTime($prodData->waktu_mulai_ekspedisi),
                "Pengirim" => $prodData->id_karyawan_ekspedisi == 0 ? 'JNE '.$prodData->no_resi : $prodData->ekspedisi->nama,
                "Penerima" => $prodData->penerima_ekspedisi,

                // "Kode"      => $order_id,
                // "Nama"          => $nama_konsumen,
                // "Pangkat"       => $pangkat_konsumen,
                // "NRP"           => $nrp_konsumen,
                // "No Telepon"    => (empty($telp1_konsumen)) ?  (empty($telp2_konsumen)) ? $telp_penerima : $telp2_konsumen : $telp1_konsumen ,
                // "TIK"           => $tik_konsumen,
                // "Fungsi"        => $fungsi_konsumen,
                // "Kerjaan"       => $kerjaan,

                // "CK_mulai"      => $cutting_karyawan_mulai,
                // "CW_mulai"      => $cutting_waktu_mulai,
                // "CK_selesai"    => $cutting_karyawan_selesai,
                // "CW_selesai"    => $cutting_waktu_selesai,

                // "JK_mulai"      => $jahit_karyawan_mulai,
                // "JW_mulai"      => $jahit_waktu_mulai,
                // "JK_selesai"    => $jahit_karyawan_selesai,
                // "JW_selesai"    => $jahit_waktu_selesai,

                // // "PK_mulai"      => $packing_karyawan_mulai,
                // // "PW_mulai"      => $packing_waktu_mulai,
                // // "PK_selesai"    => $packing_karyawan_selesai,
                // // "PW_selesai"    => $packing_waktu_selesai,

                // "FK_mulai"      => $finishing_karyawan_mulai,
                // "FW_mulai"      => $finishing_waktu_mulai,
                // "FK_selesai"    => $finishing_karyawan_selesai,
                // "FW_selesai"    => $finishing_waktu_selesai,

                // "EK_mulai"      => $ekspedisi_karyawan_mulai,
                // "EW_mulai"      => $ekspedisi_waktu_mulai,
                // "EK_selesai"    => $ekspedisi_karyawan_selesai,
                // "EW_selesai"    => $ekspedisi_waktu_selesai
            ); 
        }

        $data   = array(
            'sheetname' =>  "Ekspedisi", 
            'header'    =>  [
                                "No",
                                "Kode",
                                "Barang",
                                "Nama",
                                "NRP",
                                "Fungsi", 
                                "Lokasi", 
                                "Dikirim",
                                "Pengirim",
                                "Penerima",
                                // "No",
                                // "Kode",
                                // "Nama",
                                // "Pangkat",
                                // "NRP",
                                // "No Telepon",
                                // "TIK",
                                // "Fungsi", 
                                // "Kerjaan",

                                // "Karyawan Mulai Cutting",
                                // "Waktu Mulai Cutting",
                                // "Karyawan Selesai Cutting",
                                // "Waktu Selesai Cutting",

                                // "Karyawan Mulai Jahit",
                                // "Waktu Mulai Jahit",
                                // "Karyawan Selesai Jahit",
                                // "Waktu Selesai Jahit",

                                // "Karyawan Mulai Finishing",
                                // "Waktu Mulai Finishing",
                                // "Karyawan Selesai Finishing",
                                // "Waktu Selesai Finishing",

                                // "Karyawan Mulai Ekspedisi",
                                // "Waktu Mulai Ekspedisi",
                                // "Karyawan Selesai Ekspedisi",
                                // "Waktu Selesai Ekspedisi"
                            ], 
            'data'      =>  $models,
            'param'     =>  [] 

        );

        // dd($data);  

        return $data;
    }

    private function getPaketJne($tanggal_mulai,$tanggal_selesai)
    {
        $models     = array();

        $ekspedisi  = Order::with(['konsumen', 'produksi'])
            ->select('*',  DB::raw('CONCAT(prefix,increment_id) as kode'))
            ->whereHas('produksi', function ($prod) use($tanggal_selesai, $tanggal_mulai) {
                if (empty($tanggal_selesai)) {
                    $prod = $prod->whereDate('waktu_mulai_ekspedisi','=',$tanggal_mulai)
                        ->where('id_karyawan_ekspedisi', '=', 0);
                }
                else{
                    $prod = $prod->whereBetween('waktu_mulai_ekspedisi',[$tanggal_mulai,$tanggal_selesai])
                        ->where('id_karyawan_ekspedisi', '=', 0)
                        ->orWhereBetween('waktu_selesai_ekspedisi',[$tanggal_mulai,$tanggal_selesai])
                        ->where('id_karyawan_ekspedisi', '=', 0);
                }
                return $prod;
            })
            ->where('status_akhir', 8);

        $dataEks    = $ekspedisi->get();
        
        foreach ($dataEks as $key => $value) {
            $prodData = $value->produksi[0] ? $value->produksi[0] : [];

            $dataProduksi = $value->produksi->map(function($data){
                return $data->qty.' '.$data->jenis_jahitan;
            });

            $jahitan = $dataProduksi->implode(', ');

            $models[]   = array(
                "Kode" => $value->kode,
                "Barang" => $jahitan,
                "Alamat" => $value->konsumen->alamat, 
                "AWB JNE" => $prodData->no_resi,
                "Tanggal Kirim" => getFullDateTime($prodData->waktu_mulai_ekspedisi),
            ); 
        }

        $data   = array(
            'sheetname' =>  "Paket JNE", 
            'header'    =>  [
                "No",
                "Kode",
                "Barang",
                "Alamat",
                "AWB JNE",
                "Tanggal Kirim"
            ], 
            'data'      =>  $models,
            'param'     =>  [] 

        );

        // dd($data);  

        return $data;
    }

    private function getAbsen($tanggal_mulai,$tanggal_selesai)
    {
        $models     = array();

        $absens     = KaryawanAbsen::select('karyawan_absen.*','karyawan.nama')
            ->join('karyawan','karyawan.id','=','karyawan_absen.karyawan_id');

        if (empty($tanggal_selesai)) {
            $absens = $absens->whereDate('karyawan_absen.tanggal','=',$tanggal_mulai);
        }
        else{
            $absens = $absens->whereBetween('karyawan_absen.tanggal',[$tanggal_mulai,$tanggal_selesai]);
        }

        $dataAbsen  = $absens->get();
        
        foreach ($dataAbsen as $key => $value) {

            $telat      = ( $value->is_telat == 1 ) ? "Ya" : "Tidak" ; 
            $lembur     = ( $value->is_lembur == 1 ) ? "Ya" : "Tidak" ; 
            $status     = ( $value->status == 1 ) ? "Masuk" : "Keluar" ; 

            $models[]   = array(
                "Nama"          => $value->nama,
                "Tanggal"       => $value->tanggal,
                "Jam Kerja"     => $value->jam_kerja,
                "Telat"         => $telat,
                "Status"        => $status,
                "Lembur"        => $lembur
            ); 
        }

        $data   = array(
            'sheetname' =>  "Absen", 
            'header'    =>  ["No","Nama","Tanggal","Jam Kerja","Telat","Status","Lembur"], 
            'data'      =>  $models ,
            'param'     =>  [] 

        );

        return $data;
    }

    private function getPemakaianBahan($tanggal_mulai,$tanggal_selesai)
    {
        $models     = array();

        $bahan      = Produksi::with(['bahan'])
            ->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'));

        if (empty($tanggal_selesai)) {
            $bahan  = $bahan->whereDate('created_at','=',$tanggal_mulai);
        }
        else{
            $bahan  = $bahan->whereBetween('created_at',[$tanggal_mulai,$tanggal_selesai]);
        }

        $dataBahan  = $bahan->get();
        $sum        = 0;

        foreach ($dataBahan as $key => $value) {
            $total      = $value->qty * ((float) $value->pemakaian_bahan > 0 ? (float) $value->pemakaian_bahan : 1);
            $models[]   = array(
                "Kode"              => $value->kode,
                "Bahan"             => $value->bahan ? $value->bahan->nama_bahan : '',
                "Qty"               => $value->qty,
                "Pemakaian Bahan"   => (float) $value->pemakaian_bahan. ' meter',
                "Subtotal"          => $total. ' meter',
            ); 

            $sum += $total;
        }

        $models[] = [
            "Kode"              => '',
            "Bahan"             => '',
            "Qty"               => '',
            "Pemakaian Bahan"   => 'Total',
            "Total"             => $sum. ' meter',
        ];

        $data   = array(
            'sheetname' =>  "Pemakaian Bahan", 
            'header'    =>  [
                "No",
                "Kode",
                "Bahan",
                "Qty",
                "Pemakaian Bahan",
                "Total"
            ], 
            'data'      =>  $models,
            'param'     =>  [] 

        );

        // dd($data);  

        return $data;
    }

    private function makeExcels($filename,$doc)
    {
        $excel = Excel::create($filename, function($excel) use ($doc) {

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setWrapText(true);

            foreach ($doc as $key => $value) {
                $data           = $value['data'];
                $sheetname      = $value['sheetname'];
                $header         = $value['header'];
                $param          = $value['param'];

                $excel->sheet(substr($sheetname,0,30), function($sheet) use ($data,$header,$param, $sheetname) {
                    $sheet->setFontFamily('Tahoma');
                    $sheet->setFontSize(12);
                    $sheet->setWidth(array(
                        'A'     =>  7  * 1,
                        'B'     =>  30 * 1,
                        'C'     =>  30 * 1,
                        'D'     =>  30 * 1,
                        'E'     =>  30 * 1,
                        'F'     =>  30 * 1,
                        'G'     =>  30 * 1,
                        'H'     =>  30 * 1,
                        'I'     =>  30 * 1,
                        'J'     =>  30 * 1,
                        'K'     =>  30 * 1,
                        'L'     =>  30 * 1,
                        'M'     =>  30 * 1,
                        'N'     =>  30 * 1,
                        'O'     =>  30 * 1,
                        'P'     =>  30 * 1,
                        'Q'     =>  30 * 1,
                        'R'     =>  30 * 1,
                        'S'     =>  30 * 1,
                        'T'     =>  30 * 1,
                        'U'     =>  30 * 1,
                        'V'     =>  30 * 1,
                        'W'     =>  30 * 1,
                        'X'     =>  30 * 1,
                        'Y'     =>  30 * 1,
                    ));
                    $alphabet = range('A','Z');
                    $colalphabet = $alphabet[count($header)-1];
                    $row = $header;

                    $sheet->row($this->startHeader, $row);

                    $sheet->cell("A$this->startHeader:".$colalphabet."$this->startHeader", function($cell) {
                        $cell->setAlignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setFontColor('#ffffff');
                        $cell->setBackground('#2532e4');
                    });
                    $rownum = $this->startRow;
                    $i = 1;

                    foreach ($data as $d) {
                        array_unshift($d, $i++);
                        $sheet->row($rownum,$d);

                        if(strpos($sheetname, 'order') !== false) {
                            $sheet->cell('B'.$rownum.':B'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('K'.$rownum.':K'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('C'.$rownum.':J'.$rownum, function($cell) {
                                $cell->setAlignment('center');
                            });
                        } else {
                            $sheet->cell('A'.$rownum.':A'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                        }

                        $rownum++;
                    }

                    $sheet->setBorder("A$this->startHeader:".$colalphabet.($rownum-1), 'thin');
                    $sheet->setFreeze("A$this->startRow");

                    $sheet->setPageMargin(0.25);
                    $sheet->getSheetView()->setZoomScale(80);
                    $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    $sheet->getPageSetup()->setFitToWidth(1);
                    $sheet->getPageSetup()->setFitToHeight(0);
                });
            }
        
        });
        $excel->download();
    }
     
}
