<?php

namespace App\Http\Controllers;

use App\Model\Karyawan;
use App\Model\KaryawanAbsen;
use App\Model\MJasa;
use App\Model\Order;
use App\Model\Produksi;
use App\Model\Tambahan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id != 'admin' && Auth::user()->role_id != 'superadmin') {
            $today = Carbon::now();
            if($today->dayOfWeek === Carbon::SATURDAY) {
                $start = $today->toDateString();
            }else{
                $start = Carbon::parse('last saturday')->toDateString();
            }

            $end = Carbon::parse('this friday')->toDateString();

            return view('home', compact('start', 'end'));
        }else{
            return view('home_admin');
        }
    }

    public function importAbsen()
    {
        return view('import_absen');
    }

    public function storeAbsen(Request $request)
    {
        $file = $request->file('absen');
        $destinationPath = 'uploads/excel';
        $name = $file->move($destinationPath, $file->getClientOriginalName());

        Excel::load($name->getPathName(), function($reader) use (&$array, $request) {
            $reader->formatDates(false);
            $reader->noHeading();
            $reader->skip(4);

            $results = $reader->get();

            $key = null;
            $newArray = [];
            
            $reset = KaryawanAbsen::where('can_delete', 1)->delete();

            foreach($results[0] as $row) {
                if($row[0] == 'Fingerprint ID') {
                    $key = $row[2];
                }

                if(!is_null($key) && $key != '') {
                    $row['id'] = $key;
                    if($row[0] != 'Fingerprint ID' && $row[0] != 'Nama Karyawan' && $row[0] != 'Jabatan Karyawan' && $row[0] != 'Nama Departemen'
                        && $row[0] != 'Hari' && $row[0] != 'Total' && !is_null($row[0])) {
                        $tanggal = explode('-', $row[1]);
                        $status = (int) ($row[14]) * 60 + ((int) ($row[15]));
                        
                        $telat = (int) ($row[8]) * 60 + ((int) ($row[9]));

                        if($status >= 600) {
                            $model = new KaryawanAbsen;
                            $model->tanggal = $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
                            $model->jam_kerja = str_pad($row[3], 2, '0', STR_PAD_LEFT).':'.str_pad($row[4], 2, '0', STR_PAD_LEFT);
                            $model->jam_pulang = str_pad($row[5], 2, '0', STR_PAD_LEFT).':'.str_pad($row[6], 2, '0', STR_PAD_LEFT);
                            $model->is_telat = $telat > 10 ? 1 : 0;
                            $model->karyawan_id = $row['id'];
                            $model->status = $status > 480 ? 1 : 0;
                            $model->save();
                        }
                    }
                }
            }
        });
    }

    public function getProduksi(Request $request)
    {
        $karyawan = Karyawan::find(auth()->user()->karyawan_id);
        $data = [];

        $gaji = 0;
        if(auth()->user()->karyawan->bagian != 'finishing') {
            $models = Produksi::select('*', DB::raw('CONCAT(prefix,id_order,suffix) as kode'))
                ->where(fieldRole(auth()->user()->karyawan->bagian), auth()->user()->karyawan_id)
                ->whereDate(fieldSelesai(auth()->user()->karyawan->bagian), '>=', $request->tanggal_mulai)
                ->whereDate(fieldSelesai(auth()->user()->karyawan->bagian), '<=', $request->tanggal_selesai);

            $models = $models->get();

            foreach ($models as $model) {
                $jasa = MJasa::whereRaw('LOWER(bagian) = "'. strtolower(auth()->user()->karyawan->bagian).'"')
                    ->where('jenis_jahitan', $model->jenis_jahitan)
                    ->first();

                $order = Order::find($model->id_order);
                
                $tambahan = 0;

                if($model->id_tambahan) {
                    if(!like_match("celana%", strtolower($model->jenis_jahitan))) {
                        $tambahan = Tambahan::whereIn('id', $model->id_tambahan)
                            ->where('bagian', $karyawan->bagian)
                            ->sum('biaya'); 
                    }
                }

                if($karyawan->bagian == 'jahit') {
                    $data[] = [
                        'kode_produksi' => $model->kode,
                        'jumlah' => $model->qty,
                        'jenis_jahitan' => $model->jenis_jahitan,
                        'waktu' => getFullDateTime($model->{fieldSelesai(auth()->user()->karyawan->bagian)}),
                        'biaya' => $jasa ? $jasa->biaya : 0,
                        'tambahan' => 'Rp. ' . number_format($tambahan, 0, '.', ','),
                        'total' => 'Rp. '. number_format(($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan, 0, '.', ','),
                    ];

                    $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan;
                }else{
                    $data[] = [
                        'kode_produksi' => $model->kode,
                        'jumlah' => $model->qty,
                        'jenis_jahitan' => $model->jenis_jahitan,
                        'waktu' => getFullDateTime($model->{fieldSelesai(auth()->user()->karyawan->bagian)}),
                        'biaya' => $jasa ? $jasa->biaya : 0,
                        'tambahan' => 'Rp. ' . number_format(0, 0, '.', ','),
                        'total' => 'Rp. '. number_format(($jasa ? ($model->qty * $jasa->biaya) : 0) + 0, 0, '.', ','),
                    ];

                    $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0);
                }
            }
        }

        $result = [
            'data' => $data,
            'grand_total' => 'Rp. '. number_format($gaji + 0, 0, '.', ','),
        ];

        return response()->json($result);
    }

    public function storeFp(Request $request)
    {
        $IP = "192.168.100.110";
        $Key = "0";

        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
        if($Connect){
            $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
            $newLine="\r\n";
            fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
            fputs($Connect, "Content-Type: text/xml".$newLine);
            fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
            fputs($Connect, $soap_request.$newLine);
            $buffer="";
            while($Response=fgets($Connect, 1024)){
                $buffer=$buffer.$Response;
            }
        }else {
            return response()->json([
                'message' => 'Koneksi bermasalah',
                'message_type' => 'error',
            ]);
        }
        
        $buffer=Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
        $buffer=explode("\r\n",$buffer);
        $lastday = KaryawanAbsen::orderBy('id', 'desc')->first();

        $new = collect([]);
        for($a=0;$a<count($buffer);$a++){
            if($buffer[$a] != '') {
                $data = Parse_Data($buffer[$a],"<Row>","</Row>");
                $date = explode(' ', Parse_Data($data,"<DateTime>","</DateTime>"));
                if(isset($lastday->tanggal)) {
                    if(strtotime($date[0]) > strtotime($lastday->tanggal)) {
                        $new[] = [
                            'id' => Parse_Data($data,"<PIN>","</PIN>"),
                            'date' => $date[0],
                            'time' => $date[1],
                            'verified' => Parse_Data($data,"<Verified>","</Verified>"),
                            'status' => Parse_Data($data,"<Status>","</Status>"),
                        ];
                    }
                } else{
                    $new[] = [
                        'id' => Parse_Data($data,"<PIN>","</PIN>"),
                        'date' => $date[0],
                        'time' => $date[1],
                        'verified' => Parse_Data($data,"<Verified>","</Verified>"),
                        'status' => Parse_Data($data,"<Status>","</Status>"),
                    ];
                }
            }
        }

        // $today = Carbon::now();
        // if($today->dayOfWeek === Carbon::SATURDAY) {
        //     $start = $today->toDateString();
        // }else{
        //     $start = Carbon::parse('last saturday')->toDateString();
        // }

        // $end = Carbon::parse('this friday')->toDateString();

        $today = $new->filter(function ($item) {
            // return (strpos($item['date'], date('Y-m-d')) !== false);
            return $item;
        })->values();

        $reset = KaryawanAbsen::where('can_delete', 1)->delete();

        foreach ($today as $key => $value) {
            $absen = new KaryawanAbsen;
            $absen->karyawan_id = $value['id'];
            $absen->tanggal = $value['date'];
            $absen->jam_kerja = $value['time'];
            $absen->status = $value['status'];
            $absen->save();
        }

        return response()->json([
            'message' => 'Absensi berhasil ditambahkan',
            'message_type' => 'ok',
        ]);
    }
}
