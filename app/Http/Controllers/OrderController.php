<?php

namespace App\Http\Controllers;

use App\Model\Konsumen;
use App\Model\MReferensi;
use App\Model\Order;
use App\Model\Produksi;
use App\Model\Tambahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.order.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = Order::with('konsumen')->select('*', DB::raw('CONCAT(prefix,id) as kode'))->where('prefix', '')->where('status', '!=', 8);

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'kode':
                        $models = $models->where(DB::raw('CONCAT(prefix,id)'), $val);
                        break;
                    case 'jenis_jahitan':
                        $models = $models->whereHas('produksi', function($q) use ($val) {
                            $q->where('jenis_jahitan', $val);
                        });
                        break;
                    case 'harga':
                        $models = $models->where('harga', $val);
                        break;
                    case 'jumlah':
                        $models = $models->where('jumlah', $val);
                        break;
                    case 'kuantitas':
                        $models = $models->where('kuantitas', $val);
                        break;
                    case 'status_akhir':
                        $models = $models->where('status_akhir', setStatusProduksi($val));
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                    ->orWhereHas('konsumen', function($que) use($search) {
                        $que->where('nama','like',"%$search%");
                    })
                    ->orWhereHas('produksi', function($que) use($search) {
                        $que->where('jenis_jahitan','like',"%$search%")
                        ->orWhere('qty','like',"%$search%");
                    })
                    ->orWhere('harga','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            $bg = 'btn-default';
            if($model->status_akhir == 1 || $model->status_akhir == 2) {
                $bg = 'btn-success';
            }elseif($model->status_akhir == 3 || $model->status_akhir == 4) {
                $bg = 'btn-info';
            }elseif($model->status_akhir == 5 || $model->status_akhir == 6) {
                $bg = 'btn-danger';
            }elseif($model->status_akhir == 7 || $model->status_akhir == 8) {
                $bg = 'btn-warning';
            }

            $model->bg_color = $bg;
        	$model->status_akhir = getStatusProduksi($model->status_akhir);
            $model->harga = "Rp ".number_format($model->harga, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
        $increment = MReferensi::where('flag', 'increment_harian')->first();

        $prefix = MReferensi::where('flag', 'prefix_harian')->first();
        $kode = $prefix->value.str_pad($increment->value, 5, '0', STR_PAD_LEFT);
        
    	return view('content.order.create', compact('kode'));
    }

    public function store(Request $request)
    {
    	$model = new Order;

    	$error = $this->validate($request, [
    		'konsumen_id'	=>  'required',
    		// 'jenis_jahitan'	=>  'required',
    		// 'harga'	=>  'required',
    		// 'jumlah'	=>  'required',
    		// 'kuantitas'	=>  'required',
    		'jenis_jahitan_detail'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $model->prefix = '';
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->status_akhir = 0;
        $model->created_at = $request->tanggal_order;
        $model->is_grup = $request->is_grup;
        $model->save();

        $abjad = 1;
        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
        	$produksi = new Produksi;
            $produksi->prefix = '';
        	$produksi->id_order = $model->id;
        	$produksi->suffix = chr(64 + $abjad);
        	$produksi->id_konsumen = $model->id_konsumen;
        	$produksi->jenis_jahitan = $value;
        	$produksi->status_akhir = 0;
            $produksi->id_bahan = $request->bahan_id[$idx];
            $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
            $produksi->suffix_bahan = $request->suffix_bahan[$idx];
            $produksi->qty = $request->qty[$idx];
            $produksi->tipe = 'harian';

            if(!empty($request->tambahan)) {
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
            }

        	$produksi->save();

        	$abjad++;
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah ditambahkan'),
            'link' => url('order/edit').'/'.$model->id,
        ]);
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Data order gagal dihapus'
        ];

        $model = Order::find($request->id);

        $produksi = Produksi::where('id_order', $request->id)
            ->delete();
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data order berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = Order::with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))->find($id);
    	$produksi = Produksi::select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->where('id_order', $id)->get();

        foreach ($produksi as &$prod) {
            if($prod->id_tambahan) {
                $tambahan = Tambahan::whereIn('id', $prod->id_tambahan)->get();
            }else{
                $tambahan = null;
            }

            $prod->tambahan = $tambahan;
        }

    	return view('content.order.view', compact('model', 'produksi'));
    }

    public function edit($id)
    {
    	$model = Order::find($id);

    	return view('content.order.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = Order::find($request->id);

    	$error = $this->validate($request, [
            'konsumen_id'   =>  'required',
            // 'jenis_jahitan' =>  'required',
            // 'harga'  =>  'required',
            // 'jumlah'    =>  'required',
            // 'kuantitas' =>  'required',
            'jenis_jahitan_detail'  =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        $model->created_at = $request->tanggal_order;
        $model->is_grup = $request->is_grup;
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->save();

        $ord = Produksi::where('id_order', $request->id)->orderBy('id', 'DESC')->first();
        $abjad = ord(strtolower($ord->suffix)) - 96;

        $ex = Produksi::where('id_order', $request->id);
        $diff2 = array_diff($ex->pluck('id')->toArray(), $request->produksi_id);

        if($diff2) {
            $rem = Produksi::whereIn('id', $diff2)->delete();
        }

        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
            if($request->produksi_id[$idx]) {
                $produksi = Produksi::find($request->produksi_id[$idx]);
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->save();
            }else{
                $produksi = new Produksi;
                $produksi->prefix = '';
                $produksi->id_order = $model->id;
                $produksi->suffix = chr(64 + ($abjad+1));
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->status_akhir = 0;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = isset($request->tambahan[$idx]) ? $request->tambahan[$idx] : null;
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->save();

                $abjad++;
            }
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah diubah'),
            'link' => url('order/edit').'/'.$model->id,
        ]);
    }

    public function getOrder(Request $request)
    {
        $model = Order::pluck('jenis_jahitan', 'id');

        $model->transform(function($jenis_jahitan,$id) {
            $find = Order::select('*', DB::raw('CONCAT(prefix,id) as kode'))->find($id);
            return $find->kode." - ".$jenis_jahitan;
        });

        return response()->json($model);
    }

    public function getOrderDetail(Request $request)
    {
        $model = Order::with('konsumen')->select('*', DB::raw('CONCAT(prefix,id) as kode'))->find($request->order_id);

        $produksi = Produksi::select('id', 'prefix', 'suffix', 'id_karyawan_cutting', 'id_karyawan_jahit', 'id_karyawan_packing', 'id_karyawan_ekspedisi', 'id_karyawan_finishing', 'jenis_jahitan')
            ->with(['cutting', 'jahit', 'packing', 'ekspedisi', 'finishing'])
            ->where('id_order', $request->order_id)
            ->get();

        $model['karyawan'] = $produksi;

        return response()->json($model);
    }

    public function pending()
    {
        $order = Produksi::where('status_akhir', '!=', 8)
            ->select('*', DB::raw('CONCAT(prefix,id_order,suffix) as kode'))
            ->get();

        foreach ($order as &$value) {
            $value->proses_produksi = getStatusProduksi($value->status_akhir);
            $value->status_pending = ($value->is_pending == 1) ? 'Pending' : 'Dalam Proses';
        }

        return view('content.order.pending', compact('order'));
    }

    public function postPending(Request $request)
    {
        if(empty($request->order)) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Order tidak ditemukan',
                    'message_type' => 'error',
                ]);
        }

        $order = Produksi::whereIn('id', $request->order)
            ->update([
                'keterangan_pending' => $request->keterangan_pending,
                'is_pending' => $request->status_pending,
            ]);

        return redirect()->back()->with([
            'message' => trans('Produksi telah dipending'),
        ]);
    }

    public function batal(Request $request)
    {
        $order = Order::with('konsumen')
            ->where('status_akhir', '=', 0)
            ->select('*', DB::raw('CONCAT(prefix,id) as kode'));

        if($request->no_order) {
            $order = $order->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$request->no_order.'%');
        }

        if($request->nama_konsumen) {
            $konsumen = Konsumen::where('nama', 'like', '%'.$request->nama_konsumen.'%')->pluck('id');
            $order = $order->whereIn('id_konsumen', $konsumen);
        }

        if($request->nrp) {
            $konsumen = Konsumen::where('nrp', 'like', '%'.$request->nrp.'%')->pluck('id');
            $order = $order->whereIn('id_konsumen', $konsumen);
        }

        $order = $order->get();

        foreach ($order as &$value) {
            
        }

        return view('content.order.batal', compact('order'));
    }

    public function postBatal(Request $request)
    {
        if(empty($request->order)) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Order tidak ditemukan',
                    'message_type' => 'error',
                ]);
        }

        $order = Order::whereIn('id', $request->order)
            ->delete();

        $produksi = Produksi::whereIn('id_order', $request->order)
            ->delete();

        return redirect()->back()->with([
            'message' => trans('Order telah dibatalkan'),
        ]);
    }
}
