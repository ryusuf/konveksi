<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\MReferensi;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    
    public function username()
    {
        return 'username';
    }

    protected function authenticated(Request $request, $user)
    {
        if(auth()->user()->role_id == 'karyawan') {
            if(auth()->user()->karyawan->bagian == 'finishing') {
                return redirect('produksi/jahit/selesai');
            }else{
                return redirect('produksi/mulai');
            }
        }else{
            return redirect('home');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function logout(Request $request)
    {
        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = date('Y-m-d');
        $ref->save();

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }
}
