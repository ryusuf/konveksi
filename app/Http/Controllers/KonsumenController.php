<?php

namespace App\Http\Controllers;

use App\Model\Konsumen;
use Illuminate\Http\Request;
use File;
use Excel;

class KonsumenController extends Controller
{
    public $startHeader     = 1;
    public $startRow        = 2;

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.konsumen.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = new Konsumen;

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'nrp':
                        $models = $models->where('nrp', $val);
                        break;
                    case 'nama':
                        $models = $models->where('nama', $val);
                        break;
                    case 'pangkat':
                        $models = $models->where('pangkat', $val);
                        break;
                    case 'telp':
                        $models = $models->where('telp_1', $val)
                        	->orWhere('telp_2', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('nama','like',"%$search%")
                  ->orWhere('nrp','like',"%$search%")
                  ->orWhere('pangkat','like',"%$search%")
                  ->orWhere('telp_1','like',"%$search%")
                  ->orWhere('telp_2','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
        	$model->telp = $model->telp_1 . ($model->telp_2 ? '/'. $model->telp_2 : '');
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    private function makeExcels($filename,$doc)
    {
        $excel = Excel::create($filename, function($excel) use ($doc) {

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setWrapText(true);

            foreach ($doc as $key => $value) {
                $data           = $value['data'];
                $sheetname      = $value['sheetname'];
                $header         = $value['header'];
                $param          = $value['param'];

                $excel->sheet(substr($sheetname,0,30), function($sheet) use ($data,$header,$param, $sheetname) {
                    $sheet->setFontFamily('Tahoma');
                    $sheet->setFontSize(12);
                    $sheet->setWidth(array(
                        'A'     =>  7  * 1,
                        'B'     =>  30 * 1,
                        'C'     =>  30 * 1,
                        'D'     =>  30 * 1,
                        'E'     =>  30 * 1,
                        'F'     =>  30 * 1,
                        'G'     =>  30 * 1,
                        'H'     =>  30 * 1,
                        'I'     =>  30 * 1,
                        'J'     =>  30 * 1,
                        'K'     =>  30 * 1,
                        'L'     =>  30 * 1,
                        'M'     =>  30 * 1,
                        'N'     =>  30 * 1,
                        'O'     =>  30 * 1,
                        'P'     =>  30 * 1,
                        'Q'     =>  30 * 1,
                        'R'     =>  30 * 1,
                        'S'     =>  30 * 1,
                        'T'     =>  30 * 1,
                        'U'     =>  30 * 1,
                        'V'     =>  30 * 1,
                        'W'     =>  30 * 1,
                        'X'     =>  30 * 1,
                        'Y'     =>  30 * 1,
                    ));
                    $alphabet = range('A','Z');
                    $colalphabet = $alphabet[count($header)-1];
                    $row = $header;

                    $sheet->row($this->startHeader, $row);

                    $sheet->cell("A$this->startHeader:".$colalphabet."$this->startHeader", function($cell) {
                        $cell->setAlignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setFontColor('#ffffff');
                        $cell->setBackground('#2532e4');
                    });
                    $rownum = $this->startRow;
                    $i = 1;

                    foreach ($data as $d) {
                        array_unshift($d, $i++);
                        $sheet->row($rownum,$d);

                        if(strpos($sheetname, 'order') !== false) {
                            $sheet->cell('B'.$rownum.':B'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('K'.$rownum.':K'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('C'.$rownum.':J'.$rownum, function($cell) {
                                $cell->setAlignment('center');
                            });
                        } else {
                            $sheet->cell('A'.$rownum.':A'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                        }

                        $rownum++;
                    }

                    $sheet->setBorder("A$this->startHeader:".$colalphabet.($rownum-1), 'thin');
                    $sheet->setFreeze("A$this->startRow");

                    $sheet->setPageMargin(0.25);
                    $sheet->getSheetView()->setZoomScale(80);
                    $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    $sheet->getPageSetup()->setFitToWidth(1);
                    $sheet->getPageSetup()->setFitToHeight(0);
                });
            }
        
        });
        $excel->download();
    }

    public function download(Request $request)
    {
        $filename           = "Daftar Konsumen";
        $data   = [];
        $result = [];
        $models = new Konsumen;
        $models = $models->get();

        foreach ($models as &$model) {
        	$model->telp = $model->telp_1 . ($model->telp_2 ? '/'. $model->telp_2 : '');

            $result[]   = array(
                "NRP"          => $model->nrp,
                "Nama"       => $model->nama,
                "Pangkat"     => $model->pangkat,
                "Telepon"     => $model->telp,
                "TIK"         => $model->tik,
                "Fungsi"        => $model->fungsi,
                "Alamat"        => $model->alamat,
            );
        }

        $data[]   = array(
            'sheetname' =>  "Karyawan", 
            'header'    =>  ["No","NRP","Nama","Pangkat","Telepon","TIK","Fungsi","Alamat"],
            'data'      =>  $result,
            'param'     =>  [] 

        );

        $this->makeExcels($filename,$data);
    }

    public function create()
    {
    	return view('content.konsumen.create');
    }

    public function store(Request $request)
    {
    	$model = new Konsumen;

    	$error = $this->validate($request, [
            'nama'		=>  'required',
            'pangkat'	=>  'required',
            'tik'		=>  'required',
            'fungsi'	=>  'required',
            'alamat'	=>  'required',
        ]);

        if($error) {
            if($request->ajax()) {
                return response()->json([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }else{
                return redirect()->back()
                    ->withInput($request->all())
                    ->with([
                        'message' => $message,
                        'message_type' => 'error',
                    ]);
            }
        }
        
        $model->nrp = $request->nrp ? $request->nrp : '';
        $model->nama = $request->get('nama', '');
        $model->pangkat = $request->get('pangkat', '');
        $model->telp_1 = $request->telp1_col1.$request->telp1_col2.$request->telp1_col3;
        $model->telp_2 = $request->telp2_col1.$request->telp2_col2.$request->telp2_col3;
        $model->tik = $request->get('tik', '');
        $model->fungsi = $request->get('fungsi', '');
        $model->alamat = $request->get('alamat', '');
        $model->save();

        if($request->ajax()) {
            return response()->json([
                'message' => 'Konsumen berhasil ditambahkan',
                'message_type' => 'ok',
            ]);
        }else{
            return redirect()->back()->with([
                'message' => trans('Data Konsumen telah ditambahkan'),
                'link' => url('konsumen/edit').'/'.$model->id,
            ]);
        }
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Data Konsumen gagal dihapus'
        ];

        $model = Konsumen::find($request->id);
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data Konsumen berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = Konsumen::find($id);

    	return view('content.konsumen.view', compact('model'));
    }

    public function edit($id)
    {
    	$model = Konsumen::find($id);
        $model->telp_1 = str_split($model->telp_1, 4);
        $model->telp_2 = str_split($model->telp_2, 4);

    	return view('content.konsumen.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = Konsumen::find($request->id);

    	$error = $this->validate($request, [
    		'nrp'		=>  'required',
            'nama'		=>  'required',
            'pangkat'	=>  'required',
            'tik'		=>  'required',
            'fungsi'	=>  'required',
            'alamat'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->nrp = $request->get('nrp', '');
        $model->nama = $request->get('nama', '');
        $model->pangkat = $request->get('pangkat', '');
        $model->telp_1 = $request->telp1_col1.$request->telp1_col2.$request->telp1_col3;
        $model->telp_2 = $request->telp2_col1.$request->telp2_col2.$request->telp2_col3;
        $model->tik = $request->get('tik', '');
        $model->fungsi = $request->get('fungsi', '');
        $model->alamat = $request->get('alamat', '');
        $model->save();

        return redirect()->back()->with([
            'message' => trans('Data konsumen telah diubah'),
            'link' => url('konsumen/edit').'/'.$model->id,
        ]);
    }

    public function getKonsumen(Request $request)
    {
        $model = Konsumen::pluck('nama', 'id');

        $model->transform(function($nama,$id) {
            $find = Konsumen::find($id);
            return $find->nrp." - ".$nama;
        });

        return response()->json($model);
    }
}
