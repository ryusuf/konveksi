<?php

namespace App\Http\Controllers;

use App\Model\MBahan;
use App\Model\MJasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use Excel;

class MasterBahanController extends Controller
{
    public $startHeader     = 1;
    public $startRow        = 2;

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.bahan.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = new MBahan;
        $models = $models->select('*', DB::raw('CONCAT(prefix,id) as kode'));

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'nama_bahan':
                        $models = $models->where('nama_bahan', $val);
                        break;
                    case 'satuan':
                        $models = $models->where('satuan', $val);
                        break;
                    case 'harga':
                        $models = $models->where('harga', $val);
                        break;
                    case 'jenis':
                        $models = $models->where('jenis', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                  ->orWhere('nama_bahan','like',"%$search%")
                  ->orWhere('satuan','like',"%$search%")
                  ->orWhere('harga','like',"%$search%")
                  ->orWhere('jenis','like',"%$search%");
            });
        }

        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
        	$model->harga = "Rp ".number_format($model->harga, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    private function makeExcels($filename,$doc)
    {
        $excel = Excel::create($filename, function($excel) use ($doc) {

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setWrapText(true);

            foreach ($doc as $key => $value) {
                $data           = $value['data'];
                $sheetname      = $value['sheetname'];
                $header         = $value['header'];
                $param          = $value['param'];

                $excel->sheet(substr($sheetname,0,30), function($sheet) use ($data,$header,$param, $sheetname) {
                    $sheet->setFontFamily('Tahoma');
                    $sheet->setFontSize(12);
                    $sheet->setWidth(array(
                        'A'     =>  7  * 1,
                        'B'     =>  30 * 1,
                        'C'     =>  30 * 1,
                        'D'     =>  30 * 1,
                        'E'     =>  30 * 1,
                        'F'     =>  30 * 1,
                        'G'     =>  30 * 1,
                        'H'     =>  30 * 1,
                        'I'     =>  30 * 1,
                        'J'     =>  30 * 1,
                        'K'     =>  30 * 1,
                        'L'     =>  30 * 1,
                        'M'     =>  30 * 1,
                        'N'     =>  30 * 1,
                        'O'     =>  30 * 1,
                        'P'     =>  30 * 1,
                        'Q'     =>  30 * 1,
                        'R'     =>  30 * 1,
                        'S'     =>  30 * 1,
                        'T'     =>  30 * 1,
                        'U'     =>  30 * 1,
                        'V'     =>  30 * 1,
                        'W'     =>  30 * 1,
                        'X'     =>  30 * 1,
                        'Y'     =>  30 * 1,
                    ));
                    $alphabet = range('A','Z');
                    $colalphabet = $alphabet[count($header)-1];
                    $row = $header;

                    $sheet->row($this->startHeader, $row);

                    $sheet->cell("A$this->startHeader:".$colalphabet."$this->startHeader", function($cell) {
                        $cell->setAlignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setFontColor('#ffffff');
                        $cell->setBackground('#2532e4');
                    });
                    $rownum = $this->startRow;
                    $i = 1;

                    foreach ($data as $d) {
                        array_unshift($d, $i++);
                        $sheet->row($rownum,$d);

                        if(strpos($sheetname, 'order') !== false) {
                            $sheet->cell('B'.$rownum.':B'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('K'.$rownum.':K'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('C'.$rownum.':J'.$rownum, function($cell) {
                                $cell->setAlignment('center');
                            });
                        } else {
                            $sheet->cell('A'.$rownum.':A'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                        }

                        $rownum++;
                    }

                    $sheet->setBorder("A$this->startHeader:".$colalphabet.($rownum-1), 'thin');
                    $sheet->setFreeze("A$this->startRow");

                    $sheet->setPageMargin(0.25);
                    $sheet->getSheetView()->setZoomScale(80);
                    $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    $sheet->getPageSetup()->setFitToWidth(1);
                    $sheet->getPageSetup()->setFitToHeight(0);
                });
            }
        
        });
        $excel->download();
    }

    public function download(Request $request)
    {
        $filename           = "Daftar Bahan";
        $data   = [];
        $result = [];
        $models = new MBahan;
        $models = $models->select('*', DB::raw('CONCAT(prefix,id) as kode'));
        $models = $models->get();

        foreach ($models as &$model) {
        	$model->harga = "Rp ".number_format($model->harga, 0, ".", ",");

            $result[]   = array(
                "Kode"          => $model->kode,
                "Nama Bahan"       => $model->nama_bahan,
                "Harga"     => $model->harga,
            );
        }

        $data[]   = array(
            'sheetname' =>  "Karyawan", 
            'header'    =>  ["No","Kode","Nama Bahan","Harga"],
            'data'      =>  $result,
            'param'     =>  [] 

        );

        $this->makeExcels($filename,$data);
    }

    public function create()
    {
    	return view('content.bahan.create');
    }

    public function store(Request $request)
    {
    	$model = new MBahan;

    	$error = $this->validate($request, [
    		'nama_bahan'	=>  'required',
            'harga'			=>  'required',
        ]);

        if($error) {
            if($request->ajax()) {
                return response()->json([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }else{
                return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }
        }
        
        $model->nama_bahan = $request->get('nama_bahan', '');
        $model->harga = $request->harga ? $request->harga : 0;
        $model->save();

        if($request->ajax()) {
            return response()->json([
                'message' => 'Bahan berhasil ditambahkan',
                'message_type' => 'ok',
            ]);
        }else{
            return redirect()->back()->with([
                'message' => trans('Data bahan telah ditambahkan'),
                'link' => url('bahan/edit').'/'.$model->id,
            ]);
        }
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Data bahan gagal dihapus'
        ];

        $model = MBahan::find($request->id);
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data bahan berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = MBahan::select('*', DB::raw('CONCAT(prefix,id) as kode'))->find($id);

    	return view('content.bahan.view', compact('model'));
    }

    public function edit($id)
    {
    	$model = MBahan::find($id);

    	return view('content.bahan.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = MBahan::find($request->id);

    	$error = $this->validate($request, [
    		'nama_bahan'	=>  'required',
            'harga'			=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->nama_bahan = $request->get('nama_bahan', '');
        $model->harga = $request->harga ? $request->harga : 0;
        $model->save();

        return redirect()->back()->with([
            'message' => trans('Data bahan telah diubah'),
            'link' => url('bahan/edit').'/'.$model->id,
        ]);
    }

    public function getBahan(Request $request)
    {
        $model = MBahan::orderBy('nama_bahan', 'asc')->get()->pluck('id', 'nama_bahan');

        return response()->json($model);
    }

    public function getSatuanBahan(Request $request)
    {
        $model = MJasa::where('jenis_jahitan', $request->bahan_id)->first();

        return response()->json($model);
    }
}
