<?php

namespace App\Http\Controllers;

use App\Model\MJasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterJasaController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.jasa.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = MJasa::select('*', DB::raw('CONCAT(prefix,id) as kode'));

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'jenis_jasa':
                        $models = $models->where('jenis_jasa', $val);
                        break;
                    case 'biaya_cutting':
                        $models = $models->where('biaya_cutting', $val);
                        break;
                    case 'biaya_jahit':
                        $models = $models->where('biaya_jahit', $val);
                        break;
                    case 'biaya_packing':
                        $models = $models->where('biaya_packing', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                  ->orWhere('jenis_jasa','like',"%$search%")
                  ->orWhere('biaya_cutting','like',"%$search%")
                  ->orWhere('biaya_jahit','like',"%$search%")
                  ->orWhere('biaya_packing','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
        	$model->biaya = "Rp ".number_format($model->biaya, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
    	return view('content.jasa.create');
    }

    public function store(Request $request)
    {
    	$model = new MJasa;

    	$error = $this->validate($request, [
            'bagian'    =>  'required',
    		'jenis_jahitan'	=>  'required',
            'biaya'    =>  'required',
            'satuan'    =>  'required',
        ]);

        if($error) {
            if($request->ajax()) {
                return response()->json([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }else{
                return redirect()->back()
                    ->withInput($request->all())
                    ->with([
                        'message' => $message,
                        'message_type' => 'error',
                    ]);
            }
        }

        $cek = MJasa::where('jenis_jasa', $request->bagian.' '.$request->jenis_jahitan)->where('bagian', $request->bagian)->exists();
        if($cek) {
            if($request->ajax()) {
                return response()->json([
                    'message' => 'Jasa sudah terdaftar',
                    'message_type' => 'ok',
                ]);
            }else{
                return redirect()->back()
                    ->withInput($request->all())
                    ->with([
                        'message' => 'Jasa sudah terdaftar',
                        'message_type' => 'error',
                    ]);
            }
        }
        
        $model->bagian = $request->get('bagian', '');
        $model->jenis_jasa = $request->bagian.' '.$request->jenis_jahitan;
        $model->biaya = $request->biaya ? $request->biaya : 0;
        $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->satuan = $request->get('satuan', '');
        $model->save();

        if($request->ajax()) {
            return response()->json([
                'message' => 'Jasa berhasil ditambahkan',
                'message_type' => 'ok',
            ]);
        }else{
            return redirect()->back()->with([
                'message' => trans('Data jasa telah ditambahkan'),
                'link' => url('jasa/edit').'/'.$model->id,
            ]);
        }
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Data jasa gagal dihapus'
        ];

        $model = MJasa::find($request->id);
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data jasa berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = MJasa::select('*', DB::raw('CONCAT(prefix,id) as kode'))->find($id);

    	return view('content.jasa.view', compact('model'));
    }

    public function edit($id)
    {
    	$model = MJasa::find($id);

    	return view('content.jasa.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = MJasa::find($request->id);

    	$error = $this->validate($request, [
    		'jenis_jahitan'	=>  'required',
            'biaya'    =>  'required',
            'bagian' => 'required',
            'satuan' => 'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->bagian = $request->get('bagian', '');
        $model->jenis_jasa = $request->bagian.' '.$request->jenis_jahitan;
        $model->biaya = $request->biaya ? $request->biaya : 0;
        $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->satuan = $request->get('satuan', '');
        $model->save();

        return redirect()->back()->with([
            'message' => trans('Data jasa telah diubah'),
            'link' => url('jasa/edit').'/'.$model->id,
        ]);
    }

    public function getJahitan()
    {
        $model = MJasa::groupBy('jenis_jahitan')->pluck('jenis_jahitan', 'jenis_jahitan');

        return response()->json($model);
    }
}
