<?php

namespace App\Http\Controllers;

use App\Model\Konsumen;
use App\Model\MReferensi;
use App\Model\Order;
use App\Model\Produksi;
use App\Model\Tambahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BoronganController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.borongan.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = Order::select(DB::raw('CONCAT(prefix,increment_id) as kode'), 'id', 'increment_id', 'keterangan_borongan', 'id_konsumen')->where('prefix', 'F');

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'kode':
                        $models = $models->where(DB::raw('CONCAT(prefix,id)'), $val);
                        break;
                    case 'keterangan_borongan':
                        $models = $models->where('keterangan_borongan', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                  ->orWhere('keterangan_borongan','like',"%$search%");
            });
        }

        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }

        $models = $models->groupBy('increment_id');
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
        	$konsumen = Konsumen::find($model->id_konsumen);
            $model->konsumen = $konsumen;
        }

        $result = [
            'data' => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function create()
    {
        $increment = MReferensi::where('flag', 'increment_borongan')->first();

        $prefix = MReferensi::where('flag', 'prefix_borongan')->first();
        $kode = $prefix->value.str_pad($increment->value, 5, '0', STR_PAD_LEFT);

    	return view('content.borongan.create', compact('kode'));
    }

    public function store(Request $request)
    {
    	$error = $this->validate($request, [
            'keterangan_borongan'   =>  'required',
    		'konsumen_id'	=>  'required',
    		// 'jenis_jahitan'	=>  'required',
    		// 'jumlah'	=>  'required',
    		// 'kuantitas'	=>  'required',
    		'jenis_jahitan_detail'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $increment = MReferensi::where('flag', 'increment_borongan')->first();

        $prefix = MReferensi::where('flag', 'prefix_'.$request->prefix)->first();

        foreach ($request->konsumen_id as $key => $value) {
            $model = new Order;
            $model->prefix = 'F';
            $model->id_konsumen = $value;
            // $model->jenis_jahitan = $request->jenis_jahitan[$key];
            $model->harga = $request->harga ? $request->harga : 0;
            // $model->jumlah = $request->jumlah[$key];
            // $model->kuantitas = $request->kuantitas[$key];
            $model->status_akhir = 0;
            $model->keterangan_borongan = $request->keterangan_borongan;
            $model->created_at = $request->tanggal_order;
            $model->increment_id = $increment->value;

            $model->save();

            $abjad = 1;
            foreach (array_filter($request->jenis_jahitan_detail[$key]) as $idx => $detail) {
                $produksi = new Produksi;
                $produksi->prefix = 'F'.$model->increment_id;
                $produksi->id_order = $model->id;
                $produksi->suffix = chr(64 + $abjad);
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $detail;
                $produksi->status_akhir = 0;
                $produksi->id_bahan = $request->bahan_id[$key][$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$key][$idx];
                $produksi->suffix_bahan = $request->suffix_bahan[$key][$idx];
                $produksi->qty = $request->qty[$key][$idx];
                $produksi->increment_id = $increment->value;

                if(!empty($request->tambahan[$key])) {
                    $model->id_tambahan = isset($request->tambahan[$key]) ? (isset($request->tambahan[$key][$idx]) ? $request->tambahan[$key][$idx] : '') : '';
                }

                $produksi->save();

                $abjad++;
            }
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        $add = MReferensi::where('flag', 'increment_borongan')->first();
        $add->value = $add->value+1;
        $add->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah ditambahkan'),
            'link' => url('order/borongan/edit').'/'.$model->increment_id,
        ]);
    }

    public function view($id)
    {
        $models = Order::select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))->where('increment_id', $id)->get();

        foreach ($models as &$m) {
            $produksi = Produksi::select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->where('increment_id', $m->increment_id)->get();

            foreach ($produksi as &$p) {
                if($p->id_tambahan) {
                    $tambahan = Tambahan::whereIn('id', $p->id_tambahan)->get();
                }else{
                    $tambahan = null;
                }
                $p->tambahan = $tambahan;
            }

            $m->produksi = $produksi;
        }

        return view('content.borongan.view', compact('models'));
    }

    public function delete(Request $request)
    {
        $result = [
            'success' => false,
            'message' => 'Data order gagal dihapus'
        ];

        $model = Order::where('increment_id', $request->id);

        $produksi = Produksi::whereIn('id_order', $model->pluck('id'))->delete();
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data order berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function edit($id)
    {
        $model = Order::where('increment_id', $id)->get();

        return view('content.borongan.edit', compact('model', 'id'));
    }

    public function update(Request $request)
    {
        $error = $this->validate($request, [
            'konsumen_id'   =>  'required',
            // 'jenis_jahitan' =>  'required',
            // 'harga'  =>  'required',
            // 'jumlah'    =>  'required',
            // 'kuantitas' =>  'required',
            'jenis_jahitan_detail'  =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $max = Order::where('prefix', 'F')->orderBy('increment_id', 'DESC')->first();
               
        $inc = $max->increment_id;

//         $id = Order::where('increment_id', $max->increment_id)->pluck('id')->toArray();

//         $diff = array_diff($id, $request->id);
// dd($diff);
//         if($diff) {
//             $ord = Order::whereIn('id', $diff);
//             $pro = Produksi::whereIn('id_order', $diff)->delete();
//             $ord->delete();
//         }

        foreach ($request->konsumen_id as $key => $value) {
            if($request->id[$key]) {
                $model = Order::find($request->id[$key]);

                $model->id_konsumen = $value;
                // $model->jenis_jahitan = $request->jenis_jahitan[$key];
                $model->harga = $request->harga ? $request->harga : 0;
                // $model->jumlah = $request->jumlah[$key];
                // $model->kuantitas = $request->kuantitas[$key];
                $model->keterangan_borongan = $request->keterangan_borongan;
                $model->created_at = $request->tanggal_order;
                $model->save();
                
                $ord = Produksi::where('id_order', $request->id[$key])->orderBy('id_order', 'DESC')->first();
                $abjad = ord(strtolower($ord->suffix)) - 96;
                
                $ex = Produksi::where('id_order', $request->id[$key]);
                $diff2 = array_diff($ex->pluck('id')->toArray(), $request->produksi_id[$key]);

                if($diff2) {
                    $rem = Produksi::whereIn('id', $diff2)->delete();
                }

                foreach (array_filter($request->jenis_jahitan_detail[$key]) as $idx => $detail) {
                    if($request->produksi_id[$key][$idx]) {
                        $produksi = Produksi::find($request->produksi_id[$key][$idx]);
                        $produksi->id_konsumen = $model->id_konsumen;
                        $produksi->jenis_jahitan = $detail;
                        $produksi->id_bahan = $request->bahan_id[$key][$idx];
                        $produksi->pemakaian_bahan = $request->pemakaian_bahan[$key][$idx];
                        $produksi->id_tambahan = $request->tambahan[$key][$idx];
                        $produksi->suffix_bahan = $request->suffix_bahan[$key][$idx];
                        $produksi->qty = $request->qty[$key][$idx];
                        $produksi->increment_id = $model->increment_id;
                        $produksi->save();
                    }else{
                        $produksi = new Produksi;
                        $produksi->prefix = 'F'.$model->increment_id;
                        $produksi->id_order = $model->id;
                        $produksi->suffix = chr(64 + ($abjad + 1));
                        $produksi->id_konsumen = $model->id_konsumen;
                        $produksi->jenis_jahitan = $detail;
                        $produksi->status_akhir = 0;
                        $produksi->id_bahan = $request->bahan_id[$key][$idx];
                        $produksi->pemakaian_bahan = $request->pemakaian_bahan[$key][$idx];
                        $produksi->id_tambahan = $request->tambahan[$key][$idx];
                        $produksi->suffix_bahan = $request->suffix_bahan[$key][$idx];
                        $produksi->qty = $request->qty[$key][$idx];
                        $produksi->increment_id = $model->increment_id;
                        $produksi->save();

                        $abjad++;
                    }
                }
            }else{
                $model = new Order;
                $model->prefix = 'F';
                $model->id_konsumen = $value;
                // $model->jenis_jahitan = $request->jenis_jahitan[$key];
                $model->harga = $request->harga ? $request->harga : 0;
                // $model->jumlah = $request->jumlah[$key];
                // $model->kuantitas = $request->kuantitas[$key];
                $model->status_akhir = 0;
                $model->keterangan_borongan = $request->keterangan_borongan;

                $model->increment_id = $inc;
                $model->created_at = $request->tanggal_order;
                $model->save();

                $abjad = 1;
                foreach (array_filter($request->jenis_jahitan_detail[$key]) as $idx => $detail) {
                    $produksi = new Produksi;
                    $produksi->prefix = 'F'.$model->increment_id;
                    $produksi->id_order = $model->id;
                    $produksi->suffix = chr(64 + $abjad);
                    $produksi->id_konsumen = $model->id_konsumen;
                    $produksi->jenis_jahitan = $detail;
                    $produksi->status_akhir = 0;
                    $produksi->id_bahan = $request->bahan_id[$key][$idx];
                    $produksi->pemakaian_bahan = $request->pemakaian_bahan[$key][$idx];
                    $produksi->suffix_bahan = $request->suffix_bahan[$key][$idx];
                    $produksi->qty = $request->qty[$key][$idx];
                    $produksi->increment_id = $model->increment_id;

                    if(!empty($request->tambahan[$key][$idx])) {
                        $model->id_tambahan = $request->tambahan[$key][$idx];
                    }

                    $produksi->save();

                    $abjad++;
                }
            }
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah diubah'),
            'link' => url('order/borongan/edit').'/'.$inc,
        ]);
    }

    public function getOrder(Request $request)
    {
        $data = Order::with('produksi')->where('increment_id', $request->id)->get();

        foreach ($data as &$value) {
            $value->harga = intVal($value->harga);
        }

        return response()->json($data);
    }
}
