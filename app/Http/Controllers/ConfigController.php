<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\MReferensi;

class ConfigController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        $refs = MReferensi::where('flag', 'like', 'increment_%')->get();
    	return view('content.config.kode', compact('refs'));
    }


    public function update(Request $request)
    {
        foreach ($request->data as $key => $value) {
            $model = MReferensi::where('flag', $key)->first();
            $model->value = $value;
            $model->save();
        }

        return redirect()->back()->with([
            'message' => trans('Data nomor order telah berhasil diubah'),
            'link' => url('bahan/edit').'/'.$model->id,
        ]);
    }
}
