<?php

namespace App\Http\Controllers;

use App\Model\Konsumen;
use App\Model\MReferensi;
use App\Model\Order;
use App\Model\OrderBorongan;
use App\Model\OrderPdu;
use App\Model\OrderPermak;
use App\Model\OrderSespim;
use App\Model\Produksi;
use App\Model\Tambahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class ProduksiController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function mulai(Request $request)
    {
        $search = $request->search;
        
        if(Auth::user()->karyawan->bagian == 'ekspedisi') {
            return redirect('produksi/manual/ekspedisi');
        }

        $max = MReferensi::where('flag', 'max_produksi')->first();

        if(Auth::user()->karyawan->bagian == 'jahit') {
            $permak = Produksi::with(['order' => function($query) {
                    $query->with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'));
                }, 'bahan'])
            	->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->where('status_akhir', setStatusRole(Auth::user()->karyawan->bagian))
                ->where('id_karyawan_jahit', null)
                ->where('waktu_mulai_jahit', null)
                ->where('karyawan_permak', Auth::user()->karyawan_id)
                ->where('is_pending', 0)
                ->where('is_batal', 0)
                ->where('prefix', 'P');

            if($search) {
                $permak = $permak->Where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$search.'%');
            }

            // if($request->nrp) {
            //     $konsumen = Konsumen::where('nrp', 'like', '%'.$request->nrp.'%')->pluck('id');
            //     $permak = $permak->whereIn('id_konsumen', $konsumen);
            // }

            $permak = $permak->get();

            foreach ($permak as &$per) {
                if($per->id_tambahan) {
                    $tambahan = implode(',', Tambahan::whereIn('id', $per->id_tambahan)->pluck('jenis_tambahan')->toArray());
                }else{
                    $tambahan = null;
                }

                $per->tambahan = $tambahan;
            }

            $produksi = Produksi::with(['order' => function($query) {
                    $query->with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'));
                }, 'bahan'])
            	->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->where('status_akhir', setStatusRole(Auth::user()->karyawan->bagian))
                ->where('is_pending', 0)
                ->where('is_batal', 0)
                ->where('is_skip', 0)
                ->where('prefix', '!=', 'P')
                ->where('prefix', '!=', 'B');

            if($search != '') {
                $produksi = $produksi->Where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$search.'%');
            }

            // if($request->no_order) {
            //     $produksi = $produksi->where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$request->no_order.'%');
            // }

            // if($request->nama_konsumen) {
            //     $konsumen = Konsumen::where('nama', 'like', '%'.$request->nama_konsumen.'%')->pluck('id');
            //     $produksi = $produksi->whereIn('id_konsumen', $konsumen);
            // }

            // if($request->nrp) {
            //     $konsumen = Konsumen::where('nrp', 'like', '%'.$request->nrp.'%')->pluck('id');
            //     $produksi = $produksi->whereIn('id_konsumen', $konsumen);
            // }
            
            $produksi = $produksi->orderBy(getMulaiRole(Auth::user()->karyawan->bagian), 'asc')->get();
        }else{
            $permak = null;

            $produksi = Produksi::with(['order' => function($query) {
                    $query->with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'));
                }, 'bahan'])
            	->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->where('status_akhir', setStatusRole(Auth::user()->karyawan->bagian))
                ->where('is_pending', 0)
                ->where('is_batal', 0)
                ->where('is_skip', 0);
            
            if($search != '') {
                $produksi = $produksi->Where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$search.'%');
            }

            // if($request->no_order) {
            //     $produksi = $produksi->where('id_order', (int) $request->no_order);
            // }

            // if($request->nama_konsumen) {
            //     $konsumen = Konsumen::where('nama', 'like', '%'.$request->nama_konsumen.'%')->pluck('id');
            //     $produksi = $produksi->whereIn('id_konsumen', $konsumen);
            // }

            // if($request->nrp) {
            //     $konsumen = Konsumen::where('nrp', 'like', '%'.$request->nrp.'%')->pluck('id');
            //     $produksi = $produksi->whereIn('id_konsumen', $konsumen);
            // }

            if(auth()->user()->karyawan->bagian != 'finishing') {
                $produksi = $produksi->orderBy(getMulaiRole(Auth::user()->karyawan->bagian), 'asc');
            }else{
                $produksi = $produksi->orderBy('id', 'asc');
            }

            $produksi = $produksi->get();
        }

        foreach ($produksi as $key => &$prod) {
            if($prod->id_tambahan) {
                $tambahan = implode(',', Tambahan::whereIn('id', $prod->id_tambahan)->pluck('jenis_tambahan')->toArray());
            }else{
                $tambahan = null;
            }

            $prod->tambahan = $tambahan;

            if(Auth::user()->karyawan->bagian == 'cutting') {
                if($prod->id_karyawan_cutting != null) {
                    $prod->disabled = true;
                }else{
                    $prod->disabled = false;
                }
            }

            if(count($permak) != 0) {
                $prod->disabled = true;
            }else{
                if($key < $max->value) {
                    $prod->disabled = false;
                }else{
                    $prod->disabled = true;
                }
            }

            $prod->tanggal_order = getFullDate($prod->created_at);
        }

        if(count($permak) > 0) {
            foreach ($permak as &$per) {
                $per->tanggal_order = getFullDate($per->created_at);
            }

            $permak = $permak->groupBy('id_order');
        }

        if(count($produksi) > 0) {
            $produksi = $produksi->groupBy('id_order');
        }

        return view('content.produksi.mulai', compact('produksi', 'permak', 'search'));
    }

    public function manual($bagian)
    {
        if($bagian == 'ekspedisi') {
            $order = Produksi::with(['konsumen', 'order'])
                ->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->whereHas('order', function($ord) {
                    return $ord->where('status_akhir', '>', 5);
                })
                // ->where('status_akhir', setStatusRole('ekspedisi'))
                ->where('is_pending', 0)
                ->where('is_batal', 0)
                ->whereNotIn('status_akhir', [7, 8])
                ->orderBy('id_order', 'asc')
                ->get();

            foreach ($order as &$ord) {
                $ord->karyawan = $ord->finishing;
                $ord->waktu = getFullDateTime($ord->waktu_mulai_finishing);
                $ord->tanggal_order = getFullDate($ord->created_at);
            }

            foreach ($order as &$val) {
                if($val->status_akhir == setStatusRole('ekspedisi')) {
                    $val->disabled = false;
                }else{
                    $val->disabled = true;
                }
            }

            if(count($order) > 0) {
                $order = $order->groupBy('id_order');
            }

            // $order = Order::with(['produksi', 'konsumen'])
            //     ->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))
            //     ->where('status_akhir', setStatusRole('ekspedisi'))
            //     ->whereIn('id', $produksi)
            //     ->orderBy('id', 'ASC')
            //     ->get();

            return view('content.produksi.ekspedisi.mulai', compact('bagian', 'order'));
        }else{
            return view('content.produksi.manual', compact('bagian'));
        }

    }

    public function getProduksi(Request $request)
    {
        if($request->bagian == '') {
            return response()->json([]);
        }

        $produksi = Produksi::with(['order' => function($query) {
                $query->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'));
            }, 'konsumen', 'bahan'])
            ->where('status_akhir', setStatusRole($request->bagian))
            ->where('is_pending', 0)
            ->where('is_batal', 0);

        if($request->bagian == 'ekspedisi') {
            $produksi = $produksi->whereNotIn('status_akhir', [7, 8]);
        }

        if(auth()->user()->role_id == 'karyawan') {
            $produksi = $produksi->orderBy(getMulaiRole($request->bagian), 'asc');
        }else{
            $produksi = $produksi->orderBy('id_order', 'asc');
        }

        if($request->keyword) {
            $produksi = $produksi->whereHas('konsumen', function($q) use($request) {
                $q->where('nama', 'like', '%'.$request->keyword.'%')
                    ->orWhere('nrp', 'like', '%'.$request->keyword.'%')
                    ->orWhere('tik', 'like', '%'.$request->keyword.'%');
            })
            ->where('status_akhir', setStatusRole($request->bagian))
            ->where('is_pending', 0)
            ->where('is_batal', 0);

            $produksi = $produksi->orWhere(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$request->keyword.'%')
                ->where('status_akhir', setStatusRole($request->bagian))
                ->where('is_pending', 0)
                ->where('is_batal', 0);
        }

        $produksi = $produksi->get();
        
        $cek = $produksi->pluck('id_order');
        $arrKey = [];
        foreach ($produksi as &$prod) {
            if($prod->id_tambahan) {
                $tambahan = implode(',', Tambahan::whereIn('id', $prod->id_tambahan)->pluck('jenis_tambahan')->toArray());
            }else{
                $tambahan = null;
            }

            $prod->tambahan = $tambahan;

            if($request->bagian == 'cutting') {
                if($prod->id_karyawan_cutting != null) {
                    $prod->disabled = true;
                }else{
                    $prod->disabled = false;
                }
            }elseif($request->bagian == 'jahit') {
                if($prod->id_karyawan_jahit != null) {
                    $prod->disabled = true;
                }else{
                    $prod->disabled = false;
                }

                $prod->karyawan = $prod->cutting;
                $prod->waktu = getFullDateTime($prod->waktu_mulai_cutting);

            }elseif($request->bagian == 'finishing') {
                if($prod->id_karyawan_finishing != null) {
                    $prod->disabled = true;
                }else{
                    $prod->disabled = false;
                }

                $prod->karyawan = $prod->jahit;
                $prod->waktu = getFullDateTime($prod->waktu_mulai_jahit);

            }elseif($request->bagian == 'ekspedisi') {
                if($prod->id_karyawan_ekspedisi != null) {
                    $prod->disabled = true;
                }else{
                    $prod->disabled = false;
                }

                $prod->karyawan = $prod->finishing;
                $prod->waktu = getFullDateTime($prod->waktu_mulai_finishing);
            }

            $prod->bagian = $request->bagian;
            $prod->tanggal_order = getFullDate($prod->created_at);
        }

        if(count($produksi) > 0) {
            $produksi = $produksi->groupBy('id_order');
        }

        return response()->json($produksi);
    }

    public function getSelesaiProduksi(Request $request)
    {
        if($request->bagian == '') {
            return response()->json([]);
        }

        $order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['cutting', 'bahan', 'jahit'])->orderBy('suffix', 'asc');
        }, 'konsumen'])->orderBy('id', 'ASC');

        if($request->bagian == 'cutting') {
            $order = $order->whereHas('produksi', function($query) {
                $query->where('id_karyawan_cutting', '!=', null)
                    ->where('waktu_selesai_cutting', null)
                    ->where('status_akhir', 1);
            });
        }elseif($request->bagian == 'jahit') {
            $order = $order->whereHas('produksi', function($query) {
                $query->where('id_karyawan_jahit', '!=', null)
                    ->where('waktu_selesai_jahit', null)
                    ->where('status_akhir', 3);
            });
        }

        if($request->keyword) {
            $order = $order->whereHas('konsumen', function($q) use($request) {
                $q->where('nama', 'like', '%'.$request->keyword.'%')
                    ->orWhere('nrp', 'like', '%'.$request->keyword.'%')
                    ->orWhere('tik', 'like', '%'.$request->keyword.'%');
            });

            $order = $order->orWhereHas('produksi', function($q) use($request) {
                $q->where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$request->keyword.'%');
            });
        }

        $order = $order->get();
        
        foreach ($order as &$ord) {
            foreach ($ord->produksi as &$prod) {
                if($request->bagian == 'cutting') {
                    $prod->waktu_mulai = getFullDateTime($prod->waktu_mulai_cutting);
                }elseif($request->bagian == 'jahit') {
                    $prod->waktu_mulai = getFullDateTime($prod->waktu_mulai_jahit);
                }elseif($request->bagian == 'finishing') {
                    $prod->waktu_mulai = getFullDateTime($prod->finishing);
                }elseif($request->bagian == 'ekspedisi') {
                    $prod->waktu_mulai = getFullDateTime($prod->waktu_mulai_ekspedisi);
                }

                $prod->not_disabled = $prod->cutting && $prod->waktu_mulai_cutting;
                $prod->tanggal_order = getFullDate($prod->created_at);
            }
            $ord->bagian = $request->bagian;
        }

        return response()->json($order);
    }

    public function store(Request $request)
    {
        $error = $this->validate($request, [
            'id_order'   =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $max = MReferensi::where('flag', 'max_produksi')->first();

        if(auth()->user()->karyawan->bagian != 'finishing') {
            $limit = Produksi::where(fieldRole(auth()->user()->karyawan->bagian), auth()->user()->karyawan_id)
                ->where(fieldSelesai(auth()->user()->karyawan->bagian), null)
                ->count();

            if($limit >= 2) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->with([
                        'message' => 'Maaf anda hanya dapat memilih maksimal 2 item. Silahkan setorkan pekerjaan anda untuk melanjutkan.',
                        'message_type' => 'error',
                    ]);
            }
        }

        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value);
            $jum = 0;

            if(Auth::user()->karyawan->bagian == 'cutting') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_cutting', null)->count();

                $data = [
                    'id_karyawan_cutting' => Auth::user()->karyawan_id,
                    'waktu_mulai_cutting' => \Carbon\Carbon::now(),
                    'status_akhir' => 1,
                ];
            }elseif(Auth::user()->karyawan->bagian == 'jahit') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_jahit', null)->count();

                $data = [
                    'id_karyawan_jahit' => Auth::user()->karyawan_id,
                    'waktu_mulai_jahit' => \Carbon\Carbon::now(),
                    'status_akhir' => 3,
                ];
            }elseif(Auth::user()->karyawan->bagian == 'finishing') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_finishing', null)->count();

                $data = [
                    'id_karyawan_finishing' => Auth::user()->karyawan_id,
                    'waktu_mulai_finishing' => \Carbon\Carbon::now(),
                    'id_user_selesai_finishing' => Auth::user()->karyawan_id,
                    'status_akhir' => 6,
                ];
            }elseif(Auth::user()->karyawan->bagian == 'ekspedisi') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_ekspedisi', null)->count();

                $data = [
                    'id_karyawan_ekspedisi' => Auth::user()->karyawan_id,
                    'waktu_mulai_ekspedisi' => \Carbon\Carbon::now(),
                    'status_akhir' => 7,
                ];
            }
            // }elseif(Auth::user()->karyawan->bagian == 'finishing') {
            //     $jum = Produksi::where('id', $value)->where('id_karyawan_finishing', null)->count();

            //     $data = [
            //         'id_karyawan_finishing' => Auth::user()->karyawan_id,
            //         'waktu_mulai_finishing' => \Carbon\Carbon::now(),
            //         'status_akhir' => 9,
            //     ];
            // }

            if($jum > 0) {
                if(isset($data)) {
                    if($max->value > 1 && $max->value <= 10) {
                        $max->value -= 1;

                    }else{
                        $max->value = 10;
                    }

                    $max->save();

                    $model->update($data);
                }
            }else{
                return redirect()->back()->with([
                    'message' => trans('Produksi telah diambil'),
                    'message_type' => 'error',
                ]);
            }
        }

        if(Auth::user()->karyawan->bagian == 'cutting') {
            $prod = Produksi::where('id_order', $request->id_order)->where('id_karyawan_cutting', null)->count();

            if($prod == 0) {
                $order = Order::find($request->id_order);
                $order->status_akhir = 1;
                $order->save();
            }
        }elseif(Auth::user()->karyawan->bagian == 'jahit') {
            $prod = Produksi::where('id_order', $request->id_order)->where('id_karyawan_jahit', null)->count();

            if($prod == 0) {
                $order = Order::find($request->id_order);
                $order->status_akhir = 3;
                $order->save();
            }
        }elseif(Auth::user()->karyawan->bagian == 'finishing') {
            $prod = Produksi::where('id_order', $request->id_order)->where('id_karyawan_finishing', null)->count();

            if($prod == 0) {
                $order = Order::find($request->id_order);
                $order->status_akhir = 6;
                $order->save();
            }
        }elseif(Auth::user()->karyawan->bagian == 'ekspedisi') {
            $prod = Produksi::where('id_order', $request->id_order)->where('id_karyawan_ekspedisi', null)->count();

            if($prod == 0) {
                $order = Order::find($request->id_order);
                $order->status_akhir = 7;
                $order->save();
            }
        }

        // foreach ($request->bahan_id as $key => $value) {
        //     $produksi = Produksi::find($key);
        //     $produksi->id_bahan_cutting = $value;
        //     $produksi->pemakaian_bahan_cutting = isset($request->pemakaian_bahan[$key]) ? $request->pemakaian_bahan[$key] : '';
        //     $produksi->save();
        // }

        if(Auth::user()->karyawan->bagian != 'finishing') {
            Auth::logout();

            \Session::flush();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah dimulai'),
        ]);
    }

    public function storeManual(Request $request)
    {
        // $error = $this->validate($request, [
        //     'bagian'   =>  'required',
        //     'produksi'   =>  'required',
        //     'karyawan_id'   =>  'required',
        // ]);

        // if($error) {
        //     return redirect()->back()
        //         ->withInput($request->all())
        //         ->with([
        //             'message' => $message,
        //             'message_type' => 'error',
        //         ]);
        // }

        $date = \Carbon\Carbon::now();
        if (!empty($request->work_time) && !empty($request->work_date)) {
            $date = \Carbon\Carbon::parse($request->work_date.' '.$request->work_time);
        }

        foreach ($request->produksi as $value) {

            $model = Produksi::where('id', $value);
            $jum = 0;

            if($request->bagian == 'cutting') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_cutting', null)->count();

                $data = [
                    'id_karyawan_cutting' => $request->karyawan_id,
                    'waktu_mulai_cutting' => $date,
                    'status_akhir' => 1,
                ];
            }elseif($request->bagian == 'jahit') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_jahit', null)->count();

                $data = [
                    'id_karyawan_jahit' => $request->karyawan_id,
                    'waktu_mulai_jahit' => $date,
                    'status_akhir' => 3,
                ];
            }elseif($request->bagian == 'finishing') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_finishing', null)->count();

                $data = [
                    'id_karyawan_finishing' => $request->karyawan_id,
                    'waktu_mulai_finishing' => $date,
                    'status_akhir' => 5,
                ];
            }elseif($request->bagian == 'ekspedisi') {
                $jum = Produksi::where('id', $value)->where('id_karyawan_ekspedisi', null)->count();

                $data = [
                    'id_karyawan_ekspedisi' => $request->karyawan_id,
                    'waktu_mulai_ekspedisi' => $date,
                    'status_akhir' => 7,
                ];
            }

            if($jum > 0) {
                if(isset($data)) {
                    $model->update($data);
                }
            }else{
                return redirect()->back()->with([
                    'message' => trans('Produksi telah diambil'),
                    'message_type' => 'error',
                ]);
            }

            $model = $model->first();

            if($request->bagian == 'cutting') {
                $prod = Produksi::where('id_order', $model->id_order)->where('id_karyawan_cutting', null)->count();

                if($prod == 0) {
                    $order = Order::find($model->id_order);
                    $order->status_akhir = 1;
                    $order->save();
                }
            }elseif($request->bagian == 'jahit') {
                $prod = Produksi::where('id_order', $model->id_order)->where('id_karyawan_jahit', null)->count();

                if($prod == 0) {
                    $order = Order::find($model->id_order);
                    $order->status_akhir = 3;
                    $order->save();
                }
            }elseif($request->bagian == 'finishing') {
                $prod = Produksi::where('id_order', $model->id_order)->where('id_karyawan_finishing', null)->count();

                if($prod == 0) {
                    $order = Order::find($model->id_order);
                    $order->status_akhir = 5;
                    $order->save();
                }
            }elseif($request->bagian == 'ekspedisi') {
                $prod = Produksi::where('id_order', $model->id_order)->where('id_karyawan_ekspedisi', null)->count();

                if($prod == 0) {
                    $order = Order::find($model->id_order);
                    $order->status_akhir = 7;
                    $order->save();
                }
            }
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah dimulai'),
        ]);
    }

    public function selesaiCutting()
    {
        $order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['cutting', 'bahan'])->orderBy('suffix', 'asc');
        }])->orderBy('id', 'ASC');
        
        $order = $order->whereHas('produksi', function($query) {
            $query->where('id_karyawan_cutting', '!=', null)
            ->where('waktu_selesai_cutting', null)
            ->where('status_akhir', 1);
        });

        $order = $order->get();

    	return view('content.produksi.cutting.selesai', compact('order'));
    }

    public function postSelesaiCutting(Request $request)
    {
        $error = $this->validate($request, [
            'id_order'   =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $date = \Carbon\Carbon::now();
        if (!empty($request->work_time) && !empty($request->work_date)) {
            $date = \Carbon\Carbon::parse($request->work_date.' '.$request->work_time);
        }

        $tipe = '';
        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value);
        
            $model->update([
                'id_user_selesai_cutting' => Auth::user()->karyawan_id,
                'waktu_selesai_cutting' => $date,
                'status_akhir' => 2,
            ]);
        }

        $prod = Produksi::where('id_order', $request->id_order)->where('id_user_selesai_cutting', null)->count();

        if($prod == 0) {
            $order = Order::find($request->id_order);

            $order->status_akhir = 2;
            $order->save();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah selesai'),
        ]);
    }

    public function selesaiJahit()
    {
    	$order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['jahit', 'bahan'])->orderBy('suffix', 'asc');
        }])->orderBy('id', 'ASC');

        $order = $order->whereHas('produksi', function($query) {
            $query->where('id_karyawan_jahit', '!=', null)
                ->where('waktu_selesai_jahit', null)
                ->where('status_akhir', 3);
        });

        $order = $order->get();

    	return view('content.produksi.jahit.selesai', compact('order'));
    }

    public function postSelesaiJahit(Request $request)
    {
    	$error = $this->validate($request, [
            'id_order' => 'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $date = \Carbon\Carbon::now();
        if (!empty($request->work_time) && !empty($request->work_date)) {
            $date = \Carbon\Carbon::parse($request->work_date.' '.$request->work_time);
        }

        $tipe = '';
        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value);
        
            $model->update([
                'id_user_selesai_jahit' => Auth::user()->karyawan_id,
                'waktu_selesai_jahit' => $date,
                'status_akhir' => 4,
            ]);
        }

        $prod = Produksi::where('id_order', $request->id_order)->where('id_user_selesai_jahit', null)->count();

        if($prod == 0) {
            $order = Order::find($request->id_order);

            $order->status_akhir = 4;
            $order->save();
        }

        return redirect()->back()->with([
            'message' => trans('Proses jahit telah selesai'),
        ]);
    }

    public function selesaiPacking()
    {
        $order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['jahit', 'bahan'])->orderBy('suffix', 'asc');
        }])->orderBy('id', 'ASC');

        $order = $order->whereHas('produksi', function($query) {
            $query->where('id_karyawan_finishing', '!=', null)
                ->where('waktu_selesai_finishing', null)
                ->where('status_akhir', 5);
        });

        $order = $order->get();

    	return view('content.produksi.packing.selesai', compact('order'));
    }

    public function postSelesaiPacking(Request $request)
    {
    	$error = $this->validate($request, [
            'id_order'   =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $tipe = '';
        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value);
        
            $model->update([
                'id_user_selesai_finishing' => Auth::user()->karyawan_id,
                'waktu_selesai_finishing' => \Carbon\Carbon::now(),
                'status_akhir' => 6,
            ]);
        }

        $prod = Produksi::where('id_order', $request->id_order)->where('id_user_selesai_finishing', null)->count();

        if($prod == 0) {
            $order = Order::find($request->id_order);

            $order->status_akhir = 6;
            $order->save();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah selesai'),
        ]);
    }

    public function selesaiEkspedisi()
    {
        $order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['cutting', 'bahan'])->orderBy('suffix', 'asc');
        }])->orderBy('id', 'ASC');

        $order = $order->whereHas('produksi', function($query) {
            $query->where('id_karyawan_ekspedisi', '!=', null)
                ->where('waktu_selesai_ekspedisi', null)
                ->where('status_akhir', 7);
        });

        $order = $order->get();

        return view('content.produksi.ekspedisi.selesai', compact('order'));
    }

    public function postSelesaiEkspedisi(Request $request)
    {
        $error = $this->validate($request, [
            'id_order'   =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $tipe = '';
        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value[0]);
        
            $model->update([
                'id_user_selesai_ekspedisi' => Auth::user()->karyawan_id,
                'waktu_selesai_ekspedisi' => \Carbon\Carbon::now(),
                'status_akhir' => 8,
            ]);
        }

        $prod = Produksi::where('id_order', $request->id_order)->where('id_user_selesai_ekspedisi', null)->count();

        if($prod == 0) {
            $order = Order::find($request->id_order);
            
            $order->status_akhir = 8;
            $order->save();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah selesai'),
        ]);
    }

    public function selesaiFinishing()
    {
        $order = Order::with(['produksi' => function($q) {
            $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['cutting', 'bahan'])->orderBy('suffix', 'asc');
        }])->orderBy('id', 'ASC');

        $order = $order->whereHas('produksi', function($query) {
            $query->where('id_karyawan_finishing', '!=', null)
                ->where('waktu_selesai_finishing', null);
        });

        $order = $order->get();

        return view('content.produksi.finishing.selesai', compact('order'));
    }

    public function postSelesaiFinishing(Request $request)
    {
        $error = $this->validate($request, [
            'id_order'   =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $tipe = '';
        foreach ($request->produksi as $value) {
            $model = Produksi::where('id', $value);
        
            $model->update([
                'id_user_selesai_finishing' => Auth::user()->karyawan_id,
                'waktu_selesai_finishing' => \Carbon\Carbon::now(),
                'status_akhir' => 6,
            ]);
        }

        $prod = Produksi::where('id_order', $request->id_order)->where('id_user_selesai_finishing', null)->count();

        if($prod == 0) {
            $order = Order::find($request->id_order);

            $order->status_akhir = 6;
            $order->save();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah selesai'),
        ]);
    }

    public function detail($id)
    {
        $model = Order::with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))->find($id);
        $produksi = Produksi::select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->where('id_order', $id)->get();

        return view('content.produksi.view', compact('model', 'produksi'));
    }

    public function cetak(Request $request)
    {
        if(empty($request->id)) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Item tidak ditemukan',
                    'message_type' => 'error',
                ]);
        }

        $order = Order::with(['produksi', 'konsumen'])
            ->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))->orderBy('id_konsumen', 'asc');

        $order = $order->whereHas('produksi', function($query) use ($request) {
            $query->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->whereIn('id', $request->id);
        });

        $order = $order->get();

        // $order = Produksi::with(['order' => function($query) {
        //             $query->with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'))->orderBy('id_konsumen', 'asc');
        //     },'konsumen'])
        //     ->select('*', DB::raw('CONCAT(prefix,id_order,suffix) as kode'))
        //     ->whereIn('id', $request->id)
        //     ->orderBy('id_order', 'asc')
        //     ->get();

        foreach ($order as &$value) {
            $dataProduksi = $value->produksi->map(function($data){
                return $data->qty.' '.$data->jenis_jahitan;
            });

            $value->jahitan = $dataProduksi->implode(', ');
        }

        if($request->tipe == 'karyawan') {
            // $order = $order->groupBy('id_order')->values();

            $pdf = PDF::loadView('content.produksi.pdf.karyawan', ['order' => $order])->setPaper('a4')->setOrientation('landscape');

            // return view('content.produksi.pdf.karyawan', ['order' => $order]);
        }else{
            if($request->group) {
                $pdf = PDF::loadView('content.produksi.pdf.order', ['order' => $order])->setPaper('a4')->setOrientation('landscape');
            }else{
                $pdf = PDF::loadView('content.produksi.pdf.individu', ['order' => $order])->setPaper('a4')->setOrientation('landscape');
            }

            // return view('content.produksi.pdf.order', ['order' => $order]);
        }

        $destinationPath = public_path('ekspedisi');

        \File::makeDirectory($destinationPath, $mode = 0777, true, true);

        $filename = 'order '. getFullDate(date('Y-m-d')) .'.pdf';
        $file = $destinationPath.'/'.$filename;
        $url = asset('ekspedisi'.'/'.$filename);

        $cek = \File::exists($file);
        if($cek) {
            \File::delete($file);
        }

        $pdf->save($file);

        \Session::flash('download.in.the.next.request', $url);

        return redirect('download');
    }

    public function prosesEkspedisi(Request $request)
    {
        if(empty($request->produksi)) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Item tidak ditemukan',
                    'message_type' => 'error',
                ]);
        }

        $order = Order::with(['produksi']);

        $order = $order->whereHas('produksi', function($query) use ($request) {
            $query->whereIn('id', $request->produksi);
        });

        $order = $order->get();

        if($request->alamat != '') {
            $idk = $order->pluck('id_konsumen');

            $kon = Konsumen::whereIn('id', $idk)->update(['alamat' => $request->alamat]);
        }

        if($request->tipe == 'jne') {
            $request['karyawan_id'] = 0;
        }

        foreach ($order as $ord) {
            if($request->proses == 'mulai') {
                // if($ord->status_akhir == 7) {
                //     return redirect()->back()
                //         ->withInput($request->all())
                //         ->with([
                //             'message' => 'Order sudah dikirim',
                //             'message_type' => 'error',
                //         ]);
                // }

                foreach ($request->produksi as $value) {
                    $model = Produksi::where('id', $value);

                    $jum = Produksi::where('id', $value)->where('id_karyawan_ekspedisi', null)->count();

                    $data = [
                        'id_karyawan_ekspedisi' => $request->karyawan_id,
                        'waktu_mulai_ekspedisi' => \Carbon\Carbon::now(),
                        'status_akhir' => 7,
                        'no_resi' => $request->no_resi,
                    ];

                    $model->update($data);
                }

                $prod = Produksi::where('id_order', $ord->id)->where('id_karyawan_ekspedisi', null)->count();

                if($prod == 0) {
                    $newOrder = Order::find($ord->id);
                    $newOrder->status_akhir = 7;
                    $newOrder->save();
                }
            }elseif($request->proses == 'selesai') {
                foreach ($request->produksi as $value) {
                    $model = Produksi::where('id', $value);
                
                    $model->update([
                        'id_user_selesai_ekspedisi' => Auth::user()->karyawan_id,
                        'waktu_selesai_ekspedisi' => \Carbon\Carbon::now(),
                        'status_akhir' => 8,
                        'penerima_ekspedisi' => $request->penerima,
                        'telp_penerima' => $request->telp_penerima,
                    ]);
                }

                $prod = Produksi::where('id_order', $request->id)->where('id_user_selesai_ekspedisi', null)->count();

                if($prod == 0) {
                    $newOrder = Order::find($ord->id);
                    $newOrder->status_akhir = 8;
                    $newOrder->save();
                }
            }
        }

        if($request->proses == 'mulai') {
            return redirect('produksi/cetak?'.http_build_query(['id' => $request->produksi, 'tipe' => $request->tipe, 'group' => $request->group]));
        }else{
            return redirect()->back()->with([
                'message' => trans('Produksi telah selesai'),
            ]);
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah dimulai'),
        ]);
    }

    public function listEkspedisi()
    {
        $order = Produksi::with('konsumen')
            ->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
            ->where('status_akhir', 7)
            ->get();
        
        return view('content.produksi.ekspedisi.list', compact('bagian', 'order'));
    }

    public function batal(Request $request)
    {
        $order = Produksi::with(['order' => function($query) {
                $query->with('konsumen')->select('*', DB::raw('CONCAT(prefix,increment_id) as kode'));
            }, 'bahan'])
            ->whereNotIn('status_akhir', [0, 8])
            ->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'));

        if($request->no_order) {
            $order = $order->where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$request->no_order.'%');
        }
        
        $order = $order->whereHas('order', function($q) use($request) {
            $q->whereHas('konsumen', function($k) use($request) {
                if($request->nama_konsumen) {
                    $konsumen = Konsumen::where('nama', 'like', '%'.$request->nama_konsumen.'%')->pluck('id');
                    $k->whereIn('id_konsumen', $konsumen);
                }

                if($request->nrp) {
                    $konsumen2 = Konsumen::where('nrp', 'like', '%'.$request->nrp.'%')->pluck('id');
                    $k->whereIn('id_konsumen', $konsumen2);
                }
            });
        });

        if($request->no_order != '' || $request->nama_konsumen != '' || $request->nrp != '') {
            $order = $order->get();
        }else{
            $order = [];
        }

        return view('content.produksi.batal', compact('order'));
    }

    public function postBatal(Request $request)
    {
        if(empty($request->order)) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Produksi tidak ditemukan',
                    'message_type' => 'error',
                ]);
        }

        $count = Produksi::whereNotIn('id', $request->order)->count();
        
        $produksi = Produksi::whereIn('id', $request->order);
        $produksi = $produksi->get();

        $order_id = [];
        foreach ($produksi as $key => $value) {
            $order_id[] = $value->id_order;

            $new = Produksi::find($value->id);
            if($value->status_akhir == 1) {
                $new->status_akhir = 0;
                $new->waktu_mulai_cutting = null;
                $new->id_karyawan_cutting = null;
                $new->save();
            }elseif($value->status_akhir == 2) {
                $new->status_akhir = 1;
                $new->waktu_selesai_cutting = null;
                $new->id_user_selesai_cutting = null;
                $new->save();
            }elseif($value->status_akhir == 3) {
                $new->status_akhir = 2;
                $new->waktu_mulai_jahit = null;
                $new->id_karyawan_jahit = null;
                $new->save();
            }elseif($value->status_akhir == 4) {
                $new->status_akhir = 3;
                $new->waktu_selesai_jahit = null;
                $new->id_user_selesai_jahit = null;
                $new->save();
            }elseif($value->status_akhir == 5) {
                $new->status_akhir = 4;
                $new->waktu_mulai_finishing = null;
                $new->id_karyawan_finishing = null;
                $new->save();
            }elseif($value->status_akhir == 6) {
                $new->status_akhir = 4;
                $new->waktu_selesai_finishing = null;
                $new->id_user_selesai_finishing = null;
                $new->save();
            }elseif($value->status_akhir == 7) {
                $new->status_akhir = 6;
                $new->waktu_mulai_ekspedisi = null;
                $new->id_karyawan_ekspedisi = null;
                $new->save();
            }
        }

        $order = Order::whereIn('id', $order_id)->first();
        if($count == 0) {
            $newOrder = Order::whereIn('id', $order_id);
            $newOrder->status_akhir = ($order->status_akhir - 1);
            $newOrder->save();
        }

        return redirect()->back()->with([
            'message' => trans('Produksi telah dibatalkan'),
        ]);
    }
}
