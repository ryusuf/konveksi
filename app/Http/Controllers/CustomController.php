<?php

namespace App\Http\Controllers;

use App\Model\MReferensi;
use App\Model\Order;
use App\Model\OrderPdu;
use App\Model\Produksi;
use App\Model\Tambahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.custom.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = OrderPdu::with('konsumen')->select('*', DB::raw('CONCAT(prefix,id) as kode'))->where('prefix', 'B');

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'kode':
                        $models = $models->where(DB::raw('CONCAT(prefix,id)'), $val);
                        break;
                    case 'jenis_jahitan':
                        $models = $models->whereHas('produksi', function($q) use ($val) {
                            $q->where('jenis_jahitan', $val);
                        });
                        break;
                    case 'harga':
                        $models = $models->where('harga', $val);
                        break;
                    case 'jumlah':
                        $models = $models->where('jumlah', $val);
                        break;
                    case 'kuantitas':
                        $models = $models->where('kuantitas', $val);
                        break;
                    case 'status_akhir':
                        $models = $models->where('status_akhir', setStatusProduksi($val));
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                  ->orWhere('jenis_jahitan','like',"%$search%")
                  ->orWhere('harga','like',"%$search%")
                  ->orWhere('jumlah','like',"%$search%")
                  ->orWhere('kuantitas','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            $bg = 'btn-default';
            if($model->status_akhir == 1 || $model->status_akhir == 2) {
                $bg = 'btn-success';
            }elseif($model->status_akhir == 3 || $model->status_akhir == 4) {
                $bg = 'btn-info';
            }elseif($model->status_akhir == 5 || $model->status_akhir == 6) {
                $bg = 'btn-danger';
            }elseif($model->status_akhir == 7 || $model->status_akhir == 8) {
                $bg = 'btn-warning';
            }

            $model->bg_color = $bg;
        	$model->status_akhir = getStatusProduksi($model->status_akhir);
            $model->harga = "Rp ".number_format($model->harga, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
        $increment = MReferensi::where('flag', 'increment_pdu')->first();

        $prefix = MReferensi::where('flag', 'prefix_pdu')->first();
        $kode = $prefix->value.str_pad($increment->value, 5, '0', STR_PAD_LEFT);

    	return view('content.custom.create', compact('kode'));
    }

    public function store(Request $request)
    {
    	$model = new OrderPdu;

    	$error = $this->validate($request, [
    		'konsumen_id'	=>  'required',
    		// 'jenis_jahitan'	=>  'required',
    		// 'harga'	=>  'required',
    		'produksi_id'	=>  'required',
    		'pemakaian_bahan'	=>  'required',
    		'jenis_jahitan_detail'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->prefix = 'B';
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->status_akhir = 0;
        $model->created_at = $request->tanggal_order;
        $model->is_skip = $request->is_skip == 1 ? 1 : 0;
        $model->save();

        $abjad = 1;
        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
        	$produksi = new Produksi;
        	$produksi->prefix = 'B';
        	$produksi->id_order = $model->id;
        	$produksi->suffix = chr(64 + $abjad);
        	$produksi->id_konsumen = $model->id_konsumen;
        	$produksi->jenis_jahitan = $value;
        	$produksi->status_akhir = 0;
            $produksi->id_bahan = $request->bahan_id[$idx];
            $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
            $produksi->suffix_bahan = $request->suffix_bahan[$idx];
            $produksi->qty = $request->qty[$idx];
            $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;
            $produksi->tipe = 'pdu';

            if(!empty($request->tambahan)) {
                $produksi->id_tambahan = $request->tambahan[$idx];
            }

        	$produksi->save();

        	$abjad++;
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah ditambahkan'),
            'link' => url('order/custom/edit').'/'.$model->id,
        ]);
    }

    public function edit($id)
    {
        $model = Order::find($id);

        return view('content.custom.edit', compact('model'));
    }

    public function update(Request $request)
    {
        $model = OrderPdu::find($request->id);

        $error = $this->validate($request, [
            'konsumen_id'   =>  'required',
            // 'jenis_jahitan' =>  'required',
            // 'harga'  =>  'required',
            // 'jumlah'    =>  'required',
            // 'kuantitas' =>  'required',
            'jenis_jahitan_detail'  =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->prefix = 'B';
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->created_at = $request->tanggal_order;
        $model->is_skip = $request->is_skip == 1 ? 1 : 0;
        $model->save();

        $ord = Produksi::where('id_order', $request->id)->orderBy('id_order', 'DESC')->first();
        $abjad = ord(strtolower($ord->suffix)) - 96;

        $ex = Produksi::where('id_order', $request->id);
        $diff2 = array_diff($ex->pluck('id')->toArray(), $request->produksi_id);

        if($diff2) {
            $rem = Produksi::whereIn('id', $diff2)->delete();
        }

        foreach (array_filter($request->jenis_jahitan_detail) as $idx => $value) {
            if($request->produksi_id[$idx]) {
                $produksi = Produksi::find($request->produksi_id[$idx]);
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = $request->tambahan[$idx];
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;
                $produksi->save();
            }else{
                $produksi = new Produksi;
                $produksi->prefix = 'B';
                $produksi->id_order = $model->id;
                $produksi->suffix = chr(64 + ($abjad+1));
                $produksi->id_konsumen = $model->id_konsumen;
                $produksi->jenis_jahitan = $value;
                $produksi->status_akhir = 0;
                $produksi->id_bahan = $request->bahan_id[$idx];
                $produksi->pemakaian_bahan = $request->pemakaian_bahan[$idx];
                $produksi->id_tambahan = $request->tambahan[$idx];
                $produksi->suffix_bahan = $request->suffix_bahan[$idx];
                $produksi->qty = $request->qty[$idx];
                $produksi->is_skip = $request->is_skip == 1 ? 1 : 0;
                $produksi->save();

                $abjad++;
            }
        }

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah diubah'),
            'link' => url('order/custom/edit').'/'.$model->id,
        ]);
    }
}
