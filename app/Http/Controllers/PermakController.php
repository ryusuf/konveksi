<?php

namespace App\Http\Controllers;

use App\Model\Karyawan;
use App\Model\MReferensi;
use App\Model\Order;
use App\Model\OrderPermak;
use App\Model\Produksi;
use App\Model\Tambahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermakController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.permak.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = OrderPermak::with('konsumen')->select('*', DB::raw('CONCAT(prefix,id) as kode'))->where('prefix', 'P');

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                	case 'kode':
                        $models = $models->where(DB::raw('CONCAT(prefix,id)'), $val);
                        break;
                    case 'jenis_jahitan':
                        $models = $models->where('jenis_jahitan', $val);
                        break;
                    case 'harga':
                        $models = $models->where('harga', $val);
                        break;
                    case 'jumlah':
                        $models = $models->where('jumlah', $val);
                        break;
                    case 'kuantitas':
                        $models = $models->where('kuantitas', $val);
                        break;
                    case 'status_akhir':
                        $models = $models->where('status_akhir', setStatusProduksi($val));
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                  ->orWhere('jenis_jahitan','like',"%$search%")
                  ->orWhere('harga','like',"%$search%")
                  ->orWhere('jumlah','like',"%$search%")
                  ->orWhere('kuantitas','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            $bg = 'btn-default';
            if($model->status_akhir == 1 || $model->status_akhir == 2) {
                $bg = 'btn-success';
            }elseif($model->status_akhir == 3 || $model->status_akhir == 4) {
                $bg = 'btn-info';
            }elseif($model->status_akhir == 5 || $model->status_akhir == 6) {
                $bg = 'btn-danger';
            }elseif($model->status_akhir == 7 || $model->status_akhir == 8) {
                $bg = 'btn-warning';
            }

            $model->bg_color = $bg;
        	$model->status_akhir = getStatusProduksi($model->status_akhir);
            $model->harga = "Rp ".number_format($model->harga, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
        $increment = MReferensi::where('flag', 'increment_permak')->first();

        $prefix = MReferensi::where('flag', 'prefix_permak')->first();
        $kode = $prefix->value.str_pad($increment->value, 5, '0', STR_PAD_LEFT);

    	return view('content.permak.create', compact('kode'));
    }

    public function store(Request $request)
    {
    	$model = new OrderPermak;

    	$error = $this->validate($request, [
    		'konsumen_id'	=>  'required',
    		'jenis_jahitan'	=>  'required',
    		// 'harga'	=>  'required',
    		// 'jumlah'	=>  'required',
    		// 'kuantitas'	=>  'required',
    		// 'jenis_jahitan_detail'	=>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->prefix = 'P';
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->status_akhir = 2;
        $model->created_at = $request->tanggal_order;
        $model->is_grup = $request->is_grup;
        $model->save();

    	$produksi = new Produksi;
    	$produksi->prefix = 'P';
    	$produksi->id_order = $model->id;
    	$produksi->suffix = 'A';
    	$produksi->id_konsumen = $model->id_konsumen;
    	$produksi->jenis_jahitan = $request->jenis_jahitan;
    	$produksi->status_akhir = 2;
        $produksi->id_bahan = "";
        $produksi->pemakaian_bahan = "";
        $produksi->karyawan_permak = $request->karyawan_id;
        $produksi->suffix_bahan = $request->suffix_bahan;
        $produksi->qty = $request->qty;
        $produksi->tipe = 'permak';

        if(!empty($request->tambahan)) {
            $produksi->id_tambahan = $request->tambahan;
        }

    	$produksi->save();

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah ditambahkan'),
            'link' => url('order/permak/edit').'/'.$model->id,
        ]);
    }

    public function edit($id)
    {
        $model = Order::with('produksi')->find($id);

        return view('content.permak.edit', compact('model'));
    }

    public function update(Request $request)
    {
        $model = OrderPermak::with('produksi')->find($request->id);

        $error = $this->validate($request, [
            'konsumen_id'   =>  'required',
            'jenis_jahitan' =>  'required',
            // 'harga'  =>  'required',
            // 'jumlah'    =>  'required',
            // 'kuantitas' =>  'required',
            // 'jenis_jahitan_detail'  =>  'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->prefix = 'P';
        $model->id_konsumen = $request->get('konsumen_id', '');
        // $model->jenis_jahitan = $request->get('jenis_jahitan', '');
        $model->harga = $request->get('harga', 0);
        // $model->jumlah = $request->get('jumlah', '');
        // $model->kuantitas = $request->get('kuantitas', '');
        $model->created_at = $request->tanggal_order;
        $model->save();

        $produksi = Produksi::find($model->produksi[0]->id);
        $produksi->id_konsumen = $model->id_konsumen;
        $produksi->jenis_jahitan = $request->jenis_jahitan;
        $produksi->karyawan_permak = $request->karyawan_id;
        $produksi->id_tambahan = $request->tambahan;
        $produksi->qty = $request->qty;
        $produksi->save();

        $ref = MReferensi::where('flag', 'tanggal_order')->first();
        $ref->value = $request->tanggal_order;
        $ref->save();

        return redirect()->back()->with([
            'message' => trans('Data order telah diubah'),
            'link' => url('order/permak/edit').'/'.$model->id,
        ]);
    }
}
