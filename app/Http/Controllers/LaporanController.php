<?php

namespace App\Http\Controllers;

use App\Model\Karyawan;
use App\Model\KaryawanAbsen;
use App\Model\MJasa;
use App\Model\Order;
use App\Model\Produksi;
use App\Model\Tambahan;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class LaporanController extends Controller
{
	public $startHeader = 1;
    public $startRow = 2;

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index($tipe)
    {
        $today = Carbon::now();
        if($today->dayOfWeek === Carbon::SATURDAY) {
            $start = $today->toDateString();
        }else{
            $start = Carbon::parse('last saturday')->toDateString();
        }

        $end = Carbon::parse('this friday')->toDateString();
        
    	return view('content.laporan.index', compact('tipe', 'start', 'end'));
    }

    public function bonus()
    {
        $today = Carbon::now();
        $start = $today->toDateString();
        $end = Carbon::parse('last year')->toDateString();
        
    	return view('content.laporan.bonus', compact('start', 'end'));
    }

    public function gaji()
    {
        $start = Carbon::parse('last friday')->toDateString();
        $end = Carbon::parse('this friday')->toDateString();

    	return view('content.laporan.gaji', compact('start', 'end'));
    }

    public function absen()
    {   
        $start  = Carbon::parse('this day')->toDateString();
        $end    = "";

        return view('content.laporan.absen', compact('start', 'end'));
    }

    private function makeExcel($filename,$sheetname,$header,$data,$param = [])
    {
        $excel = Excel::create($filename, function($excel) use ($data,$sheetname,$header,$param) {
            $excel->sheet(substr($sheetname,0,30), function($sheet) use ($data,$header,$param) {
                $sheet->setFontFamily('Tahoma');
                $sheet->setFontSize(12);
                $sheet->setWidth(array(
                    'A'     =>  7  * 1,
                    'B'     =>  30 * 1,
                    'C'     =>  30 * 1,
                    'D'     =>  30 * 1,
                    'E'     =>  30 * 1,
                    'F'     =>  30 * 1,
                    'G'     =>  30 * 1,
                    'H'     =>  30 * 1,
                    'I'     =>  30 * 1,
                    'J'     =>  30 * 1,
                    'K'     =>  30 * 1,
                    'L'     =>  30 * 1,
                    'M'     =>  30 * 1,
                    'N'     =>  30 * 1,
                    'O'     =>  30 * 1,
                    'P'     =>  30 * 1,
                    'Q'     =>  30 * 1,
                    'R'     =>  30 * 1,
                    'S'     =>  30 * 1,
                    'T'     =>  30 * 1,
                    'U'     =>  30 * 1,
                    'V'     =>  30 * 1,
                    'W'     =>  30 * 1,
                ));
                $alphabet = range('A','Z');
                $colalphabet = $alphabet[count($header)-1];
                $row = $header;

                $sheet->row($this->startHeader, $row);


                $sheet->cell("A$this->startHeader:".$colalphabet."$this->startHeader", function($cell) {
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                    $cell->setFontColor('#ffffff');
                    $cell->setBackground('#2532e4');
                });
                $rownum = $this->startRow;
                $i = 1;

                foreach ($data as $d) {
                    array_unshift($d, $i++);
                    $sheet->row($rownum,$d);
                    $sheet->cell('A'.$rownum.':A'.$rownum, function($cell) {
                        $cell->setAlignment('center');
                    });
                    $rownum++;
                }


                $sheet->setBorder("A$this->startHeader:".$colalphabet.($rownum-1), 'thin');
                $sheet->setFreeze("A$this->startRow");

                $sheet->setPageMargin(0.25);
                $sheet->getSheetView()->setZoomScale(80);
                $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $sheet->getPageSetup()->setFitToWidth(1);
                $sheet->getPageSetup()->setFitToHeight(0);
            });
        });
        $excel->download();
    }

    public function getLaporan(Request $request)
    {
        $params = $request->get('params',false);
        $models = Order::select('*', DB::raw('CONCAT(prefix,id) as kode'))
            ->with(['produksi' => function($q) {
                $q->select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))->with(['bahan', 'cutting', 'jahit', 'selesai_jahit', 'finishing', 'selesai_finishing', 'ekspedisi'])->orderBy('suffix', 'asc');
            }, 'konsumen']);

        $models = $models->where('status_akhir', '!=', 8);

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'tanggal_mulai':
                        $models = $models->whereHas('produksi', function($q) use($val) {
                            $q = $q->whereDate('waktu_mulai_cutting', '>=', $val)
                                ->orWhereDate('waktu_selesai_cutting', '>=', $val)
                                ->orWhereDate('waktu_mulai_jahit', '>=', $val)
                                ->orWhereDate('waktu_selesai_jahit', '>=', $val)
                                ->orWhereDate('waktu_mulai_finishing', '>=', $val)
                                ->orWhereDate('waktu_selesai_finishing', '>=', $val)
                                ->orWhereDate('waktu_mulai_ekspedisi', '>=', $val)
                                ->orWhereDate('waktu_selesai_ekspedisi', '>=', $val)
                                ->orWhere('status_akhir', 0);
                        });
                        break;
                    case 'tanggal_selesai':
                        $models = $models->whereHas('produksi', function($q) use($val) {
                            $q = $q->whereDate('waktu_mulai_cutting', '<=', $val)
                                ->orWhereDate('waktu_selesai_cutting', '<=', $val)
                                ->orWhereDate('waktu_mulai_jahit', '<=', $val)
                                ->orWhereDate('waktu_selesai_jahit', '<=', $val)
                                ->orWhereDate('waktu_mulai_finishing', '<=', $val)
                                ->orWhereDate('waktu_selesai_finishing', '<=', $val)
                                ->orWhereDate('waktu_mulai_ekspedisi', '<=', $val)
                                ->orWhereDate('waktu_selesai_ekspedisi', '<=', $val)
                                ->orWhere('status_akhir', 0);
                        });
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->whereHas('produksi', function($p) use($search) {
                    $p->where(DB::raw('CONCAT(prefix,increment_id,suffix)'), 'like', '%'.$search.'%')
                        ->orWhere('jenis_jahitan', 'like', '%'.$search.'%');
                })
                ->orWhere(DB::raw('CONCAT(prefix,id)'), 'like', '%'.$search.'%')
                ->orWhereHas('konsumen', function($p) use($search) {
                    $p->where('nrp', 'like', '%'.$search.'%')
                        ->orWhere('nama', 'like', '%'.$search.'%')
                        ->orWhere('tik', 'like', '%'.$search.'%')
                        ->orWhere('fungsi', 'like', '%'.$search.'%');
                });
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            foreach ($model->produksi as &$prod) {
                $prod->waktu_mulai_cutting = getShortDateTime($prod->waktu_mulai_cutting);
                $prod->waktu_selesai_cutting = getShortDateTime($prod->waktu_selesai_cutting);
                $prod->tanggal_order = getFullDate($prod->created_at);

                $bg = 'well';
                if($prod->status_akhir == 1) {
                    $bg = 'bg-custom-success';
                }elseif($prod->status_akhir == 3) {
                    $bg = 'bg-primary';
                }elseif($prod->status_akhir == 6) {
                    $bg = 'bg-custom-danger';
                }elseif($prod->status_akhir == 7) {
                    $bg = 'bg-custom-warning';
                }

                $prod->bg_color = $bg;
            }
            $model->status_number = $model->status_akhir;
            $model->status_akhir = getStatusProduksi($model->status_akhir);
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function getDatagaji(Request $request)
    {
        $start = Carbon::createFromFormat('Y-m-d H:i', $request->params['tanggal_mulai'].' 15:01')->toDateTimeString();
        $end = Carbon::createFromFormat('Y-m-d H', $request->params['tanggal_selesai'].' 15')->toDateTimeString();
        $params = $request->get('params',false);

        if($request->params['bagian'] != 'finishing') {

            $karyawan = Karyawan::find($request->params['karyawan_id']);

            $models = Produksi::select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->where(fieldRole($request->params['bagian']), $request->params['karyawan_id'])
                ->where(fieldSelesai($request->params['bagian']), '>=', $start)
                ->where(fieldSelesai($request->params['bagian']), '<=', $end);

            if($request->params['bagian'] == 'finishing') {
                $models = $models->orWhere('id_user_selesai_jahit', $request->params['karyawan_id'])
                    ->whereDate('waktu_selesai_jahit', '>=', $start)
                    ->whereDate('waktu_selesai_jahit', '<=', $end);
            }

            $models = $models->get();

            $data = array();
            $gaji = 0;
            $qty = 0;
            foreach ($models as $model) {
                try {
                    $jasa = MJasa::whereRaw('LOWER(bagian) = "'. strtolower($request->params['bagian']).'"')
                        ->where('jenis_jahitan', $model->jenis_jahitan)
                        ->first();

                    if($model->id_tambahan) {
                        if(!like_match("celana%", strtolower($model->jenis_jahitan))) {
                            $tambahan = Tambahan::whereIn('id', $model->id_tambahan)
                                ->where('bagian', $karyawan->bagian)
                                ->sum('biaya'); 
                        }
                    }else{
                        $tambahan = 0;
                    }

                    if($model->id_tambahan) {
                        $nama_tambahan = implode(',', Tambahan::whereIn('id', $model->id_tambahan)->pluck('jenis_tambahan')->toArray());
                    }else{
                        $nama_tambahan = null;
                    }

                    if($karyawan->bagian == 'jahit') {
                        $data[] = [
                            'kode' => $model->kode,
                            'qty' => $model->qty,
                            'jenis_jahitan' => $model->jenis_jahitan. ($nama_tambahan ? ' + ' . $nama_tambahan : ''),
                            'tanggal_selesai' => getFullDateTime($model->{fieldSelesai($request->params['bagian'])}),
                            'harga' => 'Rp. '. number_format(($jasa ? $jasa->biaya : 0) + 0, 0, '.', ','),
                            'tambahan' => 'Rp. '. number_format($tambahan + 0, 0, '.', ','),
                            'subtotal' => 'Rp. '. number_format((($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan) + 0, 0, '.', ','),
                        ];

                        $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan;
                        $qty += $model->qty;
                    }else{
                        $data[] = [
                            'kode' => $model->kode,
                            'qty' => $model->qty,
                            'jenis_jahitan' => $model->jenis_jahitan,
                            'tanggal_selesai' => getFullDateTime($model->{fieldSelesai($request->params['bagian'])}),
                            'harga' => 'Rp. '. number_format(($jasa ? $jasa->biaya : 0) + 0, 0, '.', ','),
                            'tambahan' => 0,
                            'subtotal' => 'Rp. '. number_format(($jasa ? ($model->qty * $jasa->biaya) : 0) + 0, 0, '.', ','),
                            'category' => $request->params['bagian'],
                        ];

                        $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0);
                        $qty += $model->qty;
                    }
                } catch (Exception $e) {

                }
            }

            $data[] = [
                'total_gaji' => 'Rp. '. number_format($gaji + 0, 0, '.', ','),
                'category' => $request->params['bagian'],
                'total_qty' => $qty,
            ];
        }else{
            $karyawan = Karyawan::find($request->params['karyawan_id']);

            $tanggal = KaryawanAbsen::select('tanggal')
                ->where('karyawan_id', $request->params['karyawan_id'])
                ->whereDate('tanggal', '>=', $request->params['tanggal_mulai'])
                ->whereDate('tanggal', '<=', $request->params['tanggal_selesai'])
                ->groupBy('tanggal');
            
            $tanggal = $tanggal->get();

            $gaji = 0;
            $data = [];
            foreach ($tanggal as $key => $value) {
                $masuk = KaryawanAbsen::where('tanggal', $value->tanggal)
                    ->where('karyawan_id', $request->params['karyawan_id'])
                    ->where('status', 0)
                    ->orderBy('jam_kerja', 'asc')
                    ->first();

                $pulang = KaryawanAbsen::where('tanggal', $value->tanggal)
                    ->where('karyawan_id', $request->params['karyawan_id'])
                    ->orderBy('jam_kerja', 'desc')
                    ->first();

                $tunjangan = [];
                $total_tj = 0;
                if(isset($request->params['tunjangan'])) {
                    foreach ($request->params['tunjangan'] as $key => $tj) {
                        $tunjangan[] = [
                            'nama' => $tj,
                            'harga' => tunjangan($tj),
                        ];

                        $total_tj += tunjangan($tj);
                    };
                }

                $lembur = 0;
                $lembur2 = 0;

                $gaji += $karyawan->gaji + $total_tj + $lembur + $lembur2;

                $data[] = [
                    'tanggal' => $value->tanggal,
                    'masuk' => $masuk ? $masuk->jam_kerja : '',
                    'pulang' => $pulang ? $pulang->jam_kerja : '',
                    'gaji' => $karyawan->gaji,
                    'tunjangan' => $tunjangan,
                    'total_tj' => $total_tj,
                    'lembur' => $lembur,
                    'lembur2' => $lembur2,
                    'subtotal' => $gaji,
                    'category' => $request->params['bagian'],
                ];
            }

            $data[] = [
                'total_gaji' => 'Rp. '. number_format($gaji + 0, 0, '.', ','),
                'category' => $request->params['bagian'],
            ];
        }

        return response()->json([
            'data' => $data,
        ]);
    }

    public function getGaji(Request $request)
    {
    	set_time_limit(0);
        $start = Carbon::createFromFormat('Y-m-d H:i', $request->tanggal_mulai.' 15:01')->toDateTimeString();
        $end = Carbon::createFromFormat('Y-m-d H', $request->tanggal_selesai.' 15')->toDateTimeString();

        if($request->bagian != 'finishing') {

            $karyawan = Karyawan::find($request->karyawan_id);

            $models = Produksi::select('*', DB::raw('CONCAT(prefix,increment_id,suffix) as kode'))
                ->where(fieldRole($request->bagian), $request->karyawan_id)
                ->where(fieldSelesai($request->bagian), '>=', $start)
                ->where(fieldSelesai($request->bagian), '<=', $end);

            if($request->bagian == 'finishing') {
                $models = $models->orWhere('id_user_selesai_jahit', Auth::user()->karyawan_id)
                    ->whereDate('waktu_selesai_jahit', '>=', $start)
                    ->whereDate('waktu_selesai_jahit', '<=', $end);
            }

            $models = $models->get();

            $data = array();
            $gaji = 0;
            $qty = 0;
            foreach ($models as $model) {
                try {
                    $tambahan = 0;
                    $jasa = MJasa::whereRaw('LOWER(bagian) = "'. strtolower($request->bagian).'"')
                        ->where('jenis_jahitan', $model->jenis_jahitan)
                        ->first();

                    if($model->id_tambahan) {
                        if(!like_match("celana%", strtolower($model->jenis_jahitan))) {
                            $tambahan = Tambahan::whereIn('id', $model->id_tambahan)
                                ->where('bagian', $karyawan->bagian)
                                ->sum('biaya'); 
                        }
                    }

                    if($model->id_tambahan) {
                        $nama_tambahan = implode(',', Tambahan::whereIn('id', $model->id_tambahan)->pluck('jenis_tambahan')->toArray());
                    }else{
                        $nama_tambahan = null;
                    }

                    if($karyawan->bagian == 'jahit') {
                        $data[] = [
                            'kode' => $model->kode,
                            'qty' => $model->qty,
                            'jenis_jahitan' => $model->jenis_jahitan. ($nama_tambahan ? ' + ' . $nama_tambahan : ''),
                            'tanggal_selesai' => getFullDateTime($model->{fieldSelesai($request->bagian)}),
                            'harga' => $jasa ? $jasa->biaya : 0,
                            'tambahan' => $tambahan,
                            'subtotal' => ($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan,
                        ];

                        $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0) + $tambahan;
                        $qty += $model->qty;
                    }else{
                        $data[] = [
                            'kode' => $model->kode,
                            'qty' => $model->qty,
                            'jenis_jahitan' => $model->jenis_jahitan,
                            'tanggal_selesai' => getFullDateTime($model->{fieldSelesai($request->bagian)}),
                            'harga' => $jasa ? $jasa->biaya : 0,
                            'tambahan' => 0,
                            'subtotal' => ($jasa ? ($model->qty * $jasa->biaya) : 0),
                        ];

                        $gaji += ($jasa ? ($model->qty * $jasa->biaya) : 0);
                        $qty += $model->qty;
                    }
                } catch (Exception $e) {

                }
            }

            return view('content.laporan.pdf_gaji', ['data' => $data, 'karyawan' => $karyawan, 'gaji' => $gaji, 'qty' => $qty]);
            // $pdf = PDF::loadView('content.laporan.pdf_gaji', ['data' => $data, 'karyawan' => $karyawan, 'gaji' => $gaji, 'qty' => $qty]);
            // return $pdf->download('Struk Gaji '.$karyawan->nama.'.pdf');
        }else{
            $karyawan = Karyawan::find($request->karyawan_id);

            $tanggal = KaryawanAbsen::select('tanggal')
                ->where('karyawan_id', $request->karyawan_id)
                ->whereDate('tanggal', '>=', $request->tanggal_mulai)
                ->whereDate('tanggal', '<=', $request->tanggal_selesai)
                ->groupBy('tanggal');
            
            $tanggal = $tanggal->get();

            $gaji = 0;
            $data = [];
            foreach ($tanggal as $key => $value) {
                $masuk = KaryawanAbsen::where('tanggal', $value->tanggal)
                    ->where('karyawan_id', $request->karyawan_id)
                    ->where('status', 0)
                    ->orderBy('jam_kerja', 'asc')
                    ->first();

                $pulang = KaryawanAbsen::where('tanggal', $value->tanggal)
                    ->where('karyawan_id', $request->karyawan_id)
                    ->orderBy('jam_kerja', 'desc')
                    ->first();

                $tunjangan = [];
                $total_tj = 0;

                if(isset($request->tunjangan)) {
                    foreach ($request->tunjangan as $key => $tj) {
                        $tunjangan[] = [
                            'nama' => $tj,
                            'harga' => tunjangan($tj),
                        ];

                        $total_tj += tunjangan($tj);
                    };
                }

                $lembur = 0;
                $lembur2 = 0;

                $gaji += $karyawan->gaji + $total_tj + $lembur + $lembur2;

                $data[] = [
                    'tanggal' => $value->tanggal,
                    'masuk' => $masuk ? $masuk->jam_kerja : '',
                    'pulang' => $pulang ? $pulang->jam_kerja : '',
                    'gaji' => $karyawan->gaji,
                    'tunjangan' => $tunjangan,
                    'total_tj' => $total_tj,
                    'lembur' => $lembur,
                    'lembur2' => $lembur2,
                    'subtotal' => $gaji,
                ];
            }
            
            return view('content.laporan.pdf_gaji_finishing', ['data' => $data, 'karyawan' => $karyawan, 'gaji' => $gaji]);

            // $pdf = PDF::loadView('content.laporan.pdf_gaji_finishing', ['data' => $data, 'karyawan' => $karyawan, 'gaji' => $gaji]);
            // return $pdf->download('Struk Gaji '.$karyawan->nama.'.pdf');
        }
    }

    public function getAbsen(Request $request)
    {
        $params = $request->get('params',false);
        $models = KaryawanAbsen::select('karyawan_absen.*','karyawan.nama')
            ->join('karyawan','karyawan.id','=','karyawan_absen.karyawan_id');

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);

        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                else{
                    switch ($key) {
                        case 'tanggal_mulai':
                            $models = $models->whereDate('tanggal', '>=', $val);
                            break;
                        case 'tanggal_selesai':
                            $models = $models->whereDate('tanggal', '<=', $val);
                            break;
                        
                        case 'karyawan_id':
                            $models = $models->where('karyawan_id', '=', $val);
                            break;

                        default:
                            $models = $models->where($key,$val);
                            break;
                    }
                }
            }
        }

        if ($search != '') {
            $models = $models->where('karyawan.nama','like','%$search');
        }

        $count = $models->count();

        $page       = $request->get('page',1);
        $perpage    = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        
        // foreach ($models as &$model) {
        //     foreach ($model->produksi as &$prod) {
        //         $prod->waktu_mulai_cutting = getShortDateTime($prod->waktu_mulai_cutting);
        //         $prod->waktu_selesai_cutting = getShortDateTime($prod->waktu_selesai_cutting);

        //         $bg = 'well';
        //         if($prod->status_akhir == 1) {
        //             $bg = 'bg-success';
        //         }elseif($prod->status_akhir == 3) {
        //             $bg = 'bg-info';
        //         }elseif($prod->status_akhir == 5) {
        //             $bg = 'bg-danger';
        //         }elseif($prod->status_akhir == 7) {
        //             $bg = 'bg-warning';
        //         }

        //         $prod->bg_color = $bg;
        //     }
        //     $model->status_number = $model->status_akhir;
        //     $model->status_akhir = getStatusProduksi($model->status_akhir);
        // }


        // dd($models);

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function getBonus(Request $request)
    {
        $today = Carbon::now();
        $start = Carbon::parse('last year')->toDateString();
        $end = $today->toDateString();

        $karyawan = Karyawan::find($request->karyawan_id);
        $masakerja = date('Y') - (int) $karyawan->tahun_kerja;

        $absen = KaryawanAbsen::where('karyawan_id', $request->karyawan_id)
            ->whereBetween('tanggal', [$start, $end]);

        $disiplin = $absen->where('status', 1)->count();
        
        $hari = $absen->count();
        $total_hari = 300;
        
        $persentase_kehadiran = round((($hari / $total_hari) * 100), 2 );
        $persentase_kedisiplinan = round((($disiplin / $total_hari) * 100), 2);

        $kehadiran =  $persentase_kehadiran / 100 * getMasaKerja($masakerja);
        $kedisiplinan = $persentase_kedisiplinan / 100 * getMasaKerja($masakerja);
        $kebijakan = 0;

        return response()->json([
            'karyawan' => $karyawan,
            'masa_kerja' => $masakerja,
            'jumlah_hari_kerja' => $total_hari,
            'jumlah_kehadiran' => $hari,
            'persentase_kehadiran' => $persentase_kehadiran,
            'persentase_kedisiplinan' => $persentase_kedisiplinan,
            'bonus_kehadiran' => 'Rp. '. number_format(($kehadiran) + 0, 0, '.', ','),
            'bonus_kedisiplinan' => 'Rp. '. number_format(($kedisiplinan) + 0, 0, '.', ','),
            'bonus_kebijakan' => 'Rp. '. number_format(($kebijakan) + 0, 0, '.', ','),
            'total' => 'Rp. '. number_format(($kehadiran + $kedisiplinan + $kebijakan) + 0, 0, '.', ','),
        ]);
    }

}
