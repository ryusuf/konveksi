<?php

namespace App\Http\Controllers;

use App\KaryawanAbsen;
use App\Model\Karyawan;
use App\User;
use Illuminate\Http\Request;
use File;
use Excel;

class KaryawanController extends Controller
{
    public $startHeader     = 1;
    public $startRow        = 2;

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.karyawan.index');
    }

    public function kehadiran()
    {
        return view('content.karyawan.kehadiran');
    }

    public function simpanKehadiran(Request $request)
    {
        $check = ($request->file('file')->getClientMimeType() == 'application/vnd.ms-excel') || $request->file('file')->getClientMimeType() == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        if(!$check) {
            return redirect()->back()->with([
                'message_title' => 'Terjadi Kesalahan',
                'message'       => trans('File harus dalam format xls'),
                'message_type'  => 'error'
            ]);
        }

        if(!$request->hasFile('file')) {
            return redirect()->back()->with([
                'message_title' => 'Terjadi Kesalahan',
                'message'       => trans('File harus diisi'),
                'message_type'  => 'error'
            ]);
        }

        $files = $request->file('file');
        $filename = time().'_'. $files->getClientOriginalName();
        
        $destinationPath = public_path('uploads/excel');
        $name = $files->move($destinationPath,$filename);
        
        $array = collect([]);

        $file = $name->getPathName();
        
        Excel::load($file, function($reader) use (&$array, $request) {
            $results = $reader->get();

            foreach ($results as $key => $result) {
                $tanggal = \Carbon\Carbon::createFromFormat('m/d/Y', $result['date'])->toDateString();

                $model = new KaryawanAbsen;
                $model->karyawan_id = $result['no.'];
                $model->tanggal = $tanggal;
                $model->jam_kerja = $result['att_time'];
                $model->jam_lembur = $result['ot_time'];
                $model->is_telat = $result['late'];
                $model->save();
            }
        });

        File::delete($file);

        return redirect()->back()->with([
            'message' => trans('Kehadiran karyawan telah ditambahkan'),
        ]);
    }

    private function makeExcels($filename,$doc)
    {
        $excel = Excel::create($filename, function($excel) use ($doc) {

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setWrapText(true);

            foreach ($doc as $key => $value) {
                $data           = $value['data'];
                $sheetname      = $value['sheetname'];
                $header         = $value['header'];
                $param          = $value['param'];

                $excel->sheet(substr($sheetname,0,30), function($sheet) use ($data,$header,$param, $sheetname) {
                    $sheet->setFontFamily('Tahoma');
                    $sheet->setFontSize(12);
                    $sheet->setWidth(array(
                        'A'     =>  7  * 1,
                        'B'     =>  30 * 1,
                        'C'     =>  30 * 1,
                        'D'     =>  30 * 1,
                        'E'     =>  30 * 1,
                        'F'     =>  30 * 1,
                        'G'     =>  30 * 1,
                        'H'     =>  30 * 1,
                        'I'     =>  30 * 1,
                        'J'     =>  30 * 1,
                        'K'     =>  30 * 1,
                        'L'     =>  30 * 1,
                        'M'     =>  30 * 1,
                        'N'     =>  30 * 1,
                        'O'     =>  30 * 1,
                        'P'     =>  30 * 1,
                        'Q'     =>  30 * 1,
                        'R'     =>  30 * 1,
                        'S'     =>  30 * 1,
                        'T'     =>  30 * 1,
                        'U'     =>  30 * 1,
                        'V'     =>  30 * 1,
                        'W'     =>  30 * 1,
                        'X'     =>  30 * 1,
                        'Y'     =>  30 * 1,
                    ));
                    $alphabet = range('A','Z');
                    $colalphabet = $alphabet[count($header)-1];
                    $row = $header;

                    $sheet->row($this->startHeader, $row);

                    $sheet->cell("A$this->startHeader:".$colalphabet."$this->startHeader", function($cell) {
                        $cell->setAlignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setFontColor('#ffffff');
                        $cell->setBackground('#2532e4');
                    });
                    $rownum = $this->startRow;
                    $i = 1;

                    foreach ($data as $d) {
                        array_unshift($d, $i++);
                        $sheet->row($rownum,$d);

                        if(strpos($sheetname, 'order') !== false) {
                            $sheet->cell('B'.$rownum.':B'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('K'.$rownum.':K'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                            $sheet->cell('C'.$rownum.':J'.$rownum, function($cell) {
                                $cell->setAlignment('center');
                            });
                        } else {
                            $sheet->cell('A'.$rownum.':A'.$rownum, function($cell) {
                                $cell->setAlignment('left');
                            });
                        }

                        $rownum++;
                    }

                    $sheet->setBorder("A$this->startHeader:".$colalphabet.($rownum-1), 'thin');
                    $sheet->setFreeze("A$this->startRow");

                    $sheet->setPageMargin(0.25);
                    $sheet->getSheetView()->setZoomScale(80);
                    $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                    $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    $sheet->getPageSetup()->setFitToWidth(1);
                    $sheet->getPageSetup()->setFitToHeight(0);
                });
            }
        
        });
        $excel->download();
    }

    public function download(Request $request)
    {
        $filename           = "Daftar Karyawan";
        $data   = [];
        $result = [];

        $models = new Karyawan;
        $models = $models->get();
        foreach ($models as &$model) {
            $model->gaji = "Rp ".number_format($model->gaji, 0, ".", ",");
            $model->kasbon = "Rp ".number_format($model->kasbon, 0, ".", ",");

            $result[]   = array(
                "Nama"          => $model->nama,
                "No KTP"       => $model->no_ktp,
                "Bagian"     => $model->bagian,
                "Alamat"     => $model->alamat,
                "Kasbon"         => $model->kasbon,
                "Gaji"        => $model->gaji,
            );
        }

        $data[]   = array(
            'sheetname' =>  "Karyawan", 
            'header'    =>  ["No","Nama","No KTP","Bagian","Alamat","Kasbon","Gaji"],
            'data'      =>  $result,
            'param'     =>  [] 

        );

        $this->makeExcels($filename,$data);
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = new Karyawan;

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'nama':
                        $models = $models->where('nama', $val);
                        break;
                    case 'no_ktp':
                        $models = $models->where('no_ktp', $val);
                        break;
                    case 'alamat':
                        $models = $models->where('alamat', $val);
                        break;
                    case 'bagian':
                        $models = $models->where('bagian', $val);
                        break;
                    case 'kasbon':
                        $models = $models->where('kasbon', $val);
                        break;
                    case 'gaji':
                        $models = $models->where('gaji', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('nama','like',"%$search%")
                  ->orWhere('no_ktp','like',"%$search%")
                  ->orWhere('alamat','like',"%$search%")
                  ->orWhere('bagian','like',"%$search%")
                  ->orWhere('kasbon','like',"%$search%")
                  ->orWhere('gaji','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
            $model->gaji = "Rp ".number_format($model->gaji, 0, ".", ",");
            $model->kasbon = "Rp ".number_format($model->kasbon, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
    	return view('content.karyawan.create');
    }

    public function store(Request $request)
    {
    	$model = new Karyawan;

    	$error = $this->validate($request, [
            'nama'		=>  'required',
            'no_ktp'	=>  'required',
            'alamat'	=>  'required',
            'bagian'	=>  'required',
            'username'      =>  'required|unique:users',
            'email' => 'required|email|unique:users',
            'tahun_kerja' => 'required',
        ]);

        if($request->password != $request->konfirmasi_password) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Password tidak sama',
                    'message_type' => 'error',
                ]);
        }

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->nama = $request->get('nama', '');
        $model->no_ktp = $request->get('no_ktp', '');
        $model->alamat = $request->get('alamat', '');
        $model->bagian = $request->get('bagian', '');
        $model->tahun_kerja = $request->get('tahun_kerja', '');
        $model->kasbon = $request->kasbon ? $request->kasbon : 0;
        $model->gaji = $request->gaji ? $request->gaji : 0;
        
        if($model->save()) {
            $user = new User;
            $user->name = $request->get('nama', 'user');
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role_id = 'karyawan';
            $user->karyawan_id = $model->id;
            $user->save();
        }

        return redirect()->back()->with([
            'message' => trans('Karyawan telah ditambahkan'),
            'link' => url('karyawan/edit').'/'.$model->id,
        ]);
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Karyawan gagal dihapus'
        ];

        $model = Karyawan::find($request->id);
        
        if($model->delete()){
            User::where('karyawan_id', $request->id)->delete();

            $result = [
                'success' => true,
                'message' => 'Karyawan berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = Karyawan::with('user')->find($id);

    	return view('content.karyawan.view', compact('model'));
    }

    public function edit($id)
    {
    	$model = Karyawan::with('user')->find($id);

    	return view('content.karyawan.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = Karyawan::find($request->id);

    	$error = $this->validate($request, [
            'nama'		=>  'required',
            'no_ktp'	=>  'required',
            'alamat'	=>  'required',
            'bagian'	=>  'required',
            'gaji'	=>  'required',
            'tahun_kerja' => 'required',
        ]);

        if($request->password != $request->konfirmasi_password) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => 'Password tidak sama',
                    'message_type' => 'error',
                ]);
        }

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }

        $model->nama = $request->get('nama', '');
        $model->no_ktp = $request->get('no_ktp', '');
        $model->alamat = $request->get('alamat', '');
        $model->bagian = $request->get('bagian', '');
        $model->kasbon = $request->kasbon ? $request->kasbon : 0;
        $model->gaji = $request->get('gaji', 0);
        $model->tahun_kerja = $request->get('tahun_kerja', '');

        if($model->save()) {
            $user = User::where('karyawan_id', $model->id)->first();
            $user->name = $request->get('nama', 'user');
            $user->username = $request->username;
            $user->email = $request->email;

            if(strlen($request->password) > 0) {
                $user->password = bcrypt($request->password);
            }

            $user->role_id = 'karyawan';
            $user->karyawan_id = $model->id;
            $user->save();
        }

        return redirect()->back()->with([
            'message' => trans('Data karyawan telah diubah'),
            'link' => url('karyawan/edit').'/'.$model->id,
        ]);
    }

    public function getKaryawan(Request $request)
    {
        if($request->bagian != '') {
            $model = Karyawan::where('bagian', $request->bagian)->pluck('nama', 'id');
        }else{
            $model = Karyawan::pluck('nama', 'id');
        }

        $model->transform(function($nama,$id) {
            $find = Karyawan::find($id);
            return $nama." - ".$find->bagian;
        });

        return response()->json($model);
    }
}
