<?php

namespace App\Http\Controllers;

use App\Model\MReferensi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PengaturanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ref = MReferensi::where('flag', 'kode_order')->first();
        if(Auth::user()->role_id != 'admin') {
            return redirect('home');
        }else{
            return view('content.pengaturan.kode', compact('ref'));
        }
    }
}
