<?php

namespace App\Http\Controllers;

use App\Model\Tambahan;
use Illuminate\Http\Request;

class TambahanController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	return view('content.tambahan.index');
    }

    public function getData(Request $request)
    {
    	$params = $request->get('params',false);
        $models = new Tambahan;

        $search = $request->get('search',false);
        $order  = $request->get('order' ,false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch($key) {
                    case 'jenis_tambahan':
                        $models = $models->where('jenis_tambahan', $val);
                        break;
                    case 'biaya':
                        $models = $models->where('biaya', $val);
                        break;
                    case 'bagian':
                        $models = $models->where('bagian', $val);
                        break;
                    default:
                        $models = $models->where($key,$val);
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function($q) use ($search) {
                $q->where('jenis_tambahan','like',"%$search%")
                  ->orWhere('biaya','like',"%$search%")
                  ->orWhere('bagian','like',"%$search%");
            });
        }
        $count = $models->count();

        $page = $request->get('page',1);
        $perpage = $request->get('perpage',20);

         if ($order) {
            $order_direction = $request->get('order_direction','asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                default:
                    $models = $models->orderBy($order,$order_direction);
                    break;
            }
        }
        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();
        foreach ($models as &$model) {
        	$model->biaya = "Rp ".number_format($model->biaya, 0, ".", ",");
        }

        $result = [
            'data' => $models,
            'count' => $count
        ];

        return response()->json($result);
    }

    public function create()
    {
    	return view('content.tambahan.create');
    }

    public function store(Request $request)
    {
    	$model = new Tambahan;

    	$error = $this->validate($request, [
            'bagian'    =>  'required',
    		'jenis_tambahan'	=>  'required',
            'biaya'    =>  'required',
        ]);

        if($error) {
            if($request->ajax()) {
                return response()->json([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }else{
                return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
            }
        }
        
        $model->bagian = $request->get('bagian', '');
        $model->jenis_tambahan = $request->get('jenis_tambahan', '');
        $model->biaya = $request->biaya ? $request->biaya : 0;
        $model->save();

        if($request->ajax()) {
            return response()->json([
                'message' => 'Tambahan berhasil ditambahkan',
                'message_type' => 'ok',
            ]);
        }else{
            return redirect()->back()->with([
                'message' => trans('Data tambahan telah ditambahkan'),
                'link' => url('tambahan/edit').'/'.$model->id,
            ]);
        }
    }

    public function delete(Request $request)
    {
    	$result = [
            'success' => false,
            'message' => 'Data tambahan gagal dihapus'
        ];

        $model = Tambahan::find($request->id);
        
        if($model->delete()){
            $result = [
                'success' => true,
                'message' => 'Data tambahan berhasil dihapus'
            ];
        }

        return response()->json($result);
    }

    public function view($id)
    {
    	$model = Tambahan::find($id);

    	return view('content.tambahan.view', compact('model'));
    }

    public function edit($id)
    {
    	$model = Tambahan::find($id);

    	return view('content.tambahan.edit', compact('model'));
    }

    public function update(Request $request)
    {
    	$model = Tambahan::find($request->id);

    	$error = $this->validate($request, [
    		'jenis_tambahan'	=>  'required',
            'biaya'    =>  'required',
            'bagian' => 'required',
        ]);

        if($error) {
            return redirect()->back()
                ->withInput($request->all())
                ->with([
                    'message' => $message,
                    'message_type' => 'error',
                ]);
        }
        
        $model->bagian = $request->get('bagian', '');
        $model->jenis_tambahan = $request->get('jenis_tambahan', '');
        $model->biaya = $request->biaya ? $request->biaya : 0;
        $model->save();

        return redirect()->back()->with([
            'message' => trans('Data tambahan telah diubah'),
            'link' => url('tambahan/edit').'/'.$model->id,
        ]);
    }

    public function getTambahan()
    {
        $model = Tambahan::groupBy('jenis_tambahan', 'id')->pluck('jenis_tambahan', 'id');

        $model->transform(function($nama,$id) {
            $find = Tambahan::find($id);
            return $nama." - Rp.".number_format($find->biaya, 0, '.', ',');
        });

        return response()->json($model);
    }
}
