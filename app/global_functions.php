<?php
    function getVersion() {
        return '0.0.1';
    }
    function getTahun() {
        return intval(session('tahun') ? session('tahun') : date('Y'));
    }
    function isAllowedUpload($ext) {
        $all = array('jpg','jpeg','pdf');
        return in_array(strtolower($ext),$all);
    }
    function renderContent($content) {
        return nl2br(htmlspecialchars($content,false));
    }
    function formatWaktu($waktu) {
        return date("H:i",strtotime($waktu));
    }
    function listBulan() {
        return [""=>"-",1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    }

    function listBulanIndo() {
        return [""=>"-", '01' =>'Januari', '02' =>'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];
    }

    function listHari() {
        return [""=>"-",1=>'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];
    }
    function listBulanShort() {
        return [""=>"-",1=>'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
    }
    function getBulan($bulan) {
        $array = [1=>'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        return $array[$bulan];
    }
    function getFullDate($time = 0) {
        if ($time == "") return $time;
        if ($time && $time != "0000-00-00") {
            $time = strtotime($time);
        } else {
            return "-";
        }
        return date("j",$time)." ".listBulan()[date("n",$time)]." ".date("Y",$time);
    }

    function getFullDateTime($time = 0) {
        $time = strtotime($time);
        return date("j",$time)." ".listBulan()[date("n",$time)]." ".date("Y H:i",$time);
    }
    
    function getShortDate($time = 0) {
        if ($time == "") return $time;
        if ($time && $time != "0000-00-00") {
            $time = strtotime($time);
        } else {
            return "-";
        }
        return date("j",$time)." ".listBulanShort()[date("n",$time)]." ".date("Y",$time);
    }

    function getShortDateTime($time = 0) {
        if ($time == "") return $time;
        if ($time && $time != "0000-00-00") {
            $time = strtotime($time);
        } else {
            return "-";
        }
        return date("j",$time)." ".listBulanShort()[date("n",$time)]." ".date("Y",$time)." ".date('h:i A', $time);
    }

    function renderSelectOption($data) {
        $result = [];
        foreach ($data as $key => $value) {
            $result[] = [
                'id' => $key,
                'text' => $value
            ];
        }
        return json_encode($result);
    }
    
    function array_filter_by_fn($array,$func) {
        $keys = array_keys($array);
        $result = [];
        foreach ($array as $key => $value) {
            if ($func($value,$key)) {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    function moveFile($from, $to) {
        $filename = basename($from);
        $destinationPath = dirname($to);
        $first = true;
        while (File::exists("$destinationPath/$filename")) {
            $extension_pos = strrpos($filename, '.');
            $filename = substr($filename, 0, $extension_pos) . ($first?'-':'').str_random(5) . substr($filename, $extension_pos);
            $first = false;
        }
        File::move($from,$destinationPath.'/'.$filename);
        return $filename;
    }


    function getRevisi() {
        $model = App\Model\SubKegiatanRev::select('revisi')->where('tahun', getTahun())->where('kantor_id', Auth::user()->kantor_id)->groupBy('revisi')->pluck('revisi');

        return $model;
    }

    function cctor($obj) {
        return clone $obj;
    }
    
    function kekata($x) {
        $x = abs($x);
        $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
        $temp = "";

        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = kekata($x - 10). " belas";
        } else if ($x <100) {
            $temp = kekata($x/10)." puluh". kekata($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . kekata($x - 100);
        } else if ($x <1000) {
            $temp = kekata($x/100) . " ratus" . kekata($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . kekata($x - 1000);
        } else if ($x <1000000) {
            $temp = kekata($x/1000) . " ribu" . kekata($x % 1000);
        } else if ($x <1000000000) {
            $temp = kekata($x/1000000) . " juta" . kekata($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = kekata($x/1000000000) . " milyar" . kekata(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = kekata($x/1000000000000) . " trilyun" . kekata(fmod($x,1000000000000));
        }     
        
        return $temp;
    }

    function terbilang($x, $style=4) {
        if($x<0) {
            $hasil = "minus ". trim(kekata($x));
        } else {
            $hasil = trim(kekata($x));
        }

        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
             
        return $hasil;
    }

    function Zip($source, $destination, $type = '')
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', $file);
                
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true)
                {
                    $newFile = str_replace(realpath($source). DIRECTORY_SEPARATOR , '', $file);

                    if($type == 'spj') {
                        $ex = explode(DIRECTORY_SEPARATOR, $newFile);
                        $atase = \App\Model\Kantor::find($ex[0]);

                        $kantor = isset($atase->nama) ? $atase->nama : $ex[0];
                        
                        $ex[0] = $kantor;

                        $newFile = implode(DIRECTORY_SEPARATOR, $ex);
                    }

                    $zip->addEmptyDir($newFile);
                }
                else if (is_file($file) === true)
                {
                    $source2 = str_replace(realpath($source). DIRECTORY_SEPARATOR, '', $file);

                    if($source2 == '.gitkeep') {
                        continue;
                    }else{
                        if($type == 'spj') {
                            $ex = explode(DIRECTORY_SEPARATOR, $source2);
                            $atase = \App\Model\Kantor::find($ex[0]);

                            $kantor = isset($atase->nama) ? $atase->nama : $ex[0];
                            
                            $ex[0] = $kantor;

                            $source2 = implode(DIRECTORY_SEPARATOR, $ex);
                        }
                        
                        $zip->addFromString($source2, file_get_contents($file));
                    }
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    function getRemisTerakhir($tahun, $kantor_id) {
        $remis_terakhir = App\Model\Remis::whereRaw('year(tanggal) = '.$tahun)->where('kantor_id',$kantor_id)->orderBy('id', 'desc')->first();

        if($remis_terakhir) {
            $remis_terakhir->jumlah_usd = $remis_terakhir->jumlah_usd - $remis_terakhir->biaya_pengiriman;
            // $remis_terakhir->jumlah_valas = $remis_terakhir->jumlah_valas - ($remis_terakhir->biaya_pengiriman * $remis_terakhir->kurs_usd_valas);
            $remis_terakhir->jumlah_valas = getJumlahValas($remis_terakhir);
            $remis_terakhir->jumlah_rupiah = $remis_terakhir->jumlah_rupiah - ($remis_terakhir->biaya_pengiriman * $remis_terakhir->kurs_rupiah_usd);

            $remis_terakhir->pengembalian_usd = $remis_terakhir->pengembalian_usd - $remis_terakhir->biaya_pengiriman;
            $remis_terakhir->pengembalian_valas = $remis_terakhir->pengembalian_valas - ($remis_terakhir->biaya_pengiriman * $remis_terakhir->kurs_usd_valas);
            $remis_terakhir->pengembalian_rupiah = $remis_terakhir->pengembalian_rupiah - ($remis_terakhir->biaya_pengiriman * $remis_terakhir->kurs_rupiah_usd);
        }

        return $remis_terakhir;
    }

    function getRemis($tahun, $kantor_id) {
        $remis = App\Model\Remis::whereRaw('year(tanggal) = '.$tahun)->where('kantor_id',$kantor_id)->get();

        foreach ($remis as &$rem) {
            // dd(sumTotalKonversi($rem->id));
            // dd(sumJumlahKonversi($rem->id));

            $rem->jumlah_usd = $rem->jumlah_usd - $rem->biaya_pengiriman;
            // $rem->jumlah_valas = $rem->jumlah_valas - ($rem->biaya_pengiriman * $rem->kurs_usd_valas);
            $rem->jumlah_valas = getJumlahValas($rem);
            $rem->jumlah_rupiah = $rem->jumlah_rupiah - ($rem->biaya_pengiriman * $rem->kurs_rupiah_usd);

            $rem->pengembalian_usd = $rem->pengembalian_usd - $rem->biaya_pengiriman;
            $rem->pengembalian_valas = $rem->pengembalian_valas - ($rem->biaya_pengiriman * $rem->kurs_usd_valas);
            $rem->pengembalian_rupiah = $rem->pengembalian_rupiah - ($rem->biaya_pengiriman * $rem->kurs_rupiah_usd);
        }

        return $remis;
    }

    function sumJumlahKonversi($id) {
        $total = App\Model\RemisKonversi::where('remis_id', $id);
        
        return $total->sum('jumlah_usd');
    }

    function sumTotalKonversi($id) {
        $total = App\Model\RemisKonversi::where('remis_id', $id);
        
        return $total->sum('total');
    }

    function getJumlahValas($remis)
    {
        $jumlah = sumTotalKonversi($remis->id) + (($remis->jumlah_usd - sumJumlahKonversi($remis->id)) - $remis->biaya_pengiriman) * $remis->kurs_usd_valas;

        return $jumlah;
    }

    function getTotalRemis($remis, $tipe) {
        $total = 0;

        if($tipe == 'rupiah') {
            $total = $remis->sum('jumlah_rupiah') - $remis->sum('pengembalian_rupiah');
        }elseif($tipe == 'valas') {
            $total = $remis->sum('jumlah_valas') - $remis->sum('pengembalian_valas');
        }elseif($tipe == 'usd') {
            $total = $remis->sum('jumlah_usd') - $remis->sum('pengembalian_usd');
        }
        
        return $total;
    }

    function getTotalPengiriman($remis, $tipe) {
        $total = 0;

        if($tipe == 'rupiah') {
            foreach ($remis as $key => $value) {
                $total += $value->biaya_pengiriman * $value->kurs_rupiah_usd;
            }
        }elseif($tipe == 'valas') {
            foreach ($remis as $key => $value) {
                $total += $value->biaya_pengiriman * $value->kurs_usd_valas;
            }
        }elseif($tipe == 'usd') {
            foreach ($remis as $key => $value) {
                $total += $value->biaya_pengiriman;
            }
        }
        
        return $total;
    }

    function getStatusProduksi($id)
    {
        if($id == 0) {
            $status = 'Belum diproses';
        }elseif($id == 1) {
            $status = 'Proses Cutting';
        }elseif($id == 2) {
            $status = 'Selesai Cutting';
        }elseif($id == 3) {
            $status = 'Proses Jahit';
        }elseif($id == 4) {
            $status = 'Selesai Jahit';
        }elseif($id == 5) {
            $status = 'Proses Finishing';
        }elseif($id == 6) {
            $status = 'Selesai Finishing';
        }elseif($id == 7) {
            $status = 'Proses Ekspedisi';
        }elseif($id == 8) {
            $status = 'Selesai';
        }/*elseif($id == 9) {
            $status = 'Selesai';
        }elseif($id == 10) {
            $status = 'Selesai';
        }*/else{
            $status = 'Belum diproses';
        }

        return $status;
    }

    function setStatusProduksi($id)
    {
        if($id == 'Belum diproses') {
            $status = 0;
        }elseif($id == 'Proses Cutting') {
            $status = 1;
        }elseif($id == 'Selesai Cutting') {
            $status = 2;
        }elseif($id == 'Proses Jahit') {
            $status = 3;
        }elseif($id == 'Selsai Jahit') {
            $status = 4;
        }elseif($id == 'Proses Finishing') {
            $status = 5;
        }elseif($id == 'Selesai Finishing') {
            $status = 6;
        }elseif($id == 'Proses Ekspedisi') {
            $status = 7;
        }elseif($id == 'Selesai') {
            $status = 8;
        }/*elseif($id == 'Proses Finishing') {
            $status = 9;
        }elseif($id == 'Selesai') {
            $status = 9;
        }*/else{
            $status = 0;
        }

        return $status;
    }

    function getMulaiRole($bagian)
    {
        if($bagian == 'cutting') {
            $status = 'created_at';
        }elseif($bagian == 'jahit') {
            $status = 'waktu_selesai_cutting';
        }elseif($bagian == 'finishing') {
            $status = 'waktu_selesai_jahit';
        }elseif($bagian == 'ekspedisi') {
            $status = 'waktu_selesai_packing';
        }else{
            $status = 'waktu_selesai_cutting';
        }

        return $status;
    }

    function fieldRole($bagian)
    {
        $status = '';
        if($bagian == 'cutting') {
            $status = 'id_karyawan_cutting';
        }elseif($bagian == 'jahit') {
            $status = 'id_karyawan_jahit';
        }elseif($bagian == 'finishing') {
            $status = 'id_karyawan_finishing';
        }elseif($bagian == 'ekspedisi') {
            $status = 'id_karyawan_ekspedisi';
        }

        return $status;
    }

    function fieldSelesai($bagian)
    {
        if($bagian == 'cutting') {
            $status = 'waktu_selesai_cutting';
        }elseif($bagian == 'jahit') {
            $status = 'waktu_selesai_jahit';
        }elseif($bagian == 'finishing') {
            $status = 'waktu_selesai_finishing';
        }elseif($bagian == 'ekspedisi') {
            $status = 'waktu_selesai_ekspedisi';
        }

        return $status;
    }

    function fieldMulaiRole($bagian)
    {
        if($bagian == 'cutting') {
            $status = 'waktu_mulai_cutting';
        }elseif($bagian == 'jahit') {
            $status = 'waktu_mulai_jahit';
        }elseif($bagian == 'finishing') {
            $status = 'waktu_mulai_finishing';
        }elseif($bagian == 'ekspedisi') {
            $status = 'waktu_mulai_ekspedisi';
        }else{
            $status = 'created_at';
        }

        return $status;
    }

    function setStatusRole($id)
    {
        if($id == 'cutting') {
            $status = 0;
        }elseif($id == 'jahit') {
            $status = 2;
        }elseif($id == 'finishing') {
            $status = 4;
        }elseif($id == 'ekspedisi') {
            $status = 6;
        }/*elseif($id == 'finishing') {
            $status = 8;
        }*/else{
            $status = 0;
        }

        return $status;
    }

    function setStatusSelesaiRole($id)
    {
        if($id == 'cutting') {
            $status = 1;
        }elseif($id == 'jahit') {
            $status = 3;
        }elseif($id == 'finishing') {
            $status = 5;
        }elseif($id == 'ekspedisi') {
            $status = 7;
        }else{
            $status = 0;
        }

        return $status;
    }

    function like_match($pattern, $subject) {
        $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));

        return (bool) preg_match("/^{$pattern}$/i", $subject);
    }

    function getTanggalOrder()
    {
        $data = \App\Model\MReferensi::where('flag', 'tanggal_order')->first();
        return $data ? $data->value : date('Y-m-d');
    }

    function ddd(...$args){
        http_response_code(500);
        call_user_func_array('dd', $args);
    }

    function Parse_Data($data,$p1,$p2){
        $data=" ".$data;
        $hasil="";
        $awal=strpos($data,$p1);
        if($awal!=""){
            $akhir=strpos(strstr($data,$p1),$p2);
            if($akhir!=""){
                $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
            }
        }
        return $hasil;  
    }

    function tunjangan($name) {
        $tunjangan = 0;

        if($name == 'finishing') {
            $tunjangan = 1500;
        }elseif($name == 'bordir') {
            $tunjangan = 1500;
        }elseif($name == 'wilcom') {
            $tunjangan = 1500;
        }elseif($name == 'steam') {
            $tunjangan = 1500;
        }elseif($name == 'qc') {
            $tunjangan = 1500;
        }

        return $tunjangan;
    }

    function getMasaKerja($total) {
        $bonus = 0;
        if($total <= 1) {
            $bonus = 750000;
        } else if($total == 2) {
            $bonus = 1500000;
        } else if($total == 3) {
            $bonus = 2000000;
        } else if($total == 4) {
            $bonus = 2500000;
        } else if($total == 5) {
            $bonus = 3000000;
        } else if($total == 6) {
            $bonus = 3500000;
        } else if($total == 7) {
            $bonus = 4000000;
        } else if($total == 8) {
            $bonus = 4500000;
        } else if($total == 9) {
            $bonus = 5000000;
        } else if($total == 10) {
            $bonus = 5500000;
        } else if($total == 11) {
            $bonus = 6000000;
        } else if($total == 12) {
            $bonus = 6500000;
        } else if($total == 13) {
            $bonus = 7000000;
        } else if($total == 14) {
            $bonus = 7500000;
        } else if($total == 15) {
            $bonus = 8000000;
        } else if($total == 16) {
            $bonus = 8500000;
        } else if($total == 17) {
            $bonus = 9000000;
        } else if($total == 18) {
            $bonus = 9500000;
        } else if($total >= 19) {
            $bonus = 10000000;
        }

        return $bonus;
    }
?>
