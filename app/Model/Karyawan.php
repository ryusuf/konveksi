<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';

    public function user()
    {
    	return $this->hasOne('App\User', 'karyawan_id', 'id');
    }
}
