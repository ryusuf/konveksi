<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\BaseModel;

class Produksi extends BaseModel
{
    protected $table = 'produksi';

    protected $casts = [
        'id_tambahan' => 'array',
        'id_order' => 'integer',
   ];

    public function cutting()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'id_karyawan_cutting', 'id');
    }

    public function selesai_cutting()
    {
        return $this->belongsTo('App\Model\Karyawan', 'id_user_selesai_cutting', 'id');
    }

    public function jahit()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'id_karyawan_jahit', 'id');
    }

    public function selesai_jahit()
    {
        return $this->belongsTo('App\Model\Karyawan', 'id_user_selesai_jahit', 'id');
    }

    public function packing()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'id_karyawan_packing', 'id');
    }

    public function selesai_packing()
    {
        return $this->belongsTo('App\Model\Karyawan', 'id_user_selesai_packing', 'id');
    }

    public function ekspedisi()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'id_karyawan_ekspedisi', 'id');
    }

    public function selesai_ekspedisi()
    {
        return $this->belongsTo('App\Model\Karyawan', 'id_user_selesai_ekspedisi', 'id');
    }

    public function finishing()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'id_karyawan_finishing', 'id');
    }

    public function selesai_finishing()
    {
        return $this->belongsTo('App\Model\Karyawan', 'id_user_selesai_finishing', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'id_order', 'id');
    }

    public function konsumen()
    {
        return $this->belongsTo('App\Model\Konsumen', 'id_konsumen', 'id');
    }

    public function bahan()
    {
        return $this->belongsTo('App\Model\MBahan', 'id_bahan', 'id');
    }
}
