<?php namespace App\Model;

use Auth;
use Request;
use DB;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $hidden = ['deleted_at'];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            return true;
        });
        static::updating(function($model) {
            /*$tablename = static::getTableName();
            $pk = strtolower(static::getPrimaryKeyName());
            $original = [];
            $dirty = $model->getDirty();
            foreach($dirty as $attribute => $value){
                switch ($attribute) {
                    default:
                        $original[$attribute] = $model->getOriginal($attribute);
                        break;
                }
            }
            DB::table('users_action_log')->insert([
                'username' => getUsername(),
                'ip_user' => Request::ip(),
                'time' => date("Y-m-d H:i:s"),
                'model_name' => $tablename,
                'model_id' => $model->$pk,
                'action_id' => ACTION_UPDATE,
                'before' => json_encode($original),
                'after' => json_encode($dirty),
            ]);*/
            return true;
        });

        static::created(function($model) {
            /*$pk = static::getPrimaryKeyName();
            DB::table('users_action_log')->insert([
                'username' => getUsername(),
                'ip_user' => Request::ip(),
                'time' => date("Y-m-d H:i:s"),
                'model_name' => static::getTableName(),
                'model_id' => $model->$pk,
                'action_id' => ACTION_CREATE,
            ]);*/
        });

        static::deleting(function($model) {
            /*$pk = static::getPrimaryKeyName();
            DB::table('users_action_log')->insert([
                'username' => getUsername(),
                'ip_user' => Request::ip(),
                'time' => date("Y-m-d H:i:s"),
                'model_name' => static::getTableName(),
                'model_id' => $model->$pk,
                'action_id' => ACTION_DELETE,
            ]);*/
        });
    }

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public static function getPrimaryKeyName()
    {
        return with(new static)->getKeyName();
    }
}
