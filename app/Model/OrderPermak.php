<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderPermak extends Model
{
    protected $table = 'order_permak';

    public function konsumen()
    {
    	return $this->belongsTo('App\Model\Konsumen', 'id_konsumen', 'id');
    }

    public function produksi()
    {
    	return $this->hasMany('App\Model\Produksi', 'id_order', 'id');
    }
}
