<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DynamicOrder extends Model
{
    protected $table = 'dynamic_order';
}
