<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KaryawanAbsen extends Model
{
    protected $table = 'karyawan_absen';

    public function karyawan()
    {
    	return $this->belongsTo('App\Model\Karyawan', 'karyawan_id', 'id');
    }
}
