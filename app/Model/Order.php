<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\BaseModel;

class Order extends BaseModel
{
    protected $table = 'orders';

    public function konsumen()
    {
    	return $this->belongsTo('App\Model\Konsumen', 'id_konsumen', 'id');
    }

    public function produksi()
    {
    	return $this->hasMany('App\Model\Produksi', 'id_order', 'id');
    }
}
