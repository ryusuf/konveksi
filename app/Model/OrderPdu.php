<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderPdu extends Model
{
    protected $table = 'order_pdu';

    public function konsumen()
    {
    	return $this->belongsTo('App\Model\Konsumen', 'id_konsumen', 'id');
    }

    public function produksi()
    {
    	return $this->hasMany('App\Model\Produksi', 'id_order', 'id');
    }
}
