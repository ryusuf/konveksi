<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function(){
    return redirect('/');
});

Route::get('/download', function(){
    return view('content.download');
});

Route::get('/import/absen', 'HomeController@importAbsen');

Route::POST('/store/absen', 'HomeController@storeAbsen');

Route::post('/store/fp', 'HomeController@storeFp');

Route::group(['prefix' => 'pengaturan'], function() {
	Route::get('kode-order', 'PengaturanController@index');
});

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/home/get-produksi', 'HomeController@getProduksi');

Route::group(['prefix' => 'karyawan'], function() {
	Route::get('/', 'KaryawanController@index');
	Route::get('/get-data', 'KaryawanController@getData');
	Route::get('/download', 'KaryawanController@download');

	Route::get('/create', 'KaryawanController@create');
	Route::post('/store', 'KaryawanController@store');

	Route::post('/delete', 'KaryawanController@delete');

	Route::get('/view/{id}', 'KaryawanController@view');

	Route::get('/edit/{id}', 'KaryawanController@edit');
	Route::post('/update', 'KaryawanController@update');

	Route::get('/get-karyawan', 'KaryawanController@getKaryawan');

	Route::get('/kehadiran', 'KaryawanController@kehadiran');
	Route::post('/kehadiran', 'KaryawanController@simpanKehadiran');
});

Route::group(['prefix' => 'konsumen'], function() {
	Route::get('/', 'KonsumenController@index');
	Route::get('/get-data', 'KonsumenController@getData');
	Route::get('/download', 'KonsumenController@download');

	Route::get('/create', 'KonsumenController@create');
	Route::post('/store', 'KonsumenController@store');

	Route::post('/delete', 'KonsumenController@delete');

	Route::get('/view/{id}', 'KonsumenController@view');

	Route::get('/edit/{id}', 'KonsumenController@edit');
	Route::post('/update', 'KonsumenController@update');

	Route::get('/get-konsumen', 'KonsumenController@getKonsumen');
});

Route::group(['prefix' => 'bahan'], function() {
	Route::get('/', 'MasterBahanController@index');
	Route::get('/get-data', 'MasterBahanController@getData');
	Route::get('/download', 'MasterBahanController@download');

	Route::get('/create', 'MasterBahanController@create');
	Route::post('/store', 'MasterBahanController@store');

	Route::post('/delete', 'MasterBahanController@delete');

	Route::get('/view/{id}', 'MasterBahanController@view');

	Route::get('/edit/{id}', 'MasterBahanController@edit');
	Route::post('/update', 'MasterBahanController@update');

	Route::get('/get-bahan', 'MasterBahanController@getBahan');
	Route::get('/get-satuan-bahan', 'MasterBahanController@getSatuanBahan');
});

Route::group(['prefix' => 'jasa'], function() {
	Route::get('/', 'MasterJasaController@index');
	Route::get('/get-data', 'MasterJasaController@getData');

	Route::get('/create', 'MasterJasaController@create');
	Route::post('/store', 'MasterJasaController@store');

	Route::post('/delete', 'MasterJasaController@delete');

	Route::get('/view/{id}', 'MasterJasaController@view');

	Route::get('/edit/{id}', 'MasterJasaController@edit');
	Route::post('/update', 'MasterJasaController@update');

	Route::get('/get-jahitan', 'MasterJasaController@getJahitan');
});

Route::group(['prefix' => 'tambahan'], function() {
	Route::get('/', 'TambahanController@index');
	Route::get('/get-data', 'TambahanController@getData');

	Route::get('/create', 'TambahanController@create');
	Route::post('/store', 'TambahanController@store');

	Route::post('/delete', 'TambahanController@delete');

	Route::get('/view/{id}', 'TambahanController@view');

	Route::get('/edit/{id}', 'TambahanController@edit');
	Route::post('/update', 'TambahanController@update');

	Route::get('/get-tambahan', 'TambahanController@getTambahan');
});

Route::group(['prefix' => 'new-order'], function() {
	Route::get('/index/{slug}', 'newOrderController@index');
	Route::get('/create/{slug}', 'newOrderController@create');

	Route::get('/tambah-kode', 'newOrderController@tambahKode');
	Route::post('/save-kode', 'newOrderController@saveKode');

	Route::get('/get-data', 'newOrderController@getData');
	Route::post('/store', 'newOrderController@store');
	Route::post('/update', 'newOrderController@update');
});

Route::group(['prefix' => 'order'], function() {
	Route::get('/', 'OrderController@index');
	Route::get('/get-data', 'OrderController@getData');

	Route::get('/create', 'OrderController@create');
	Route::post('/store', 'OrderController@store');

	Route::post('/delete', 'OrderController@delete');

	Route::get('/view/{id}', 'OrderController@view');

	Route::get('/edit/{id}', 'OrderController@edit');
	Route::post('/update', 'OrderController@update');

	Route::get('/get-order', 'OrderController@getOrder');
	Route::get('/get-order-detail', 'OrderController@getOrderDetail');

	Route::get('/pending', 'OrderController@pending');
	Route::post('/pending', 'OrderController@postPending');

	Route::get('/batal', 'OrderController@batal');
	Route::post('/batal', 'OrderController@postBatal');
});

Route::group(['prefix' => 'order/custom'], function() {
	Route::get('/', 'CustomController@index');
	Route::get('/get-data', 'CustomController@getData');

	Route::get('/create', 'CustomController@create');
	Route::post('/store', 'CustomController@store');

	Route::get('/edit/{id}', 'CustomController@edit');
	Route::post('/update', 'CustomController@update');
});

Route::group(['prefix' => 'order/permak'], function() {
	Route::get('/', 'PermakController@index');
	Route::get('/get-data', 'PermakController@getData');

	Route::get('/create', 'PermakController@create');
	Route::post('/store', 'PermakController@store');

	Route::get('/edit/{id}', 'PermakController@edit');
	Route::post('/update', 'PermakController@update');
});

Route::group(['prefix' => 'order/borongan'], function() {
	Route::get('/', 'BoronganController@index');
	Route::get('/get-data', 'BoronganController@getData');

	Route::get('/create', 'BoronganController@create');
	Route::post('/store', 'BoronganController@store');

	Route::get('/view/{id}', 'BoronganController@view');

	Route::post('/delete', 'BoronganController@delete');

	Route::get('/edit/{id}', 'BoronganController@edit');
	Route::post('/update', 'BoronganController@update');

	Route::get('/get-order', 'BoronganController@getOrder');
});

Route::group(['prefix' => 'order/sespim'], function() {
	Route::get('/', 'SespimController@index');
	Route::get('/get-data', 'SespimController@getData');

	Route::get('/create', 'SespimController@create');
	Route::post('/store', 'SespimController@store');

	Route::get('/edit/{id}', 'SespimController@edit');
	Route::post('/update', 'SespimController@update');
});

Route::group(['prefix' => 'produksi'], function() {
	Route::get('/get-produksi', 'ProduksiController@getProduksi');
	Route::get('/get-selesai-produksi', 'ProduksiController@getSelesaiProduksi');

	Route::get('/detail/{id}', 'ProduksiController@detail');
	Route::get('/cetak', 'ProduksiController@cetak');

	Route::get('/mulai', 'ProduksiController@mulai');
	Route::post('/store', 'ProduksiController@store');

	Route::get('/manual/{bagian}', 'ProduksiController@manual');
	Route::post('/store/manual', 'ProduksiController@storeManual');

	// Route::get('/cutting', 'ProduksiController@mulaiCutting');
	// Route::post('/cutting/store', 'ProduksiController@postMulaiCutting');

	Route::get('/cutting/selesai', 'ProduksiController@selesaiCutting');
	Route::post('/cutting/finish', 'ProduksiController@postSelesaiCutting');

	// Route::get('/jahit', 'ProduksiController@mulaiJahit');
	// Route::post('/jahit/store', 'ProduksiController@postMulaiJahit');

	Route::get('/jahit/selesai', 'ProduksiController@selesaiJahit');
	Route::post('/jahit/finish', 'ProduksiController@postSelesaiJahit');

	// Route::get('/packing', 'ProduksiController@mulaiPacking');
	// Route::post('/packing/store', 'ProduksiController@postMulaiPacking');

	Route::get('/packing/selesai', 'ProduksiController@selesaiPacking');
	Route::post('/packing/finish', 'ProduksiController@postSelesaiPacking');

	Route::get('/ekspedisi', 'ProduksiController@mulaiEkspedisi');
	Route::post('/ekspedisi/store', 'ProduksiController@postMulaiEkspedisi');

	Route::get('/ekspedisi/selesai', 'ProduksiController@selesaiEkspedisi');
	Route::post('/ekspedisi/finish', 'ProduksiController@postSelesaiEkspedisi');
	Route::post('/ekspedisi/selesai-manual', 'ProduksiController@selesaiEkspedisiManual');

	Route::post('/ekspedisi/proses', 'ProduksiController@prosesEkspedisi');
	Route::get('/ekspedisi/list', 'ProduksiController@listEkspedisi');

	// Route::get('/finishing', 'ProduksiController@mulaiFinishing');
	// Route::post('/finishing/store', 'ProduksiController@postMulaiFinishing');

	Route::get('/finishing/selesai', 'ProduksiController@selesaiFinishing');
	Route::post('/finishing/finish', 'ProduksiController@postSelesaiFinishing');

	Route::get('/batal', 'ProduksiController@batal');
	Route::post('/batal', 'ProduksiController@postBatal');
});

Route::group(['prefix' => 'laporan'], function() {
	Route::get('list/{tipe}', 	'LaporanController@index');
	Route::get('get-laporan', 	'LaporanController@getLaporan');
	Route::get('excel-laporan', 'LaporanController@getLaporan');
	Route::get('get-data-gaji', 	'LaporanController@getDataGaji');

	Route::get('gaji', 			'LaporanController@gaji');
	Route::post('get-gaji', 	'LaporanController@getGaji');

	Route::get('bonus', 			'LaporanController@bonus');
	Route::post('get-bonus', 	'LaporanController@getBonus');

	Route::get('absen', 		'LaporanController@absen');
	Route::get('get-absen', 	'LaporanController@getAbsen');
});

Route::group(['prefix' => 'report'], function() {
	Route::get('/', 			'ReportController@index');
	Route::get('/download', 	'ReportController@download');
});

Route::group(['prefix' => 'config'], function() {
	Route::get('/', 			'ConfigController@index');
	Route::patch('/update', 	'ConfigController@update');
});

Route::group(['prefix' => 'api', 'middleware' => 'api'], function() {
	Route::get('/', 'ApiController@index');
	Route::get('sync-mesin', 'ApiController@syncMesin');
	Route::get('get-tpp', 'ApiController@getTpp');
	Route::get('get-satuan-kerja', 'ApiController@getSatuanKerja');
	Route::get('get-satuan-organisasi', 'ApiController@getSatuanOrganisasi');
	Route::get('get-unit-kerja', 'ApiController@getUnitKerja');
	Route::get('get-daftar-hadir/{tgl1}/{tgl2}', 'ApiController@getDaftarHadir');
	Route::get('get-presensi-bulan', 'ApiController@getPresensiBulan');
	Route::get('get-daftar-absen-apel', 'ApiController@getDaftarAbsenApel');
});